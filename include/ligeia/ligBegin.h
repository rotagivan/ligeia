/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligBegin.h
 *  @brief ligBegin.h
 *
 *  ligBegin.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/05/07
 *  @date 2017/06/24
 */

#ifndef LIGEIA_LIGBEGIN_H_
#define LIGEIA_LIGBEGIN_H_

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#ifndef LIGEIA_CALL
# if defined(_WINDOWS) && !defined(__GNUC__)
#  define LIGEIA_CALL __cdecl
# else
#  define LIGEIA_CALL
# endif
#endif /* LIGEIA_CALL */
/* ========================================================================= */
#if defined(__GNUC__)
# if __GNUC__ >= 4
#  define LIGEIA_DECLSPEC_EXPORTS __attribute__ ((visibility("default")))
#  define LIGEIA_DECLSPEC_IMPORTS
# elif __GNUC__ >= 2
#  define LIGEIA_DECLSPEC_EXPORTS __declspec(dllexport)
#  define LIGEIA_DECLSPEC_IMPORTS
# endif
#elif defined(_WINDOWS)
#  define LIGEIA_DECLSPEC_EXPORTS __declspec(dllexport)
#  define LIGEIA_DECLSPEC_IMPORTS __declspec(dllimport)
#else
# define LIGEIA_DECLSPEC_EXPORTS
# define LIGEIA_DECLSPEC_IMPORTS
#endif
/* ------------------------------------------------------------------------- */
#ifndef LIGEIA_DECLSPEC
# if defined(LIGEIA_EXPORTS)
#  define LIGEIA_DECLSPEC LIGEIA_DECLSPEC_EXPORTS
# else
#  define LIGEIA_DECLSPEC LIGEIA_DECLSPEC_IMPORTS
# endif
#endif /* LIGEIA_DECLSPEC */

#endif  /* LIGEIA_LIGBEGIN_H_ */
