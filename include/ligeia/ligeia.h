/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file liegia.h
 *  @brief liegia.h
 *
 *  liegia.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/05/31
 *  @date 2018/05/19
 */

#ifndef LIGEIA_LIGEIA_H_
#define LIGEIA_LIGEIA_H_

#include <stdlib.h>

#include <ligeia/ligBegin.h>

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct Ligeia_ Ligeia;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  Ligeia*       LIGEIA_CALL Ligeia_new(size_t);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL Ligeia_delete(Ligeia*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGEIA_H_ */
