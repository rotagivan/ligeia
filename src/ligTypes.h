/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligType.h
 *  @brief ligType.h
 *
 *  ligType.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/07
 *  @date 2018/07/04
 */

#ifndef LIGEIA_LIGTYPE_H_
#define LIGEIA_LIGTYPE_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "./ligReturn.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct        ligSECD_                ligSECD;
  typedef               ligReturn               (*ligSECD_func_t)(ligSECD*);
  /* ======================================================================= */
  typedef struct        ligMemory_              ligMemory;
  /* ======================================================================= */
  typedef struct        ligReader_              ligReader;
  /* ======================================================================= */
  typedef union         ligCell_                ligCell;
  typedef /*@null@*/    ligCell                 *ligRegister;
  /* ----------------------------------------------------------------------- */
  typedef ligReturn     (*ligCell_fold_func_t)(ligCell*,
                                               const ligCell*, const ligCell*);
  /* ----------------------------------------------------------------------- */
  typedef size_t        (*ligCell_cnum_cst_t)(/*@null@*/ const ligCell*);
  typedef void          (*ligCell_display_t)(FILE*, /*@null@*/ const ligCell*);
  typedef ligReturn     (*ligCell_migration_t)(ligCell*, ligMemory*);
  typedef bool          (*ligCell_is_eqv_t)(const ligCell*, const ligCell*);
  typedef ligReturn     (*ligSECD_expand_Cell_t)(ligSECD*);
  typedef ligReturn     (*ligSECD_quasi_Cell_t)(ligSECD*);
  typedef ligReturn     (*ligSECD_eval_Cell_t)(ligSECD*);
  typedef ligReturn     (*ligSECD_call_Cell_t)(ligSECD*);
  typedef ligReturn     (*ligSECD_run_Cell_t)(ligSECD*);
  typedef ligReturn     (*ligSECD_fold_Cell_t)(ligSECD*,
                                               const ligCell_fold_func_t);
  /* ----------------------------------------------------------------------- */
  typedef struct LIGEIA_VTBL_t_ {
    /*@null@*/ const    ligCell_cnum_cst_t      cnum_cst;
    /*@null@*/ const    ligCell_display_t       display;
    /*@null@*/ const    ligCell_migration_t     migration;
    /*@null@*/ const    ligCell_is_eqv_t        is_eqv;
    /*@null@*/ const    ligSECD_expand_Cell_t   expand;
    /*@null@*/ const    ligSECD_quasi_Cell_t    quasi;
    /*@null@*/ const    ligSECD_eval_Cell_t     eval;
    /*@null@*/ const    ligSECD_call_Cell_t     call;
    /*@null@*/ const    ligSECD_run_Cell_t      run;
    /*@null@*/ const    ligSECD_fold_Cell_t     foldl;
    /*@null@*/ const    ligCell_fold_func_t     mul;
    /*@null@*/ const    ligCell_fold_func_t     div;
    /*@null@*/ const    ligCell_fold_func_t     add;
    /*@null@*/ const    ligCell_fold_func_t     sub;
  } LIGEIA_VTBL_t;

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGTYPE_H_ */
