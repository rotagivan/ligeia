/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligMemory.h
 *  @brief ligMemory.h
 *
 *  ligMemory.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/06
 *  @date 2018/07/04
 */

#ifndef LIGEIA_LIGMEMORY_H_
#define LIGEIA_LIGMEMORY_H_

#include <ligeia/ligBegin.h>

#include "./ligReturn.h"
#include "./ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligMemory*
  LIGEIA_CALL ligMemory_new(size_t);

  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligMemory_delete(/*@null@*/ ligMemory*);

  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligMemory_cell(ligMemory*, ligRegister*, size_t, size_t);

  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligMemory_gc(ligMemory*, ligRegister*, size_t);

  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligMemory_migration(ligMemory*, ligRegister*);

  extern LIGEIA_DECLSPEC size_t
  LIGEIA_CALL ligMemory_capacity(const ligMemory*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGMEMORY_H_ */
