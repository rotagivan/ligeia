/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligReturn.c
 *  @brief ligReturn.c
 *
 *  ligReturn.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/07
 *  @date 2018/07/22
 */

#include "./ligReturn.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
const char* ligReturnStrings[LIGEIA_RETURN_SIZE_] = {
  "ok.",

  "code push 00.",
  "code push 01.",
  "code push 02.",
  "code push 03.",
  "code push 04.",
  "code push 05.",
  "code push 06.",
  "code push 07.",
  "code push 08.",
  "code push 09.",
  "code push 10.",

  "inner error.",

  "no memory.",
  "immutable.",
  "unbound.",
  "zero divide.",

  "invalid argc.",
  "invalid args.",
  "invalid applicable.",

  "parse error.",

  "syntax not in quasiquote.",

  "quasiquote: invalid nesting level.",
  "unquote-splicing: not list.",

  "io: no such file or directory.",
  "io: eacces.",

  "failed fopen.",

  "unknown.",
};
