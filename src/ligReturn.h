/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligReturn.h
 *  @brief ligReturn.h
 *
 *  ligReturn.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/06
 *  @date 2018/07/01
 */

#ifndef LIGEIA_LIGRETURN_H_
#define LIGEIA_LIGRETURN_H_

#include <stdio.h>

#include <ligeia/ligBegin.h>

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligReturn_ {
    LIGEIA_RETURN_BEGIN_        = 0,

    LIGEIA_RETURN_OK            = LIGEIA_RETURN_BEGIN_,

    LIGEIA_RETURN_CODE_PUSH_00,
    LIGEIA_RETURN_CODE_PUSH_01,
    LIGEIA_RETURN_CODE_PUSH_02,
    LIGEIA_RETURN_CODE_PUSH_03,
    LIGEIA_RETURN_CODE_PUSH_04,
    LIGEIA_RETURN_CODE_PUSH_05,
    LIGEIA_RETURN_CODE_PUSH_06,
    LIGEIA_RETURN_CODE_PUSH_07,
    LIGEIA_RETURN_CODE_PUSH_08,
    LIGEIA_RETURN_CODE_PUSH_09,
    LIGEIA_RETURN_CODE_PUSH_10,

    LIGEIA_RETURN_INNER_ERROR,

    LIGEIA_RETURN_NO_MEMORY,
    LIGEIA_RETURN_IMMUTABLE,
    LIGEIA_RETURN_UNBOUND,
    LIGEIA_RETURN_ZERO_DIVIDE,

    LIGEIA_RETURN_INVALID_ARGC,
    LIGEIA_RETURN_INVALID_ARGS,
    LIGEIA_RETURN_INVALID_APPLICABLE,

    LIGEIA_RETURN_PARSE_ERROR,

    LIGEIA_RETURN_SYNTAX_NOT_MATCH,

    LIGEIA_RETURN_QQ_INVALID_LV,
    LIGEIA_RETURN_QQ_UNQSPL_NOT_LIST,

    LIGEIA_RETURN_ERRNO_ENOENT,
    LIGEIA_RETURN_ERRNO_EACCES,

    LIGEIA_RETURN_FAILED_FOPEN,

    LIGEIA_RETURN_UNKNOWN,

    LIGEIA_RETURN_END_,
    LIGEIA_RETURN_SIZE_         = LIGEIA_RETURN_END_ - LIGEIA_RETURN_BEGIN_
  } ligReturn;
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC const char* ligReturnStrings[LIGEIA_RETURN_SIZE_];
  /* ----------------------------------------------------------------------- */
# define ligRetAction___(expr, line, rettype, retval, action, arg)      \
  do {                                                                  \
    rettype retval = (expr);                                            \
    if (LIGEIA_RETURN_OK != retval) {                                   \
      (void) fprintf(stderr,                                            \
                     PACKAGE_STRING ": " __FILE__ "(" #line "): %s\n",  \
                     ligReturnStrings[retval]);                         \
      (void) fflush(stderr);                                            \
      action arg;                                                       \
    }                                                                   \
  } while (0)
  /* ----------------------------------------------------------------------- */
# define ligRetExp__(expr, line)                                        \
  ligRetAction___(expr, line, ligReturn, retval_##line, return, retval_##line)
# define ligRetExp_(expr, line)         ligRetExp__(expr, line)
# define ligRetExp(expr)                ligRetExp_((expr), __LINE__)
  /* ----------------------------------------------------------------------- */
# define ligRetExpNull__(expr, line)                                    \
  ligRetAction___(expr, line, ligReturn, retval_##line, return, NULL)
# define ligRetExpNull_(expr, line)     ligRetExpNull__(expr, line)
# define ligRetExpNull(expr)            ligRetExpNull_((expr), __LINE__)
  /* ----------------------------------------------------------------------- */
# define ligGotoExp__(expr, retval, label, line)        \
  ligRetAction___(expr, line, , retval, goto, label)
# define ligGotoExp_(expr, retval, label, line) \
  ligGotoExp__(expr, retval, label, line)
# define ligGotoExp(expr, retval, label)        \
  ligGotoExp_((expr), retval, label, __LINE__)

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */


#endif  /* LIGEIA_LIGRETURN_H_ */
