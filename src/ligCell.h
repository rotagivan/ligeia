/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCell.h
 *  @brief ligCell.h
 *
 *  ligCell.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/03
 *  @date 2018/07/05
 */

#ifndef LIGEIA_LIGCELL_H_
#define LIGEIA_LIGCELL_H_

#include <ligeia/ligBegin.h>

#include "./ligTypes.h"
#include "./ligReturn.h"
#include "./ligCell/ligOperator.h"
#include "./ligCell/ligPort.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligCellNotConsType {
    LIGEIA_CNCT_SHIFT_          = 4u,

    LIGEIA_CNCT_BEGIN_          = 0u,

    LIGEIA_CNCT_DUMMY_          = LIGEIA_CNCT_BEGIN_,  /* LIGEIA_CNCT_TRUE */
    LIGEIA_CNCT_FREE,
    LIGEIA_CNCT_LOCATION,
    LIGEIA_CNCT_ENVSPEC,
    LIGEIA_CNCT_VARSPEC,
    LIGEIA_CNCT_ARGIDX,
    LIGEIA_CNCT_FLOAT,
    LIGEIA_CNCT_INT,
    LIGEIA_CNCT_CHAR,
    LIGEIA_CNCT_CSTR,
    LIGEIA_CNCT_SYMBOL,
    LIGEIA_CNCT_CONTINUE,
    LIGEIA_CNCT_PORT,
    LIGEIA_CNCT_LET,
    LIGEIA_CNCT_DEFINE,
    LIGEIA_CNCT_SETX,
    LIGEIA_CNCT_LAMBDA,
    LIGEIA_CNCT_PROCEDURE,
    LIGEIA_CNCT_FOREIGN,
    LIGEIA_CNCT_OPERATOR,
    LIGEIA_CNCT_VECTOR,
    LIGEIA_CNCT_CONSTVEC,
    LIGEIA_CNCT_SYNCLO,
    LIGEIA_CNCT_MACTRA,
    LIGEIA_CNCT_UNDEF,
    LIGEIA_CNCT_QUOTE,
    LIGEIA_CNCT_QUASIQUOTE,
    LIGEIA_CNCT_UNQUOTE,
    LIGEIA_CNCT_UNQSPL,
    LIGEIA_CNCT_ERROR,
    LIGEIA_CNCT_STRING,
    LIGEIA_CNCT_ELLIPSIS,
    LIGEIA_CNCT_EOF,

    LIGEIA_CNCT_END_,
    LIGEIA_CNCT_SIZE_           = LIGEIA_CNCT_END_ - LIGEIA_CNCT_BEGIN_

  } ligCellNotConsType;
  /* ======================================================================= */
# define MAKE_LIGEIA_CT(type)                                           \
  LIGEIA_CT_##type = (                                                  \
      (LIGEIA_CNCT_##type << LIGEIA_CNCT_SHIFT_) | LIGEIA_CT_TRUE)
  typedef enum ligCellType {
    /*! xxxx xxxx xxxx xxxA */
    LIGEIA_CT_PRECIOUS_MASK     = (   1u <<  0u),

    /*! xxxx xxxx xxxx xxIx */
    LIGEIA_CT_IMMUTABLE_MASK    = (   1u <<  1u),

    /*! pppp pppp pppp pCxx */
    LIGEIA_CT_CONS_MASK         = (   1u <<  2u),
    LIGEIA_CT_CONS              = LIGEIA_CT_CONS_MASK,
    LIGEIA_CT_NOT_CONS          = (   0u <<  2u),

    /*! pppp pppp pppp pCIA */
    LIGEIA_CT_TAG_MASK          = (LIGEIA_CT_CONS_MASK |
                                   LIGEIA_CT_IMMUTABLE_MASK |
                                   LIGEIA_CT_PRECIOUS_MASK),
    /*! PPPP PPPP PPPP Pxxx */
    LIGEIA_CT_POINTER_MASK      = ~LIGEIA_CT_TAG_MASK,

    /*! 0000 0000 0000 1100 */
    LIGEIA_CT_BOOL_MASK         = ((   1u <<  3u) | LIGEIA_CT_CONS_MASK),

    /*! 0000 0000 0000 00xx */
    LIGEIA_CT_FALSE             = ((   0u <<  3u) | LIGEIA_CT_NOT_CONS),

    /*! 0000 0000 0000 10xx */
    LIGEIA_CT_TRUE              = ((   1u <<  3u) | LIGEIA_CT_NOT_CONS),

    /*! TTTT TTTT TTTT 10xx */
    MAKE_LIGEIA_CT(FREE),
    MAKE_LIGEIA_CT(LOCATION),
    MAKE_LIGEIA_CT(ENVSPEC),
    MAKE_LIGEIA_CT(VARSPEC),
    MAKE_LIGEIA_CT(ARGIDX),
    MAKE_LIGEIA_CT(FLOAT),
    MAKE_LIGEIA_CT(INT),
    MAKE_LIGEIA_CT(CHAR),
    MAKE_LIGEIA_CT(CSTR),
    MAKE_LIGEIA_CT(SYMBOL),
    MAKE_LIGEIA_CT(CONTINUE),
    MAKE_LIGEIA_CT(PORT),
    MAKE_LIGEIA_CT(LET),
    MAKE_LIGEIA_CT(DEFINE),
    MAKE_LIGEIA_CT(SETX),
    MAKE_LIGEIA_CT(LAMBDA),
    MAKE_LIGEIA_CT(PROCEDURE),
    MAKE_LIGEIA_CT(FOREIGN),
    MAKE_LIGEIA_CT(OPERATOR),
    MAKE_LIGEIA_CT(VECTOR),
    MAKE_LIGEIA_CT(CONSTVEC),
    MAKE_LIGEIA_CT(SYNCLO),
    MAKE_LIGEIA_CT(MACTRA),
    MAKE_LIGEIA_CT(UNDEF),
    MAKE_LIGEIA_CT(QUOTE),
    MAKE_LIGEIA_CT(QUASIQUOTE),
    MAKE_LIGEIA_CT(UNQUOTE),
    MAKE_LIGEIA_CT(UNQSPL),
    MAKE_LIGEIA_CT(ERROR),
    MAKE_LIGEIA_CT(STRING),
    MAKE_LIGEIA_CT(ELLIPSIS),
    MAKE_LIGEIA_CT(EOF),

    LIGEIA_CT_END_
  } ligCellType;
# undef MAKE_LIGEIA_CT
  /* ======================================================================= */
# define MAKE_LIGEIA_CST(type)                                  \
  LIGEIA_CST_##type = (LIGEIA_CT_##type >> LIGEIA_CST_SHIFT_)
  typedef enum ligCellShiftedType {
    LIGEIA_CST_SHIFT_           = 2u,

    MAKE_LIGEIA_CST(CONS),
    MAKE_LIGEIA_CST(FALSE),
    MAKE_LIGEIA_CST(TRUE),
    MAKE_LIGEIA_CST(FREE),
    MAKE_LIGEIA_CST(LOCATION),
    MAKE_LIGEIA_CST(ENVSPEC),
    MAKE_LIGEIA_CST(VARSPEC),
    MAKE_LIGEIA_CST(ARGIDX),
    MAKE_LIGEIA_CST(FLOAT),
    MAKE_LIGEIA_CST(INT),
    MAKE_LIGEIA_CST(CHAR),
    MAKE_LIGEIA_CST(CSTR),
    MAKE_LIGEIA_CST(SYMBOL),
    MAKE_LIGEIA_CST(CONTINUE),
    MAKE_LIGEIA_CST(PORT),
    MAKE_LIGEIA_CST(LET),
    MAKE_LIGEIA_CST(DEFINE),
    MAKE_LIGEIA_CST(SETX),
    MAKE_LIGEIA_CST(LAMBDA),
    MAKE_LIGEIA_CST(PROCEDURE),
    MAKE_LIGEIA_CST(FOREIGN),
    MAKE_LIGEIA_CST(OPERATOR),
    MAKE_LIGEIA_CST(VECTOR),
    MAKE_LIGEIA_CST(CONSTVEC),
    MAKE_LIGEIA_CST(SYNCLO),
    MAKE_LIGEIA_CST(MACTRA),
    MAKE_LIGEIA_CST(UNDEF),
    MAKE_LIGEIA_CST(QUOTE),
    MAKE_LIGEIA_CST(QUASIQUOTE),
    MAKE_LIGEIA_CST(UNQUOTE),
    MAKE_LIGEIA_CST(UNQSPL),
    MAKE_LIGEIA_CST(ERROR),
    MAKE_LIGEIA_CST(STRING),
    MAKE_LIGEIA_CST(ELLIPSIS),
    MAKE_LIGEIA_CST(EOF),

    LIGEIA_CST_END_
  } ligCellShiftedType;
# undef MAKE_LIGEIA_CST
  /* ======================================================================= */
  typedef uintptr_t ligCell_type_t;
# define ligCell_value_size sizeof(ligCell_type_t)
  /* ======================================================================= */
  union ligCell_ {
    ligCell_type_t      type;
    struct Cons {
      union {
        ligCell_type_t                  type;
        ligCell*                        car;
      }                         a;
      union {
        ligCell*                        cdr;
      }                         b;
    }                   cons;
    struct Atom {
      ligCell_type_t            type;
      union {
        ligCell*                        cell;
        intptr_t                        int_;
        float                           float_;
        char                            char_;
        char                            cstr[ligCell_value_size/sizeof(char)];
        ligOperator                     operator;
      }                         value;
    }                   atom;
  };
  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
# define ligCell_p_notag(s)                                     \
  ((ligCell* const)(((ligCell_type_t)(s)) &                     \
                    (ligCell_type_t)LIGEIA_CT_POINTER_MASK))
# define ligCell_p_tag(s)                                       \
  (((ligCell_type_t)s) & (ligCell_type_t)LIGEIA_CT_TAG_MASK)
# define ligCell_p_is_nil(s)        (NULL == ligCell_p_notag(s))
# define ligCell_p_mix(s, t)                                    \
  ((ligCell* const)(((ligCell_type_t)(ligCell_p_notag(s))|t)))

# define ligCell_type(s)            (ligCell_p_notag(s)->type)
# define ligCell_cnct(s)                                        \
  ((ligCellNotConsType)(ligCell_type(s) >> LIGEIA_CNCT_SHIFT_))
# define ligCell_cst2cnct(cst)                                          \
  ((ligCellNotConsType)((cst) >> (LIGEIA_CNCT_SHIFT_ - LIGEIA_CST_SHIFT_)))

# define ligCell_is_eq(l, r)        (ligCell_p_notag(l) == ligCell_p_notag(r))

# define ligCell_is_precious(s)                         \
  (0u != (ligCell_type(s) & LIGEIA_CT_PRECIOUS_MASK))
# define ligCell_is_immutable(s)                        \
  (0u != (ligCell_type(s) & LIGEIA_CT_IMMUTABLE_MASK))

# define ligCell_is_cons(s)                                     \
  ((ligCell_type(s) & LIGEIA_CT_CONS_MASK) == LIGEIA_CT_CONS)
# define ligCell_is_false(s)                                    \
  ((ligCell_type(s) & LIGEIA_CT_BOOL_MASK) == LIGEIA_CT_FALSE)
# define ligCell_is_broad_true(s)   (!ligCell_is_false(s))
# define ligCell_is_strict_true(s)  (ligCell_cst(s) == LIGEIA_CST_TRUE)
# define ligCell_is_free(s)         (ligCell_cst(s) == LIGEIA_CST_FREE)
# define ligCell_is_location(s)     (ligCell_cst(s) == LIGEIA_CST_LOCATION)
# define ligCell_is_envspec(s)      (ligCell_cst(s) == LIGEIA_CST_ENVSPEC)
# define ligCell_is_varspec(s)      (ligCell_cst(s) == LIGEIA_CST_VARSPEC)
# define ligCell_is_argidx(s)       (ligCell_cst(s) == LIGEIA_CST_ARGIDX)
# define ligCell_is_float(s)        (ligCell_cst(s) == LIGEIA_CST_FLOAT)
# define ligCell_is_int(s)          (ligCell_cst(s) == LIGEIA_CST_INT)
# define ligCell_is_char(s)         (ligCell_cst(s) == LIGEIA_CST_CHAR)
# define ligCell_is_cstr(s)         (ligCell_cst(s) == LIGEIA_CST_CSTR)
# define ligCell_is_symbol(s)       (ligCell_cst(s) == LIGEIA_CST_SYMBOL)
# define ligCell_is_continue(s)     (ligCell_cst(s) == LIGEIA_CST_CONTINUE)
# define ligCell_is_port(s)         (ligCell_cst(s) == LIGEIA_CST_PORT)
# define ligCell_is_let(s)          (ligCell_cst(s) == LIGEIA_CST_LET)
# define ligCell_is_define(s)       (ligCell_cst(s) == LIGEIA_CST_DEFINE)
# define ligCell_is_setx(s)         (ligCell_cst(s) == LIGEIA_CST_SETX)
# define ligCell_is_lambda(s)       (ligCell_cst(s) == LIGEIA_CST_LAMBDA)
# define ligCell_is_procedure(s)    (ligCell_cst(s) == LIGEIA_CST_PROCEDURE)
# define ligCell_is_foreign(s)      (ligCell_cst(s) == LIGEIA_CST_FOREIGN)
# define ligCell_is_operator(s)     (ligCell_cst(s) == LIGEIA_CST_OPERATOR)
# define ligCell_is_vector(s)       (ligCell_cst(s) == LIGEIA_CST_VECTOR)
# define ligCell_is_constvec(s)     (ligCell_cst(s) == LIGEIA_CST_CONSTVEC)
# define ligCell_is_synclo(s)       (ligCell_cst(s) == LIGEIA_CST_SYNCLO)
# define ligCell_is_mactra(s)       (ligCell_cst(s) == LIGEIA_CST_MACTRA)
# define ligCell_is_undef(s)        (ligCell_cst(s) == LIGEIA_CST_UNDEF)
# define ligCell_is_quote(s)        (ligCell_cst(s) == LIGEIA_CST_QUOTE)
# define ligCell_is_quasiquote(s)   (ligCell_cst(s) == LIGEIA_CST_QUASIQUOTE)
# define ligCell_is_unquote(s)      (ligCell_cst(s) == LIGEIA_CST_UNQUOTE)
# define ligCell_is_unqspl(s)       (ligCell_cst(s) == LIGEIA_CST_UNQSPL)
# define ligCell_is_error(s)        (ligCell_cst(s) == LIGEIA_CST_ERROR)
# define ligCell_is_string(s)       (ligCell_cst(s) == LIGEIA_CST_STRING)
# define ligCell_is_ellipsis(s)     (ligCell_cst(s) == LIGEIA_CST_ELLIPSIS)
# define ligCell_is_eof(s)          (ligCell_cst(s) == LIGEIA_CST_EOF)

# define ligCell_set_precious(s)                \
  (ligCell_type(s) |= LIGEIA_CT_PRECIOUS_MASK)
# define ligCell_reset_precious(s)                                      \
  (ligCell_type(s) &= ((ligCell_type_t)(~LIGEIA_CT_PRECIOUS_MASK)))
# define ligCell_set_immutable(s)               \
  (ligCell_type(s) |= LIGEIA_CT_IMMUTABLE_MASK)
# define ligCell_reset_immutable(s)                                     \
  (ligCell_type(s) &= ((ligCell_type_t)(~LIGEIA_CT_IMMUTABLE_MASK)))

# define ligCell_set_cons(s)         (ligCell_type(s) |= LIGEIA_CT_CONS_MASK)
# define ligCell_set_t(s, type_)     (ligCell_type(s)  = (type_))
# define ligCell_set_false(s)        ligCell_set_t((s), LIGEIA_CT_FALSE)
# define ligCell_set_true(s)         ligCell_set_t((s), LIGEIA_CT_TRUE)
# define ligCell_set_free(s)         ligCell_set_t((s), LIGEIA_CT_FREE)
# define ligCell_set_location(s)     ligCell_set_t((s), LIGEIA_CT_LOCATION)
# define ligCell_set_envspec(s)      ligCell_set_t((s), LIGEIA_CT_ENVSPEC)
# define ligCell_set_varspec(s)      ligCell_set_t((s), LIGEIA_CT_VARSPEC)
# define ligCell_set_argidx(s)       ligCell_set_t((s), LIGEIA_CT_ARGIDX)
# define ligCell_set_float(s)        ligCell_set_t((s), LIGEIA_CT_FLOAT)
# define ligCell_set_int(s)          ligCell_set_t((s), LIGEIA_CT_INT)
# define ligCell_set_char(s)         ligCell_set_t((s), LIGEIA_CT_CHAR)
# define ligCell_set_cstr(s)         ligCell_set_t((s), LIGEIA_CT_CSTR)
# define ligCell_set_symbol(s)       ligCell_set_t((s), LIGEIA_CT_SYMBOL)
# define ligCell_set_continue(s)     ligCell_set_t((s), LIGEIA_CT_CONTINUE)
# define ligCell_set_port(s)         ligCell_set_t((s), LIGEIA_CT_PORT)
# define ligCell_set_let(s)          ligCell_set_t((s), LIGEIA_CT_LET)
# define ligCell_set_define(s)       ligCell_set_t((s), LIGEIA_CT_DEFINE)
# define ligCell_set_setx(s)         ligCell_set_t((s), LIGEIA_CT_SETX)
# define ligCell_set_lambda(s)       ligCell_set_t((s), LIGEIA_CT_LAMBDA)
# define ligCell_set_procedure(s)    ligCell_set_t((s), LIGEIA_CT_PROCEDURE)
# define ligCell_set_foreign(s)      ligCell_set_t((s), LIGEIA_CT_FOREIGN)
# define ligCell_set_next(s, next)   (ligCell_next(s)  = ligCell_p_notag(next))
# define ligCell_set_operator(s)     ligCell_set_t((s), LIGEIA_CT_OPERATOR)
# define ligCell_set_vector(s)       ligCell_set_t((s), LIGEIA_CT_VECTOR)
# define ligCell_set_constvec(s)     ligCell_set_t((s), LIGEIA_CT_CONSTVEC)
# define ligCell_set_synclo(s)       ligCell_set_t((s), LIGEIA_CT_SYNCLO)
# define ligCell_set_mactra(s)       ligCell_set_t((s), LIGEIA_CT_MACTRA)
# define ligCell_set_undef(s)        ligCell_set_t((s), LIGEIA_CT_UNDEF)
# define ligCell_set_quote(s)        ligCell_set_t((s), LIGEIA_CT_QUOTE)
# define ligCell_set_quasiquote(s)   ligCell_set_t((s), LIGEIA_CT_QUASIQUOTE)
# define ligCell_set_unquote(s)      ligCell_set_t((s), LIGEIA_CT_UNQUOTE)
# define ligCell_set_unqspl(s)       ligCell_set_t((s), LIGEIA_CT_UNQSPL)
# define ligCell_set_error(s)        ligCell_set_t((s), LIGEIA_CT_ERROR)
# define ligCell_set_string(s)       ligCell_set_t((s), LIGEIA_CT_STRING)
# define ligCell_set_ellipsis(s)     ligCell_set_t((s), LIGEIA_CT_ELLIPSIS)
# define ligCell_set_eof(s)          ligCell_set_t((s), LIGEIA_CT_EOF)

# define ligCell_cons(s)        (ligCell_p_notag(s)->cons)
# define ligCell_car(s)         (ligCell_cons(s).a.car)
# define ligCell_cdr(s)         (ligCell_cons(s).b.cdr)
# define ligCell_caar(s)        ligCell_car(ligCell_p_notag(ligCell_car(s)))
# define ligCell_cadr(s)        ligCell_car(ligCell_cdr(s))
# define ligCell_cddr(s)        ligCell_cdr(ligCell_cdr(s))
# define ligCell_cdar(s)        ligCell_cdr(ligCell_p_notag(ligCell_car(s)))
# define ligCell_caaar(s)       ligCell_car(ligCell_p_notag(ligCell_caar(s)))
# define ligCell_caadr(s)       ligCell_car(ligCell_p_notag(ligCell_cadr(s)))
# define ligCell_caddr(s)       ligCell_car(ligCell_cddr(s))
# define ligCell_cadar(s)       ligCell_car(ligCell_cdar(s))
# define ligCell_cdaar(s)       ligCell_cdr(ligCell_p_notag(ligCell_caar(s)))
# define ligCell_cdadr(s)       ligCell_cdr(ligCell_p_notag(ligCell_cadr(s)))
# define ligCell_cdddr(s)       ligCell_cdr(ligCell_cddr(s))
# define ligCell_cddar(s)       ligCell_cdr(ligCell_cdar(s))
# define ligCell_caaaar(s)      ligCell_car(ligCell_p_notag(ligCell_caaar(s)))
# define ligCell_caaadr(s)      ligCell_car(ligCell_p_notag(ligCell_caadr(s)))
# define ligCell_caaddr(s)      ligCell_car(ligCell_p_notag(ligCell_caddr(s)))
# define ligCell_caadar(s)      ligCell_car(ligCell_p_notag(ligCell_cadar(s)))
# define ligCell_cadaar(s)      ligCell_car(ligCell_cdaar(s))
# define ligCell_cadadr(s)      ligCell_car(ligCell_cdadr(s))
# define ligCell_cadddr(s)      ligCell_car(ligCell_cdddr(s))
# define ligCell_caddar(s)      ligCell_car(ligCell_cddar(s))
# define ligCell_cdaaar(s)      ligCell_cdr(ligCell_p_notag(ligCell_caaar(s)))
# define ligCell_cdaadr(s)      ligCell_cdr(ligCell_p_notag(ligCell_caadr(s)))
# define ligCell_cdaddr(s)      ligCell_cdr(ligCell_p_notag(ligCell_caddr(s)))
# define ligCell_cdadar(s)      ligCell_cdr(ligCell_p_notag(ligCell_cadar(s)))
# define ligCell_cddaar(s)      ligCell_cdr(ligCell_cdaar(s))
# define ligCell_cddadr(s)      ligCell_cdr(ligCell_cdadr(s))
# define ligCell_cddddr(s)      ligCell_cdr(ligCell_cdddr(s))
# define ligCell_cdddar(s)      ligCell_cdr(ligCell_cddar(s))

# define ligCell_value(s)            (ligCell_p_notag(s)->atom.value)
# define ligCell_int(s)              (ligCell_value(s).int_)
# define ligCell_float(s)            (ligCell_value(s).float_)
# define ligCell_char(s)             (ligCell_value(s).char_)
# define ligCell_cstr(s)             ((char*)(ligCell_value(s).cstr))
# define ligCell_symbol_ptr(s)       ((ligSymbol*)(&(ligCell_value(s))))
# define ligCell_continue_ptr(s)     ((ligContinue*)(&(ligCell_value(s))))
# define ligCell_location(s)         (ligCell_value(s).cell)
# define ligCell_envspec_env(s)      (ligCell_value(s).cell)
# define ligCell_varspec_cell(s)     (ligCell_value(s).cell)
# define ligCell_argidx_cell(s)      (ligCell_value(s).cell)
# define ligCell_port_ptr(s)         ((ligPort*)(&(ligCell_value(s))))
# define ligCell_procedure_ptr(s)    ((ligProcedure*)(&(ligCell_value(s))))
# define ligCell_foreign_ptr(s)      ((ligForeign*)(&(ligCell_value(s))))
# define ligCell_next(s)             (ligCell_value(s).cell)
# define ligCell_operator(s)         (ligCell_value(s).operator)
# define ligCell_vector_ptr(s)       ((ligVector*)(&(ligCell_value(s))))
# define ligCell_synclo_ptr(s)       ((ligSynClo*)(&(ligCell_value(s))))
# define ligCell_mactra_ptr(s)       ((ligMacTra*)(&(ligCell_value(s))))
# define ligCell_string_ptr(s)       ((ligString*)(&(ligCell_value(s))))
# define ligCell_ellipsis_ptr(s)     ((ligEllipsis*)(&(ligCell_value(s))))
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligCell_type_t
  LIGEIA_CALL ligCell_cst(const /*@null@*/ ligCell*);
  extern LIGEIA_DECLSPEC size_t
  LIGEIA_CALL ligCell_cnum(/*@null@*/ const ligCell*);
  extern LIGEIA_DECLSPEC size_t
  LIGEIA_CALL ligCell_cnum_size(size_t);
  extern LIGEIA_DECLSPEC size_t
  LIGEIA_CALL ligCell_cnum_cst(/*@null@*/ const ligCell*, ligCell_type_t);
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligCell_display(FILE*, /*@null@*/ const ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCell_migration(ligCell*, ligMemory*);
  extern LIGEIA_DECLSPEC bool
  LIGEIA_CALL ligCell_is_eqv(const ligCell*, const ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_run_Cell(const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t* const
  LIGEIA_VTBL[LIGEIA_CNCT_SIZE_];

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_H_ */
