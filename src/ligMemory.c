/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligMemory.c
 *  @brief ligMemory.c
 *
 *  ligMemory.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/07
 *  @date 2018/07/04
 */

#include "./ligMemory.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <memory.h>

#include "./debug.h"
#include "./ligReturn.h"
#include "./ligCell.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
typedef struct ligPool_ {
  /*@null@*/ ligCell*           begin;
  /*@null@*/ ligCell*           next;
  /*@null@*/ ligCell*           end;
} ligPool;
/* ========================================================================= */
static void                     ligPool_synapse(ligPool*, ligCell* const,
                                                size_t);
static void                     ligPool_akasha(ligPool*, ligCell* const,
                                               size_t);
static void                     ligPool_dispose(ligPool*);
static void                     ligPool_reset(ligPool*);
static size_t                   ligPool_capacity(const ligPool*);
static bool                     ligPool_is_living(ligPool*,
                                                  /*@null@*/ const ligCell*);
static /*@null@*/ ligCell*      ligPool_synapse_new_cell(ligPool*, size_t);
static /*@null@*/ ligCell*      ligPool_akasha_new_cell(ligPool*, size_t);
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
typedef enum ligMemoryFlags_ {
  LIGEIA_MEMORY_FLAG_SYNAPSE    = (1u << 0u),
  LIGEIA_MEMORY_FLAG_VACUUM     = (1u << 1u),
  LIGEIA_MEMORY_FLAG_PRECIOUS   = (1u << 2u),
  LIGEIA_MEMORY_FLAG_END_
} ligMemoryFlags;
/* ------------------------------------------------------------------------- */
typedef unsigned ligMemoryFlags_t;
#define ligMemoryFlags_set(flags, f)    ((flags) |= ((ligMemoryFlags_t)(f)))
#define ligMemoryFlags_reset(flags, f)  ((flags) &= (~((ligMemoryFlags_t)(f))))
#define ligMemoryFlags_flip(flags, f)   ((flags) ^= ((ligMemoryFlags_t)(f)))
#define ligMemoryFlags_test(flags, f)           \
  (0u != ((flags) & ((ligMemoryFlags_t)(f))))
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
struct ligMemory_ {
  ligPool                       synapse[2];
  ligPool                       akasha;
  ligCell*                      root;
  ligMemoryFlags_t              flags;
};
/* ========================================================================= */
#define ligMemory_current_synapse_idx(self)                             \
  ((self)->flags & ((ligMemoryFlags_t)(LIGEIA_MEMORY_FLAG_SYNAPSE)))
/* ========================================================================= */
static void     ligMemory_vacuum(ligMemory*, ligPool*);
static void     ligMemory_set_precious(ligMemory*, ligCell*);
static bool     ligMemory_test_precious(ligMemory*, ligCell*);
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligPool_synapse(ligPool* self, ligCell* const root, size_t size) {
  LIGEIA_TRACE_DEBUG("ligPool_synapse");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(root);
  LIGEIA_ASSERT(size > 0);
  self->begin   = root;
  self->next    = self->begin;
  self->end     = self->begin + size;
}
/* ========================================================================= */
void ligPool_akasha(ligPool* self, ligCell* const root, size_t size) {
  LIGEIA_TRACE_DEBUG("ligPool_akasha");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(root);
  LIGEIA_ASSERT(size > 0);
  ligPool_synapse(self, root, size);
  {
    ligCell *current = self->begin, *end = self->end;
    for (; current != end; ++current) {
      ligCell_set_free(current);
      ligCell_set_next(current, (current + 1));
    }
  }
}
/* ========================================================================= */
void ligPool_dispose(ligPool* self) {
  LIGEIA_TRACE_DEBUG("ligPool_dispose");
  LIGEIA_ASSERT(self);
  self->begin   = NULL;
  self->next    = NULL;
  self->end     = NULL;
}
/* ========================================================================= */
void ligPool_reset(ligPool* self) {
  LIGEIA_TRACE_DEBUG("ligPool_reset");
  LIGEIA_ASSERT(self);
  self->next = self->begin;
}
/* ========================================================================= */
size_t ligPool_capacity(const ligPool* self) {
  LIGEIA_TRACE_DEBUG("ligPool_capacity");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(self->begin < self->end);
  return (size_t)(self->end - self->begin);
}
/* ========================================================================= */
bool ligPool_is_living(ligPool* self, const ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligPool_is_living");
  LIGEIA_ASSERT(self);
  cell = ligCell_p_notag(cell);
  return ((self->begin <= cell) && (cell < self->end));
}
/* ========================================================================= */
ligCell* ligPool_synapse_new_cell(ligPool* self, size_t num) {
  LIGEIA_TRACE_DEBUG("ligPool_synapse_new_cell");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(0 < num);
  LIGEIA_ASSERT(self->begin <= self->next);
  LIGEIA_ASSERT(!ligCell_p_is_nil(self->next));
  if ((self->next + num) > self->end) { return NULL; }
  {
    ligCell *cell = self->next;
    self->next += num;
    ligCell_set_t(cell, 0u);
    return cell;
  }
}
/* ========================================================================= */
ligCell* ligPool_akasha_new_cell(ligPool* self, size_t num) {
  LIGEIA_TRACE_DEBUG("ligPool_akasha_new_cell");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(0 < num);
  LIGEIA_ASSERT(self->begin <= self->next);
  LIGEIA_ASSERT(!ligCell_p_is_nil(self->next));
  {
    ligCell *head       = self->next;
    if (num == 1) {
      if (head >= self->end) { return NULL; }
      self->next        = ligCell_next(head);
    } else {
      ligCell **link    = &self->next;
      ligCell *tail     = head;
      while ((size_t)(tail - head) < num) {
        if (tail >= self->end) { return NULL; }
        if ((ligCell_next(tail) - tail) == 1) {
          tail = ligCell_next(tail);
        } else {
          link = &(ligCell_next(tail));
          head = *link;
          tail = head;
        }
      }
      *link = tail;
    }
    ligCell_set_t(head, 0u);
    return head;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
/* SIZEOF_ligMemory
 * padding for align sizeof(ligCell)
 */
#define SIZEOF_ligMemory                        \
  ((sizeof(ligMemory) + sizeof(ligCell) - 1u) / \
   sizeof(ligCell) * sizeof(ligCell))
/* ------------------------------------------------------------------------- */
ligMemory* ligMemory_new(size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligMemory_new");
  ligRetExpNull((SIZEOF_ligMemory + sizeof(ligCell)) < a_Size ?
                LIGEIA_RETURN_OK : LIGEIA_RETURN_INVALID_ARGS);
  a_Size -= SIZEOF_ligMemory;
  a_Size /= sizeof(ligCell);
  {
    size_t cap = (size_t) ((float) a_Size * 0.191f);
    ligRetExpNull(0u != cap ? LIGEIA_RETURN_OK : LIGEIA_RETURN_INVALID_ARGS);
    {
      ligMemory *self = (ligMemory*) malloc(SIZEOF_ligMemory +
                                            a_Size * sizeof(ligCell));
      if (!self) {
        LIGEIA_TRACE_ALWAYS("err!: ligMemory_new: failed malloc.");
        return NULL;
      }
      self->flags = 0u;
      self->root = (ligCell*) ((uintptr_t) self + SIZEOF_ligMemory);
      {
        ligPool_synapse(&self->synapse[0], self->root,       cap);
        ligPool_synapse(&self->synapse[1], self->root + cap, cap);
        cap += cap;
        ligPool_akasha(&self->akasha, self->root + cap, a_Size - cap);
      }
      return self;
    }
  }
}
/* ------------------------------------------------------------------------- */
#undef SIZEOF_ligMemory
/* ========================================================================= */
void ligMemory_delete(ligMemory *self) {
  LIGEIA_TRACE_DEBUG("ligMemory_delete");
  if (!self) { return; }
  ligPool_dispose(&self->akasha);
  ligPool_dispose(&self->synapse[1]);
  ligPool_dispose(&self->synapse[0]);
  self->root = NULL;
  self->flags = 0u;
  free(self);
}
/* ========================================================================= */
size_t ligMemory_capacity(const ligMemory *self) {
  LIGEIA_TRACE_DEBUG("ligMemory_capacity");
  LIGEIA_ASSERT(self);
  return (ligPool_capacity(&self->synapse[0]) +
          ligPool_capacity(&self->synapse[1]) +
          ligPool_capacity(&self->akasha));
}
/* ========================================================================= */
ligCell* ligMemory_cell(ligMemory *self,
                        ligRegister *regs, size_t regc, size_t size) {
  LIGEIA_TRACE_DEBUG("ligMemory_cell");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(regs);
  LIGEIA_ASSERT(0 < size);
  {
    size_t idx    = ligMemory_current_synapse_idx(self);
    ligCell *cell = ligPool_synapse_new_cell(&self->synapse[idx], size);
    if (cell) { return cell; }
    {
      int i = 0;
      for (; i < 3; ++i) {
        ligRetExpNull(ligMemory_gc(self, regs, regc));
        idx = ligMemory_current_synapse_idx(self);
        cell = ligPool_synapse_new_cell(&self->synapse[idx], size);
        if (cell) { return cell; }
      }
    }
    return NULL;
  }
}
/* ========================================================================= */
ligReturn ligMemory_gc(ligMemory *self, ligRegister *regs, size_t regc) {
  LIGEIA_TRACE_DEBUG("ligMemory_gc");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(regs);
  LIGEIA_TRACE("*** GARBAGE COLLECTION ***");
  {
    size_t i = 0u;
    for (; i < regc; ++i) { ligRetExp(ligMemory_migration(self, &regs[i])); }
  }
  ligPool_reset(&self->synapse[ligMemory_current_synapse_idx(self)]);
  ligMemoryFlags_flip(self->flags, LIGEIA_MEMORY_FLAG_SYNAPSE);
# ifndef LIGEIA_DEBUG_GC
  if (ligMemoryFlags_test(self->flags, LIGEIA_MEMORY_FLAG_VACUUM))
# endif
  {
    ligMemory_vacuum(self, &self->akasha);
    ligMemoryFlags_reset(self->flags, LIGEIA_MEMORY_FLAG_VACUUM);
  }
  ligMemoryFlags_flip(self->flags, LIGEIA_MEMORY_FLAG_PRECIOUS);
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligMemory_migration(ligMemory *self, ligRegister *psrc) {
  LIGEIA_TRACE_DEBUG("ligMemory_migration");
  LIGEIA_ASSERT_DEBUG(self);
  LIGEIA_ASSERT_DEBUG(psrc);
  if (!*psrc || ligCell_p_is_nil(*psrc)) { return LIGEIA_RETURN_OK; }
  LIGEIA_ASSERT(!ligCell_is_free(*psrc));
  if (ligCell_is_location(*psrc)) {
    LIGEIA_ASSERT(!ligPool_is_living(&self->synapse[
        ligMemory_current_synapse_idx(self) ^ 1], *psrc));
    LIGEIA_ASSERT(!ligPool_is_living(&self->akasha, *psrc));
    *psrc = ligCell_p_mix(ligCell_location(*psrc), ligCell_p_tag(*psrc));
    return LIGEIA_RETURN_OK;
  }
  if (ligPool_is_living(&self->akasha, *psrc)) {
    /* akasha */
    LIGEIA_ASSERT(!ligPool_is_living(&self->synapse[0], *psrc));
    LIGEIA_ASSERT(!ligPool_is_living(&self->synapse[1], *psrc));
    if (ligMemory_test_precious(self, *psrc)) { return LIGEIA_RETURN_OK; }
    ligMemory_set_precious(self, *psrc);
    return ligCell_migration(*psrc, self);
  } else {
    /* synapse */
    size_t ni     = ligMemory_current_synapse_idx(self) ^ 1;
    size_t num    = ligCell_cnum(*psrc);
    ligCell *dest = NULL;
    LIGEIA_ASSERT(!ligPool_is_living(&self->synapse[ni], *psrc));
    LIGEIA_ASSERT(!ligPool_is_living(&self->akasha, *psrc));
    if (ligCell_is_precious(*psrc)) {
      dest = ligPool_akasha_new_cell(&self->akasha, num);
      if (dest) {
        ligMemory_set_precious(self, *psrc);
      } else {
        ligMemoryFlags_set(self->flags, LIGEIA_MEMORY_FLAG_VACUUM);
        dest = ligPool_synapse_new_cell(&self->synapse[ni], num);
        if (!dest) {
          LIGEIA_TRACE_ALWAYS("ERROR!: failed ligPool_synapse_new_cell");
          return LIGEIA_RETURN_NO_MEMORY;
        }
        ligCell_set_precious(*psrc);
      }
    } else {
      dest = ligPool_synapse_new_cell(&self->synapse[ni], num);
      if (!dest) {
        LIGEIA_TRACE_ALWAYS("ERROR!: failed ligPool_synapse_new_cell");
        return LIGEIA_RETURN_NO_MEMORY;
      }
      ligCell_set_precious(*psrc);
    }

    memcpy(dest, ligCell_p_notag(*psrc), num * sizeof(ligCell));
    ligCell_set_location(*psrc);
    ligCell_location(*psrc) = dest;

    *psrc = ligCell_p_mix(dest, ligCell_p_tag(*psrc));
    return ligCell_migration(*psrc, self);
  }
}
/* ========================================================================= */
void ligMemory_vacuum(ligMemory *self, ligPool* pool) {
  LIGEIA_TRACE("***       VACUUM       ***");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(pool);
  LIGEIA_ASSERT(pool->begin);
  {
    size_t num          = 0u;
    ligCell *current    = pool->begin;
    for (; current != pool->end; current += num) {
      LIGEIA_ASSERT_ALWAYS(current);
      num = ligCell_cnum(current);
      if (ligCell_is_free(current) ||
          ligMemory_test_precious(self, current)) {
        continue;
      } else {
        ligCell *last   = NULL;
        for (last = current + num - 1; last >= current; --last) {
          ligCell_set_free(last);
          ligCell_set_next(last, pool->next);
          pool->next = ligCell_p_notag(last);
        }
      }
    }
  }
}
/* ========================================================================= */
void ligMemory_set_precious(ligMemory *self, ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligMemory_set_precious");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(cell));
  (void) (ligMemoryFlags_test(self->flags, LIGEIA_MEMORY_FLAG_PRECIOUS) ?
          ligCell_set_precious(cell) : ligCell_reset_precious(cell));
}
/* ------------------------------------------------------------------------- */
bool ligMemory_test_precious(ligMemory *self, ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligMemory_reset_precious");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(cell));
  return (ligMemoryFlags_test(self->flags, LIGEIA_MEMORY_FLAG_PRECIOUS) ?
          ligCell_is_precious(cell) : !ligCell_is_precious(cell));
}
