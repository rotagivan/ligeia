/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligHash.c
 *  @brief ligHash.c
 *
 *  ligHash.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/27
 *  @date 2017/07/15
 */

#include "./ligHash.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include "./debug.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
uint32_t ligHash_combine(uint32_t a_Seed,
                         const uint8_t *a_Data, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligHash_combine");
  LIGEIA_ASSERT(a_Data);
  LIGEIA_ASSERT(0 < a_Size);
  {
    const uint8_t *l_End = a_Data + a_Size;
    for (; a_Data != l_End; ++a_Data) {
      a_Seed ^= *a_Data ^ 0x9e3779b9u ^ (a_Seed << 6u) ^ (a_Seed >> 2u);
    }
    return a_Seed;
  }
}
