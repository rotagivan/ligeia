/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSECD.h
 *  @brief ligSECD.h
 *
 *  Stack, Environment, Code and Dump machine.
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/07/08
 */

#ifndef LIGEIA_LIGSECD_H_
#define LIGEIA_LIGSECD_H_

#include <ligeia/ligBegin.h>

#include "./ligTypes.h"
#include "./ligReturn.h"
#include "./ligCell/ligOperator.h"
#include "./ligMemory.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligSECD*
  LIGEIA_CALL ligSECD_new(size_t);
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligSECD_delete(/*@null@*/ ligSECD*);
  extern LIGEIA_DECLSPEC ligReader*
  LIGEIA_CALL ligSECD_reader(ligSECD*);
  /* ======================================================================= */
# define ligSECD_print(s) do {                                  \
    LIGEIA_TRACE_ALWAYS("print"); ligSECD_print_impl(s);        \
  } while(0)
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligSECD_print_impl(const ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_display(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_displayln(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_newline(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_gc(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_reset(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_top_level(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_quasi(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_eval(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_call(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_proc_pre(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_run(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_stack(const ligSECD*);
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligSECD_stack_set(ligSECD*, /*@null@*/ ligCell*);
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_stack_top(const ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_set_imm(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push(ligSECD*, /*@NULL@*/ ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_nil(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_cons(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_envspec(ligSECD*, ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_varspec(ligSECD*, ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_bool(ligSECD*, bool);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_float(ligSECD*, float);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_int(ligSECD*, intptr_t);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_op(ligSECD*, ligOperator);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_vector(ligSECD*, size_t);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_constvec(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_varidx(ligSECD*, uint16_t, uint16_t);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_cstr(ligSECD*, const char*, size_t);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_symbol(ligSECD*, const char*, size_t);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_let(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_define(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_setx(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_lambda(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_ellipsis(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_quote(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_quasiquote(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_unquote(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_push_unqspl(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_pop(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_head(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_copy(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_press(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_swap(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_cons
   * <pre>
   * start              | stack | ~ |  cdr  |  car  | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_cons         | stack | ~ |  (car . cdr)  | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_cons(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_xcons
   * <pre>
   * start              | stack | ~ |  car  |  cdr  | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_xcons        | stack | ~ |  (car . cdr)  | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_xcons(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_unlist
   * <pre>
   * start              | stack |  (head ~ last)  | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_unlist       | stack | last | ~ | head | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_unlist(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_append
   * <pre>
   * start              | stack | ~ | (tail ~) | (head ~) | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_append       | stack | ~ |  (head ~ tails ~)   | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_append(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_xappend
   * <pre>
   * start              | stack | ~ | (head ~) | (tail ~) | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_xappend      | stack | ~ |  (head ~ tails ~)   | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_xappend(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_car
   * <pre>
   * start              | stack | ~ | (car.cdr) | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_car          | stack | ~ |    car    | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_car(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_stack_cdr
   * <pre>
   * start              | stack | ~ | (car.cdr) | *top*
   *
   * LIGEIA_RETURN_OK
   * stack_cdr          | stack | ~ |    cdr    | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_cdr(ligSECD*);
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_to_code(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_env(const ligSECD*);
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligSECD_env_set(ligSECD*, /*@null@*/ ligCell*);
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_env_top(const ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_env_pop(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_env_push(ligSECD*, /*@NULL@*/ ligCell*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_env_push_nil(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_code(const ligSECD*);
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligSECD_code_set(ligSECD*, /*@null@*/ ligCell*);
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_code_top(const ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_code_pop(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_code_push_op(ligSECD*, ligOperator);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_dump_save_emit(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligSECD_work(const ligSECD*);
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_stack_to_work(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_env_to_work(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_work_to_stack(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_work_to_env(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC intptr_t
  LIGEIA_CALL ligSECD_qqlv_int(const ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_qqlv_inc(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_qqlv_dec(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_list(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_list_to_vector(ligSECD*);
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_list_last(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_define(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_define_impl
   * <pre>
   * start              | stack |  ~  |  val  | symbol | *top*
   *                    |  env  | ( ) | *top*
   *
   * LIGEIA_RETURN_OK
   * define_impl        | stack |  ~  | *top*
   *                    |  env  | ((symbol . val)) | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * start              | stack |  ~  |  val  | symbol | *top*
   *                    |  env  | ( ) | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_define_impl(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_find
   * <pre>
   * start              | stack |  ~  | symbol | *top*
   *
   * LIGEIA_RETURN_OK
   * find               | stack |  ~  |  val   | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY, LIGEIA_RETURN_UNBOUND
   * find               | stack |  ~  | symbol | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_find(ligSECD*);
  /* ======================================================================= */
  /*! ligSECD_lambda
   * <pre>
   * start              | stack | ~ | keys | body | *top*
   *
   * LIGEIA_RETURN_OK
   * lambda             | stack | ~ |  procedure  | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * lambda             | stack | ~ | keys | body | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_lambda(ligSECD*);
  /* ======================================================================= */
  /*! ligSECD_mksynclo
   * <pre>
   * start              | stack | ~ | form | freevar | envspec | *top*
   *
   * LIGEIA_RETURN_OK
   * mksynclo           | stack | ~ | syntactic-closure | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * mksynclo           | stack | ~ | form | freevar | envspec | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_mksynclo(ligSECD*);
  /* ======================================================================= */
  /*! ligSECD_mul
   * <pre>
   * start              | stack | argsN | ... | args1 | args0 | *top*
   *
   * LIGEIA_RETURN_OK
   * mul                | stack | args0 * args1 * ... * argsN | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * mul                | stack | argsN | ... | args0 | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_mul(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_div
   * <pre>
   * start              | stack | argsN | ... | args1 | args0 | *top*
   *
   * LIGEIA_RETURN_OK
   * div                | stack | args0 / args1 / ... / argsN | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * div                | stack | argsN | ... | args0 | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_div(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_add
   * <pre>
   * start              | stack | argsN | ... | args1 | args0 | *top*
   *
   * LIGEIA_RETURN_OK
   * add                | stack | args0 + args1 + ... + argsN | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * add                | stack | argsN | ... | args0 | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_add(ligSECD*);
  /* ----------------------------------------------------------------------- */
  /*! ligSECD_sub
   * <pre>
   * start              | stack | argsN | ... | args1 | args0 | *top*
   *
   * LIGEIA_RETURN_OK
   * sub                | stack | args0 - args1 - ... - argsN | *top*
   *
   * LIGEIA_RETURN_NO_MEMORY
   * sub                | stack | argsN | ... | args0 | *top*
   * </pre>
   */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_sub(ligSECD*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGSECD_H_ */
