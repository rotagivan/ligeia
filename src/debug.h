/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file debug.h
 *  @brief debug.h
 *
 *  debug.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/24
 *  @date 2018/07/19
 */

#ifndef LIGEIA_DEBUG_H_
#define LIGEIA_DEBUG_H_

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#define LIGEIA_VERBOSITY_DEBUG                  (0x00)
#define LIGEIA_VERBOSITY_INFO                   (0x3F)
#define LIGEIA_VERBOSITY_WARNING                (0x7F)
#define LIGEIA_VERBOSITY_ERROR                  (0xBF)
#define LIGEIA_VERBOSITY_CRITICAL               (0xFF)
/* ------------------------------------------------------------------------- */
#define LIGEIA_VERBOSITY_ALL                    LIGEIA_VERBOSITY_DEBUG
#define LIGEIA_VERBOSITY_DEFAULT                LIGEIA_VERBOSITY_INFO
#define LIGEIA_VERBOSITY_QUIET                  LIGEIA_VERBOSITY_CRITICAL
/* ------------------------------------------------------------------------- */
#if !defined(LIGEIA_VERBOSITY)
# define LIGEIA_VERBOSITY                       LIGEIA_VERBOSITY_DEFAULT
#endif
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#ifdef __cplusplus
# include <cstdio>
# include <cstdlib>
# include <cstdarg>
# define LIGEIA_USING_NAMESPACE_STD using namespace std
#else  /* !__cplusplus */
# include <stdio.h>
# include <stdlib.h>
# include <stdarg.h>
# define LIGEIA_USING_NAMESPACE_STD
#endif  /* !__cplusplus */
/* ========================================================================= */
#define LIGEIA_TRACE_ALWAYS__(line, d)                                  \
  do {                                                                  \
    LIGEIA_USING_NAMESPACE_STD;                                         \
    (void) fprintf(stderr, PACKAGE_STRING ": " __FILE__ "(" #line "): " \
                   d "\n");                                             \
    (void) fflush(stderr);                                              \
  } while (0)
#define LIGEIA_TRACE_ALWAYS_(line, d)   LIGEIA_TRACE_ALWAYS__(line, d)
#define LIGEIA_TRACE_ALWAYS(d)          LIGEIA_TRACE_ALWAYS_(__LINE__, d)
/* ========================================================================= */
#if ((defined(__cplusplus) && (__cplusplus >= 201103L)) ||              \
     (defined(__STDC__) &&                                              \
      defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)))
# define LIGEIA_TRACEF_ALWAYS__(line, fmt, ...)                         \
  do {                                                                  \
    LIGEIA_USING_NAMESPACE_STD;                                         \
    fprintf(stderr, __FILE__ "(" #line ") %s: " fmt "\n", __func__,     \
            __VA_ARGS__);                                               \
    (void) fflush(stderr);                                              \
  } while (0)
# define LIGEIA_TRACEF_ALWAYS_(line, fmt, ...)          \
  LIGEIA_TRACEF_ALWAYS__(line, fmt, __VA_ARGS__)
# define LIGEIA_TRACEF_ALWAYS(fmt, ...)                 \
  LIGEIA_TRACEF_ALWAYS_(__LINE__, fmt, __VA_ARGS__)
#endif  /* defined(__STDC__) && ... */
/* ========================================================================= */
#define LIGEIA_ASSERT_ALWAYS__(c, t)            \
  do { if (!(c)) {                              \
      LIGEIA_TRACE_ALWAYS(t);                   \
      abort();                                  \
    }                                           \
  } while (0)
#define LIGEIA_ASSERT_ALWAYS_(c, t)             LIGEIA_ASSERT_ALWAYS__(c, t)
#define LIGEIA_ASSERT_ALWAYS(c)                 LIGEIA_ASSERT_ALWAYS_(c, #c)
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#if (defined(DEBUG) && !defined(LIGEIA_DEBUG))
# define LIGEIA_DEBUG
#endif
/* ========================================================================= */
#if (defined(NDEBUG) && defined(LIGEIA_DEBUG))
# undef LIGEIA_DEBUG
#endif
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#ifdef LIGEIA_DEBUG
/* ========================================================================= */
# define LIGEIA_TRACE(d)                        LIGEIA_TRACE_ALWAYS(d)
/* ------------------------------------------------------------------------- */
# define LIGEIA_TRACE_VERBOSITY__(l, d)                                 \
  do { if (LIGEIA_VERBOSITY <= (l)) { LIGEIA_TRACE(d); } } while (0)
# define LIGEIA_TRACE_VERBOSITY_(l, d)          LIGEIA_TRACE_VERBOSITY__(l, d)
# define LIGEIA_TRACE_VERBOSITY(l, d)           LIGEIA_TRACE_VERBOSITY_(l, d)
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_DEBUG)
#   define LIGEIA_TRACE_DEBUG__(d)              LIGEIA_TRACE(d)
#   define LIGEIA_TRACE_DEBUG_(d)               LIGEIA_TRACE_DEBUG__(d)
#   define LIGEIA_TRACE_DEBUG(d)                LIGEIA_TRACE_DEBUG_(d)
# else
#   define LIGEIA_TRACE_DEBUG(d)                ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_INFO)
#   define LIGEIA_TRACE_INFO__(d)               LIGEIA_TRACE(d)
#   define LIGEIA_TRACE_INFO_(d)                LIGEIA_TRACE_INFO__(d)
#   define LIGEIA_TRACE_INFO(d)                 LIGEIA_TRACE_INFO_(d)
# else
#   define LIGEIA_TRACE_INFO(d)                 ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_WARNING)
#   define LIGEIA_TRACE_WARNING__(d)            LIGEIA_TRACE(d)
#   define LIGEIA_TRACE_WARNING_(d)             LIGEIA_TRACE_WARNING__(d)
#   define LIGEIA_TRACE_WARNING(d)              LIGEIA_TRACE_WARNING_(d)
# else
#   define LIGEIA_TRACE_WARNING(d)              ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_ERROR)
#   define LIGEIA_TRACE_ERROR__(d)              LIGEIA_TRACE(d)
#   define LIGEIA_TRACE_ERROR_(d)               LIGEIA_TRACE_ERROR__(d)
#   define LIGEIA_TRACE_ERROR(d)                LIGEIA_TRACE_ERROR_(d)
# else
#   define LIGEIA_TRACE_ERROR(d)                ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_CRITICAL)
#   define LIGEIA_TRACE_CRITICAL__(d)           LIGEIA_TRACE(d)
#   define LIGEIA_TRACE_CRITICAL_(d)            LIGEIA_TRACE_CRITICAL__(d)
#   define LIGEIA_TRACE_CRITICAL(d)             LIGEIA_TRACE_CRITICAL_(d)
# else
#   define LIGEIA_TRACE_CRITICAL(d)             ((void)(0))
# endif
/* ========================================================================= */
# if ((defined(__cplusplus) && (__cplusplus >= 201103L)) ||             \
      (defined(__STDC__) &&                                             \
       defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)))
#   define LIGEIA_TRACEF(fmt, ...)      LIGEIA_TRACEF_ALWAYS(fmt, __VA_ARGS__)
/* ------------------------------------------------------------------------- */
#   define LIGEIA_TRACEF_VERBOSITY__(l, fmt, ...)                       \
  do {                                                                  \
    if (LIGEIA_VERBOSITY <= (l)) { LIGEIA_TRACEF(fmt, __VA_ARGS__); }   \
  } while (0)
#   define LIGEIA_TRACEF_VERBOSITY_(l, fmt, ...)        \
  LIGEIA_TRACEF_VERBOSITY__(l, fmt, __VA_ARGS__)
#   define LIGEIA_TRACEF_VERBOSITY(l, fmt, ...) \
  LIGEIA_TRACEF_VERBOSITY_(l, fmt, __VA_ARGS__)
/* ------------------------------------------------------------------------- */
#   if (defined(LIGEIA_VERBOSITY) &&                    \
        LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_DEBUG)
#     define LIGEIA_TRACEF_DEBUG__(fmt, ...)    \
  LIGEIA_TRACEF(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_DEBUG_(fmt, ...)     \
  LIGEIA_TRACEF_DEBUG__(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_DEBUG(fmt, ...)      \
  LIGEIA_TRACEF_DEBUG_(fmt, __VA_ARGS__)
#   else
#     define LIGEIA_TRACEF_DEBUG(fmt, ...)      ((void)(0))
#   endif
/* ------------------------------------------------------------------------- */
#   if (defined(LIGEIA_VERBOSITY) &&                    \
        LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_INFO)
#     define LIGEIA_TRACEF_INFO__(fmt, ...)     \
  LIGEIA_TRACEF(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_INFO_(fmt, ...)      \
  LIGEIA_TRACEF_INFO__(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_INFO(fmt, ...)       \
  LIGEIA_TRACEF_INFO_(fmt, __VA_ARGS__)
#   else
#     define LIGEIA_TRACEF_INFO(fmt, ...)       ((void)(0))
#   endif
/* ------------------------------------------------------------------------- */
#   if (defined(LIGEIA_VERBOSITY) &&                    \
        LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_WARNING)
#     define LIGEIA_TRACEF_WARNING__(fmt, ...)  \
  LIGEIA_TRACEF(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_WARNING_(fmt, ...)   \
  LIGEIA_TRACEF_WARNING__(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_WARNING(fmt, ...)    \
  LIGEIA_TRACEF_WARNING_(fmt, __VA_ARGS__)
#   else
#     define LIGEIA_TRACEF_WARNING(fmt, ...)    ((void)(0))
#   endif
/* ------------------------------------------------------------------------- */
#   if (defined(LIGEIA_VERBOSITY) &&                    \
        LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_ERROR)
#     define LIGEIA_TRACEF_ERROR__(fmt, ...)    \
  LIGEIA_TRACEF(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_ERROR_(fmt, ...)     \
  LIGEIA_TRACEF_ERROR__(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_ERROR(fmt, ...)      \
  LIGEIA_TRACEF_ERROR_(fmt, __VA_ARGS__)
#   else
#     define LIGEIA_TRACEF_ERROR(fmt, ...)      ((void)(0))
#   endif
/* ------------------------------------------------------------------------- */
#   if (defined(LIGEIA_VERBOSITY) &&                    \
        LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_CRITICAL)
#     define LIGEIA_TRACEF_CRITICAL__(fmt, ...) \
  LIGEIA_TRACEF(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_CRITICAL_(fmt, ...)  \
  LIGEIA_TRACEF_CRITICAL__(fmt, __VA_ARGS__)
#     define LIGEIA_TRACEF_CRITICAL(fmt, ...)   \
  LIGEIA_TRACEF_CRITICAL_(fmt, __VA_ARGS__)
#   else
#     define LIGEIA_TRACEF_CRITICAL(fmt, ...)   ((void)(0))
#   endif
# endif  /* defined(__STDC__) && ... */
/* ========================================================================= */
# define LIGEIA_ASSERT(c)                       LIGEIA_ASSERT_ALWAYS(c)
/* ------------------------------------------------------------------------- */
# define LIGEIA_ASSERT_VERBOSITY__(l, c)                                \
  do { if (LIGEIA_VERBOSITY <= (l)) { LIGEIA_ASSERT(c); } } while (0)
# define LIGEIA_ASSERT_VERBOSITY_(l, c)         \
  LIGEIA_ASSERT_VERBOSITY__(l, c)
# define LIGEIA_ASSERT_VERBOSITY(l, c)          \
  LIGEIA_ASSERT_VERBOSITY_(l, c)
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_DEBUG)
#   define LIGEIA_ASSERT_DEBUG__(c)             LIGEIA_ASSERT(c)
#   define LIGEIA_ASSERT_DEBUG_(c)              LIGEIA_ASSERT_DEBUG__(c)
#   define LIGEIA_ASSERT_DEBUG(c)               LIGEIA_ASSERT_DEBUG_(c)
# else
#   define LIGEIA_ASSERT_DEBUG(c)               ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_INFO)
#   define LIGEIA_ASSERT_INFO__(c)              LIGEIA_ASSERT(c)
#   define LIGEIA_ASSERT_INFO_(c)               LIGEIA_ASSERT_INFO__(c)
#   define LIGEIA_ASSERT_INFO(c)                LIGEIA_ASSERT_INFO_(c)
# else
#   define LIGEIA_ASSERT_INFO(c)                ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_WARNING)
#   define LIGEIA_ASSERT_WARNING__(c)           LIGEIA_ASSERT(c)
#   define LIGEIA_ASSERT_WARNING_(c)            LIGEIA_ASSERT_WARNING__(c)
#   define LIGEIA_ASSERT_WARNING(c)             LIGEIA_ASSERT_WARNING_(c)
# else
#   define LIGEIA_ASSERT_WARNING(c)             ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_ERROR)
#   define LIGEIA_ASSERT_ERROR__(c)             LIGEIA_ASSERT(c)
#   define LIGEIA_ASSERT_ERROR_(c)              LIGEIA_ASSERT_ERROR__(c)
#   define LIGEIA_ASSERT_ERROR(c)               LIGEIA_ASSERT_ERROR_(c)
# else
#   define LIGEIA_ASSERT_ERROR(c)               ((void)(0))
# endif
/* ------------------------------------------------------------------------- */
# if (defined(LIGEIA_VERBOSITY) &&                      \
      LIGEIA_VERBOSITY <= LIGEIA_VERBOSITY_CRITICAL)
#   define LIGEIA_ASSERT_CRITICAL__(c)          LIGEIA_ASSERT(c)
#   define LIGEIA_ASSERT_CRITICAL_(c)           LIGEIA_ASSERT_CRITICAL__(c)
#   define LIGEIA_ASSERT_CRITICAL(c)            LIGEIA_ASSERT_CRITICAL_(c)
# else
#   define LIGEIA_ASSERT_CRITICAL(c)            ((void)(0))
# endif
#else  /* LIGEIA_DEBUG  //////////////////////////////////////////////////// */
/* ========================================================================= */
# define LIGEIA_TRACE(d)                        ((void)(0))
# define LIGEIA_TRACE_VERBOSITY(l, d)           ((void)(0))
# define LIGEIA_TRACE_DEBUG(d)                  ((void)(0))
# define LIGEIA_TRACE_INFO(d)                   ((void)(0))
# define LIGEIA_TRACE_WARNING(d)                ((void)(0))
# define LIGEIA_TRACE_ERROR(d)                  ((void)(0))
# define LIGEIA_TRACE_CRITICAL(d)               ((void)(0))
/* ========================================================================= */
# if ((defined(__cplusplus) && (__cplusplus >= 201103L)) ||             \
      (defined(__STDC__) &&                                             \
       defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L)))
#   define LIGEIA_TRACEF(fmt, ...)              ((void)(0))
#   define LIGEIA_TRACEF_VERBOSITY(l, fmt, ...) ((void)(0))
#   define LIGEIA_TRACEF_DEBUG(fmt, ...)        ((void)(0))
#   define LIGEIA_TRACEF_INFO(fmt, ...)         ((void)(0))
#   define LIGEIA_TRACEF_WARNING(fmt, ...)      ((void)(0))
#   define LIGEIA_TRACEF_ERROR(fmt, ...)        ((void)(0))
#   define LIGEIA_TRACEF_CRITICAL(fmt, ...)     ((void)(0))
# endif  /* defined(__STDC__) && ... */
/* ========================================================================= */
# define LIGEIA_ASSERT(c)                       ((void)(0))
# define LIGEIA_ASSERT_VERBOSITY(l, c)          ((void)(0))
# define LIGEIA_ASSERT_DEBUG(c)                 ((void)(0))
# define LIGEIA_ASSERT_INFO(c)                  ((void)(0))
# define LIGEIA_ASSERT_WARNING(c)               ((void)(0))
# define LIGEIA_ASSERT_ERROR(c)                 ((void)(0))
# define LIGEIA_ASSERT_CRITICAL(c)              ((void)(0))
#endif  /* LIGEIA_DEBUG  /////////////////////////////////////////////////// */

#endif  /* LIGEIA_DEBUG_H_ */
