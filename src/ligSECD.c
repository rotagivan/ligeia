/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSECD.c
 *  @brief ligSECD.c
 *
 *  Stack, Environment, Code and Dump machine.
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/07/26
 */

#include "./ligSECD.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "./debug.h"
#include "./ligCell.h"
#include "./ligCell/ligCStr.h"
#include "./ligCell/ligCons.h"
#include "./ligCell/ligConstVec.h"
#include "./ligCell/ligContinue.h"
#include "./ligCell/ligDefine.h"
#include "./ligCell/ligEnvSpec.h"
#include "./ligCell/ligFloat.h"
#include "./ligCell/ligInt.h"
#include "./ligCell/ligLambda.h"
#include "./ligCell/ligLet.h"
#include "./ligCell/ligOperator.h"
#include "./ligCell/ligProcedure.h"
#include "./ligCell/ligQuasiquote.h"
#include "./ligCell/ligQuote.h"
#include "./ligCell/ligSetx.h"
#include "./ligCell/ligSymbol.h"
#include "./ligCell/ligSynClo.h"
#include "./ligCell/ligUnqSpl.h"
#include "./ligCell/ligUnquote.h"
#include "./ligCell/ligVarSpec.h"
#include "./ligCell/ligVector.h"
#include "./ligReader.h"
#include "./ligReturn.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
typedef enum ligReg_ {
  REG_BEGIN_ = 0,

  REG_tmp = REG_BEGIN_,
  REG_stack,
  REG_env,
  REG_code,
  REG_dump,
  REG_work,
  REG_symbols,
  REG_qqlv,
  REG_curin,
  REG_curout,
  REG_curerr,
  REG_stdin,
  REG_stdout,
  REG_stderr,

  REG_END_,
  REG_SIZE_ = REG_END_ - REG_BEGIN_
} ligReg;
/* ========================================================================= */
struct ligSECD_ {
  ligRegister regs[REG_SIZE_];
  ligMemory * memory;
  ligReader * reader;
};
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static /*@null@*/ ligCell *ligSECD_new_cell(ligSECD *, size_t);
static /*@null@*/ ligCell *ligSECD_new_cons(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_envspec(ligSECD *, ligCell *);
static /*@null@*/ ligCell *ligSECD_new_varspec(ligSECD *, ligCell *);
static /*@null@*/ ligCell *ligSECD_new_bool(ligSECD *, bool);
static /*@null@*/ ligCell *ligSECD_new_float(ligSECD *, float);
static /*@null@*/ ligCell *ligSECD_new_int(ligSECD *, intptr_t);
static /*@null@*/ ligCell *ligSECD_new_op(ligSECD *, ligOperator);
static /*@null@*/ ligCell *ligSECD_new_vector(ligSECD *, size_t);
static /*@null@*/ ligCell *ligSECD_new_constvec(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_cstr(ligSECD *, const char *, size_t);
static /*@null@*/ ligCell *ligSECD_new_symbol(ligSECD *, const char *, size_t);
static /*@null@*/ ligCell *ligSECD_new_let(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_define(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_setx(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_lambda(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_continue(ligSECD *, size_t, size_t);
static /*@null@*/ ligCell *ligSECD_new_ellipsis(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_quote(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_quasiquote(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_unquote(ligSECD *);
static /*@null@*/ ligCell *ligSECD_new_unqspl(ligSECD *);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_nop(ligSECD *);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_reg_set_imm(ligSECD *, ligReg);
static ligReturn ligSECD_reg_pop(ligSECD *, ligReg);
static ligReturn ligSECD_reg_head(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push(ligSECD *, ligReg, /*@null@*/ ligCell *);
static ligReturn ligSECD_reg_push_nil(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_cons(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_envspec(ligSECD *, ligReg, ligCell *);
static ligReturn ligSECD_reg_push_varspec(ligSECD *, ligReg, ligCell *);
static ligReturn ligSECD_reg_push_bool(ligSECD *, ligReg, bool);
static ligReturn ligSECD_reg_push_float(ligSECD *, ligReg, float);
static ligReturn ligSECD_reg_push_int(ligSECD *, ligReg, intptr_t);
static ligReturn ligSECD_reg_push_op(ligSECD *, ligReg, ligOperator);
static ligReturn ligSECD_reg_push_vector(ligSECD *, ligReg, size_t);
static ligReturn ligSECD_reg_push_constvec(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_cstr(ligSECD *, ligReg, const char *,
                                       size_t);
static ligReturn ligSECD_reg_push_symbol(ligSECD *, ligReg, const char *,
                                         size_t);
static ligReturn ligSECD_reg_push_let(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_define(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_setx(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_lambda(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_continue(ligSECD *, ligReg, size_t, size_t);
static ligReturn ligSECD_reg_push_ellipsis(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_quote(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_quasiquote(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_unquote(ligSECD *, ligReg);
static ligReturn ligSECD_reg_push_unqspl(ligSECD *, ligReg);
static ligReturn ligSECD_reg_copy(ligSECD *, ligReg);
static ligReturn ligSECD_reg_press(ligSECD *, ligReg);
static ligReturn ligSECD_reg_swap(ligSECD *, ligReg);
static ligReturn ligSECD_reg_cons(ligSECD *, ligReg);
static ligReturn ligSECD_reg_xcons(ligSECD *, ligReg);
static ligReturn ligSECD_reg_unlist(ligSECD *, ligReg);
static ligReturn ligSECD_reg_append(ligSECD *, ligReg);
static ligReturn ligSECD_reg_xappend(ligSECD *, ligReg);
static ligReturn ligSECD_reg_car(ligSECD *, ligReg);
static ligReturn ligSECD_reg_cdr(ligSECD *, ligReg);
static ligReturn ligSECD_reg_to_reg(ligSECD *, ligReg, ligReg);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_dump_push_continue(ligSECD *, size_t, size_t);
static ligReturn ligSECD_dump_save_1_2(ligSECD *);
static ligReturn ligSECD_dump_save_1_4(ligSECD *);
static ligReturn ligSECD_dump_load(ligSECD *);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_wts_splice(ligSECD *);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_quasi_rest(ligSECD *);
static ligReturn ligSECD_eval_rest(ligSECD *);
static ligReturn ligSECD_begin_impl(ligSECD *);
static ligReturn ligSECD_setx_variable(ligSECD *);
static ligReturn ligSECD_setx(ligSECD *);
static ligReturn ligSECD_find_variable(ligSECD *);
static ligReturn ligSECD_bind_rest(ligSECD *);
/* ========================================================================= */
/* clang-format off */
#define ligSECD_reg(s, r)               (s->regs[r])
#define ligSECD_reg_set(s, r, c)        (ligSECD_reg(s, r) = c)
#define ligSECD_reg_top(s, r)           ligCell_car(ligSECD_reg(s, r))
/* ------------------------------------------------------------------------- */
#define ligSECD_tmp(s)                  ligSECD_reg(s, REG_tmp)
/* ------------------------------------------------------------------------- */
#define ligSECD_code_push(s, c)         ligSECD_reg_push(s, REG_code, c)
/* ------------------------------------------------------------------------- */
#define ligSECD_dump(s)                 ligSECD_reg(s, REG_dump)
#define ligSECD_dump_set(s, c)          ligSECD_reg_set(s, REG_dump, c)
#define ligSECD_dump_top(s)             ligSECD_reg_top(s, REG_dump)
#define ligSECD_dump_pop(s)             ligSECD_reg_pop(s, REG_dump)
#define ligSECD_dump_push(s, c)         ligSECD_reg_push(s, REG_dump, c)
/* ------------------------------------------------------------------------- */
#define ligSECD_work_set(s, c)          ligSECD_reg_set(s, REG_work, c)
#define ligSECD_work_top(s)             ligSECD_reg_top(s, REG_work)
#define ligSECD_work_pop(s)             ligSECD_reg_pop(s, REG_work)
#define ligSECD_work_head(s)            ligSECD_reg_head(s, REG_work)
#define ligSECD_work_push(s, c)         ligSECD_reg_push(s, REG_work, c)
#define ligSECD_work_swap(s)            ligSECD_reg_swap(s, REG_work)
/* ------------------------------------------------------------------------- */
#define ligSECD_symbols(s)              ligSECD_reg(s, REG_symbols)
#define ligSECD_symbols_set(s, c)       ligSECD_reg_set(s, REG_symbols, c)
#define ligSECD_symbols_top(s)          ligSECD_reg_top(s, REG_symbols)
#define ligSECD_symbols_pop(s)          ligSECD_reg_pop(s, REG_symbols)
#define ligSECD_symbols_head(s)         ligSECD_reg_head(s, REG_symbols)
#define ligSECD_symbols_push(s, c)      ligSECD_reg_push(s, REG_symbols, c)
/* ------------------------------------------------------------------------- */
#define ligSECD_qqlv(s)                 ligSECD_reg(s, REG_qqlv)
#define ligSECD_curin(s)                ligSECD_reg(s, REG_curin)
#define ligSECD_curout(s)               ligSECD_reg(s, REG_curout)
#define ligSECD_curerr(s)               ligSECD_reg(s, REG_curerr)
#define ligSECD_stdin(s)                ligSECD_reg(s, REG_stdin)
#define ligSECD_stdout(s)               ligSECD_reg(s, REG_stdout)
#define ligSECD_stderr(s)               ligSECD_reg(s, REG_stderr)
/* clang-format on */
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
/* clang-format off */
/*
 * LIGEIA_OPERATOR_INFO
 *
 * argc
 *      -2:     [0 ...]
 *      -1:     [1 ...]
 *       0:     0
 *       N:     N
 */
const ligOperatorInfo LIGEIA_OPERATOR_INFO[LIGEIA_OPERATOR_SIZE_] = {
  {&ligSECD_nop,            -1, "nop",            NULL,           NULL},
  {&ligSECD_display,         1, "display",        "display",      NULL},
  /*  {&ligSECD_displayln,       1, "displayln",      "displayln",    NULL},*/
  {&ligSECD_newline,        -2, "newline",        "newline",      NULL},
  {&ligSECD_stack_pop,      -1, "stack_pop",      NULL,           NULL},
  {&ligSECD_stack_head,     -1, "stack_head",     NULL,           NULL},
  {&ligSECD_stack_copy,     -1, "stack_copy",     NULL,           NULL},
  {&ligSECD_stack_press,    -1, "stack_press",    NULL,           NULL},
  {&ligSECD_stack_swap,     -1, "stack_swap",     NULL,           NULL},
  {&ligSECD_stack_cons,     -1, "stack_cons",     NULL,           NULL},
  {&ligSECD_stack_xcons,    -1, "stack_xcons",    NULL,           NULL},
  {&ligSECD_stack_unlist,   -1, "stack_unlist",   NULL,           NULL},
  {&ligSECD_stack_append,    2, "stack_append",   "append",       NULL},
  {&ligSECD_stack_xappend,  -1, "stack_xappend",  NULL,           NULL},
  {&ligSECD_stack_car,       1, "stack_car",      "car",          NULL},
  {&ligSECD_stack_cdr,       1, "stack_cdr",      "cdr",          NULL},
  {&ligSECD_stack_set_imm,  -1, "stack_set_imm",  NULL,           NULL},
  {&ligSECD_env_pop,        -1, "env_pop",        NULL,           NULL},
  {&ligSECD_env_push_nil,   -1, "env_push_nil",   NULL,           NULL},
  {&ligSECD_dump_save_1_2,  -1, "dump_save_1_2",  NULL, "dump_save_emit"},
  {&ligSECD_dump_save_1_4,  -1, "dump_save_1_4",  NULL, "dump_save_emit"},
  {&ligSECD_dump_load,      -1, "dump_load",      NULL,           NULL},
  {&ligSECD_stack_to_work,  -1, "stack_to_work",  NULL,           NULL},
  {&ligSECD_work_to_stack,  -1, "work_to_stack",  NULL,           NULL},
  {&ligSECD_wts_splice,     -2, "wts_splice",     NULL,           NULL},
  {&ligSECD_qqlv_inc,       -1, "qqlv_inc",       NULL,           NULL},
  {&ligSECD_qqlv_dec,       -1, "qqlv_dec",       NULL,           NULL},
  {&ligSECD_quasi,          -1, "quasi",          NULL,           NULL},
  {&ligSECD_quasi_rest,     -1, "quasi_rest",     NULL,           NULL},
  {&ligSECD_eval,           -1, "eval",           NULL,           NULL},
  {&ligSECD_eval_rest,      -2, "eval_rest",      NULL,           NULL},
  {&ligSECD_call,           -1, "call",           NULL,           NULL},
  {&ligSECD_proc_pre,       -1, "proc_pre",       NULL,           NULL},
  {&ligSECD_begin_impl,     -1, "begin_impl",     NULL,           NULL},
  {&ligSECD_list,           -1, "list",           "list",         NULL},
  {&ligSECD_list_to_vector,  1, "list_to_vector", "list->vector", NULL},
  {&ligSECD_list_last,       1, "list_last",      NULL,           NULL},
  {&ligSECD_define_impl,    -1, "define_impl",    NULL,           NULL},
  {&ligSECD_define,         -1, "define",         NULL,           NULL},
  {&ligSECD_bind_rest,      -1, "bind_rest",      NULL,           NULL},
  {&ligSECD_setx,            2, "setx",           NULL,           NULL},
  {&ligSECD_find,           -1, "find",           NULL,           NULL},
  {&ligSECD_lambda,         -1, "lambda",         NULL,           NULL},
  {&ligSECD_mksynclo,       -1, "mksynclo", "make-syntactic-closure", NULL},
  {&ligSECD_mul,            -1, "mul",            "*",            NULL},
  {&ligSECD_div,            -1, "div",            "/",            NULL},
  {&ligSECD_add,            -1, "add",            "+",            NULL},
  {&ligSECD_sub,            -1, "sub",            "-",            NULL},
  {&ligSECD_stack_cons,      2, "cons",           "cons",         NULL},
};
/* clang-format on */
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
ligSECD *ligSECD_new(size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligSECD_new");
  if (0u == a_Size) {
    LIGEIA_TRACE_ALWAYS("ligSECD_new: invalid capacity");
    return NULL;
  }
  {
    /* malloc  ------------------------------------------------------------- */
    ligSECD *self = (ligSECD *)malloc(sizeof(ligSECD));
    if (!self) {
      LIGEIA_TRACE_ALWAYS("ligSECD_new: failed malloc");
      return NULL;
    }
    /* memory  ------------------------------------------------------------- */
    self->memory = ligMemory_new(a_Size);
    if (!self->memory) {
      LIGEIA_TRACE_ALWAYS("ligSECD_new: failed ligMemory_new");
      goto ligSECD_new_error_memory;
    }
    /* reader  ------------------------------------------------------------- */
    self->reader = ligReader_new();
    if (!self->reader) {
      LIGEIA_TRACE_ALWAYS("ligSECD_new: failed ligReader_new");
      goto ligSECD_new_error_reader;
    }
    /* reset  -------------------------------------------------------------- */
    if (LIGEIA_RETURN_OK != ligSECD_reset(self)) {
      LIGEIA_TRACE_ALWAYS("ligSECD_new: failed ligSECD_reset");
      goto ligSECD_new_error_reset;
    }
    return self;
  ligSECD_new_error_reset:
    ligReader_delete(self->reader);
  ligSECD_new_error_reader:
    ligMemory_delete(self->memory);
  ligSECD_new_error_memory:
    free(self);
  }
  return NULL;
}
/* ========================================================================= */
void ligSECD_delete(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_delete");
  if (!self) {
    return;
  }
  ligReader_delete(self->reader);
  self->reader = NULL;
  ligMemory_delete(self->memory);
  self->memory = NULL;
  free(self);
}
/* ========================================================================= */
ligReader *ligSECD_reader(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_reader");
  LIGEIA_ASSERT(self);
  return self->reader;
}
/* ========================================================================= */
ligReturn ligSECD_reset(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_reset");
  if (!self || !self->memory) {
    LIGEIA_TRACE_ALWAYS("ligSECD_reset: invalid args");
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  /* reg  ------------------------------------------------------------------ */
  memset(self->regs, 0, sizeof(ligCell *) * REG_SIZE_);
  /* qqlv  ----------------------------------------------------------------- */
  ligSECD_qqlv(self) = ligSECD_new_int(self, 0);
  if (ligCell_p_is_nil(ligSECD_qqlv(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_reset: failed qqlv");
    return LIGEIA_RETURN_NO_MEMORY;
  }
  /* system env  ----------------------------------------------------------- */
  ligRetExp(ligSECD_env_push_nil(self));
  { /* define system operator */
    size_t i;
    for (i = 0u; i < LIGEIA_OPERATOR_SIZE_; ++i) {
      const ligOperatorInfo *const inf = &LIGEIA_OPERATOR_INFO[i];
      if (!inf->export) {
        continue;
      }
      ligRetExp(ligSECD_stack_push_op(self, (ligOperator)i));
      ligRetExp(
          ligSECD_stack_push_symbol(self, inf->export, strlen(inf->export)));
      ligRetExp(ligSECD_define_impl(self));
    }
  }
  /* user env  ------------------------------------------------------------- */
  ligRetExp(ligSECD_env_push_nil(self));

  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
void ligSECD_print_impl(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_print");
  LIGEIA_ASSERT(self);
  printf(";;; =========================================================="
         "\n   qqlv   : ");
  ligCell_display(stdout, ligSECD_qqlv(self));
  printf("\n   work   : ");
  ligCell_display(stdout, ligSECD_work(self));
  printf("\n   stack  : ");
  ligCell_display(stdout, ligSECD_stack(self));
  printf("\n   code   : ");
  ligCell_display(stdout, ligSECD_code(self));
  printf("\n   env    : ");
  ligCell_display(stdout, ligSECD_env(self));
  printf("\n   dump   : ");
  ligCell_display(stdout, ligSECD_dump(self));
  printf("\n;;; ==========================================================\n");
  (void)fflush(stdout);
}
/* ========================================================================= */
ligReturn ligSECD_display(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_display");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));

  ligCell_display(stdout, ligSECD_stack_top(self));

  return ligSECD_stack_push_bool(self, true);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_displayln(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_display");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  {
    ligCell_display(stdout, ligSECD_stack_top(self));
    (void)putc('\n', stdout);
    (void)fflush(stdout);
  }
  return ligSECD_stack_push_bool(self, true);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_newline(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_newline");
  LIGEIA_ASSERT(self);
  (void)self;
  {
    (void)putc('\n', stdout);
    (void)fflush(stdout);
  }
  return ligSECD_stack_push_bool(self, true);
}
/* ========================================================================= */
ligReturn ligSECD_gc(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_gc");
  LIGEIA_ASSERT(self);
  return ligMemory_gc(self->memory, self->regs, REG_SIZE_);
}
/* ========================================================================= */
ligReturn ligSECD_top_level(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_top_level");
  LIGEIA_ASSERT(self);
  ligSECD_stack_set(self, NULL);
  ligSECD_code_set(self, NULL);
  ligSECD_dump_set(self, NULL);
  ligSECD_work_set(self, NULL);
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE:
        return LIGEIA_RETURN_CODE_PUSH_00;
      case LIGEIA_CST_CONS:
        return ligSECD_quasi_Cons(self);
      case LIGEIA_CST_TRUE:
        return LIGEIA_RETURN_CODE_PUSH_00;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->eval);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->quasi);
        return ligSECD_qqlv_int(self) < 1 ? LIGEIA_VTBL[cnct]->eval(self)
                                          : LIGEIA_VTBL[cnct]->quasi(self);
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_quasi_rest(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_rest");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));

  if (ligCell_p_is_nil((ligSECD_stack_top(self)))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  }

  if (ligCell_is_cons(ligSECD_stack_top(self)) &&
      ligCell_is_unqspl(ligCell_car(ligSECD_stack_top(self))) &&
      1 == ligSECD_qqlv_int(self)) {
    if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_wts_splice));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi));
      return LIGEIA_RETURN_CODE_PUSH_03;
    } else {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_wts_splice));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi));
      return LIGEIA_RETURN_CODE_PUSH_04;
    }
  } else if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi));
    return LIGEIA_RETURN_CODE_PUSH_01;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi));
    return LIGEIA_RETURN_CODE_PUSH_04;
  }
}
/* ========================================================================= */
ligReturn ligSECD_eval(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));

  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE:
        return LIGEIA_RETURN_CODE_PUSH_00;
      case LIGEIA_CST_CONS:
        return ligSECD_eval_Cons(self);
      case LIGEIA_CST_TRUE:
        return LIGEIA_RETURN_CODE_PUSH_00;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->eval);
        return LIGEIA_VTBL[cnct]->eval(self);
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_eval_rest(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_rest");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_stack(self)) ||
                ligCell_is_cons(ligSECD_stack(self)));
  if (ligCell_p_is_nil(ligSECD_stack(self))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    return LIGEIA_RETURN_CODE_PUSH_01;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    return LIGEIA_RETURN_CODE_PUSH_04;
  }
}
/* ========================================================================= */
ligReturn ligSECD_call(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_call: invalid applicable");
    return LIGEIA_RETURN_INVALID_APPLICABLE;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE:
        LIGEIA_TRACE_ALWAYS("ligSECD_call: invalid applicable");
#if defined(LIGEIA_DEBUG)
        ligSECD_print(self);
#endif
        return LIGEIA_RETURN_INVALID_APPLICABLE;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        if (!LIGEIA_VTBL[cnct]->call) {
          LIGEIA_TRACE_ALWAYS("ligSECD_call: invalid applicable");
          return LIGEIA_RETURN_INVALID_APPLICABLE;
        }
        return LIGEIA_VTBL[cnct]->call(self);
      }
    }
  }
}
/* ========================================================================= */
ligReturn ligSECD_run(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_run");
  LIGEIA_ASSERT(self);
  {
    ligReturn ret = LIGEIA_RETURN_OK;
    while (ret == LIGEIA_RETURN_OK && !ligCell_p_is_nil(ligSECD_code(self))) {
      LIGEIA_ASSERT(!ligCell_is_cons(ligSECD_code_top(self)));
      LIGEIA_ASSERT(!ligCell_is_false(ligSECD_code_top(self)));
      LIGEIA_ASSERT(!ligCell_is_strict_true(ligSECD_code_top(self)));
      {
        const ligCellNotConsType cnct = ligCell_cnct(ligSECD_code_top(self));
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->run);
        ret = LIGEIA_VTBL[cnct]->run(self);
      }
    }
    return ret;
  }
}
/* ========================================================================= */
ligReturn ligSECD_begin_impl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_begin_impl");
  LIGEIA_ASSERT(self);
  if (ligCell_p_is_nil(ligSECD_stack(self))) {
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else {
    ligReturn ret = LIGEIA_RETURN_CODE_PUSH_01;
    if (!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_begin_impl));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
      ret += 2;
    }
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    return ret;
  }
}
/* ========================================================================= */
/*@null@*/ ligCell *ligSECD_stack(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack");
  LIGEIA_ASSERT(self);
  return ligSECD_reg(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
void ligSECD_stack_set(ligSECD *self, /*@null@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_set");
  LIGEIA_ASSERT(self);
  ligSECD_reg_set(self, REG_stack, cell);
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_stack_top(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_top");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_top(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_set_imm(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_top_immutable");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_set_imm(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push(ligSECD *self, /*@NULL@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push(self, REG_stack, cell);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_nil(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_nil");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_nil(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_cons");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_cons(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_envspec(ligSECD *self, ligCell *env) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_envspec");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_envspec(self, REG_stack, env);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_varspec(ligSECD *self, ligCell *a_Cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_varspec");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_varspec(self, REG_stack, a_Cell);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_bool(ligSECD *self, bool a_Bool) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_bool");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_bool(self, REG_stack, a_Bool);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_float(ligSECD *self, float a_Float) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_float");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_float(self, REG_stack, a_Float);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_int(ligSECD *self, intptr_t a_Int) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_int");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_int(self, REG_stack, a_Int);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_op(ligSECD *self, ligOperator a_op) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_op");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_op(self, REG_stack, a_op);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_vector(ligSECD *self, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_vector");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_vector(self, REG_stack, a_Size);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_constvec(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_constvec");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_constvec(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_cstr(ligSECD *self, const char *a_Name,
                                  size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_cstr");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_cstr(self, REG_stack, a_Name, a_Len);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_symbol(ligSECD *self, const char *a_Name,
                                    size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_symbol");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_symbol(self, REG_stack, a_Name, a_Len);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_let(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_let");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_let(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_define(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_define");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_define(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_setx(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_setx");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_setx(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_lambda(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_lambda");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_lambda(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_ellipsis(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_ellipsis");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_ellipsis(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_quote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_quote");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_quote(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_quasiquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_quasiquote");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_quasiquote(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_unquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_unquote");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_unquote(self, REG_stack);
}

/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_push_unqspl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_push_unqspl");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_unqspl(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_pop(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_pop");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_pop(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_head(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_head");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_head(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_copy(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_copy");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_copy(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_press(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_press");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_press(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_swap(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_swap");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_swap(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_cons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  return ligSECD_reg_cons(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_xcons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_xcons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  return ligSECD_reg_xcons(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_unlist(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_unlist");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_unlist(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_append(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_append");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  return ligSECD_reg_append(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_xappend(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_xappend");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  return ligSECD_reg_xappend(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_car(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_car");
  LIGEIA_ASSERT(self);
  if (ligCell_p_is_nil(ligSECD_stack_top(self)) ||
      !ligCell_is_cons(ligSECD_stack_top(self))) {
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  return ligSECD_reg_car(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_cdr(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_cdr");
  LIGEIA_ASSERT(self);
  if (ligCell_p_is_nil(ligSECD_stack_top(self)) ||
      !ligCell_is_cons(ligSECD_stack_top(self))) {
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  return ligSECD_reg_cdr(self, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_to_code(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_to_code");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  return ligSECD_reg_to_reg(self, REG_stack, REG_code);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_stack_to_work(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_stack_to_work");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  return ligSECD_reg_to_reg(self, REG_stack, REG_work);
}
/* ========================================================================= */
/*@null@*/ ligCell *ligSECD_env(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_env");
  LIGEIA_ASSERT(self);
  return ligSECD_reg(self, REG_env);
}
/* ------------------------------------------------------------------------- */
void ligSECD_env_set(ligSECD *self, /*@null@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_set");
  LIGEIA_ASSERT(self);
  ligSECD_reg(self, REG_env) = cell;
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_env_top(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_top");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_top(self, REG_env);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_env_pop(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_pop");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_pop(self, REG_env);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_env_push(ligSECD *self, /*@NULL@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_push");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push(self, REG_env, cell);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_env_push_nil(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_push_nil");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_nil(self, REG_env);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_env_to_work(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_env_to_work");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_env(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env_top(self)));
  return ligSECD_reg_to_reg(self, REG_env, REG_work);
}

/* ========================================================================= */
/*@null@*/ ligCell *ligSECD_code(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_code");
  LIGEIA_ASSERT(self);
  return ligSECD_reg(self, REG_code);
}
/* ------------------------------------------------------------------------- */
void ligSECD_code_set(ligSECD *self, /*@null@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_code_set");
  LIGEIA_ASSERT(self);
  ligSECD_reg(self, REG_code) = cell;
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_code_top(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_code_top");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_top(self, REG_code);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_code_pop(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_code_pop");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_pop(self, REG_code);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_code_push_op(ligSECD *self, ligOperator a_op) {
  LIGEIA_TRACE_DEBUG("ligSECD_code_push_op");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_op(self, REG_code, a_op);
}
/* ========================================================================= */
ligReturn ligSECD_dump_push_continue(ligSECD *self, size_t n_stack,
                                     size_t n_code) {
  LIGEIA_TRACE_DEBUG("ligSECD_dump_push_continue");
  LIGEIA_ASSERT(self);
  return ligSECD_reg_push_continue(self, REG_dump, n_stack, n_code);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_dump_save_1_2(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_dump_save_1_2");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)) &&
                ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)) &&
                ligCell_is_cons(ligSECD_env(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_code(self)) &&
                ligCell_is_cons(ligSECD_code(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_code(self))) &&
                ligCell_is_cons(ligCell_cdr(ligSECD_code(self))));
  LIGEIA_ASSERT(ligCell_is_operator(ligCell_cadr(ligSECD_code(self))));
  LIGEIA_ASSERT(LIGEIA_OPERATOR_dump_load ==
                ligCell_operator(ligCell_cadr(ligSECD_code(self))));
  {
    ligRetExp(ligSECD_dump_push_continue(self, 1u, 2u));
    return LIGEIA_RETURN_OK;
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_dump_save_1_4(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_dump_save_1_4");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)) &&
                ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)) &&
                ligCell_is_cons(ligSECD_env(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_code(self)) &&
                ligCell_is_cons(ligSECD_code(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_code(self))) &&
                ligCell_is_cons(ligCell_cdr(ligSECD_code(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cddr(ligSECD_code(self))) &&
                ligCell_is_cons(ligCell_cddr(ligSECD_code(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdddr(ligSECD_code(self))) &&
                ligCell_is_cons(ligCell_cdddr(ligSECD_code(self))));
  LIGEIA_ASSERT(ligCell_is_operator(ligCell_cadddr(ligSECD_code(self))));
  LIGEIA_ASSERT(LIGEIA_OPERATOR_dump_load ==
                ligCell_operator(ligCell_cadddr(ligSECD_code(self))));
  { return ligSECD_dump_push_continue(self, 1u, 4u); }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_dump_save_emit(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_dump_save_emit");
  LIGEIA_ASSERT(self);
  { return ligSECD_dump_push_continue(self, 0u, 0u); }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_dump_load(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_dump_load");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_dump(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_dump(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_car(ligSECD_dump(self))));
  LIGEIA_ASSERT(ligCell_is_continue(ligCell_car(ligSECD_dump(self))));
  {
    ligContinue *cntn = ligCell_continue_ptr(ligSECD_dump_top(self));
    ligCell *    cell = ligSECD_stack_top(self);
    ligSECD_stack_set(self, cntn->stack);
    ligSECD_env_set(self, cntn->env);
    ligSECD_code_set(self, cntn->code);
    ligSECD_dump_set(self, cntn->dump);
    ligSECD_work_set(self, cntn->work);
    ligCell_int(ligSECD_qqlv(self)) = cntn->qqlv;
    ligSECD_curin(self)             = cntn->curin;
    ligSECD_curout(self)            = cntn->curout;
    ligSECD_curerr(self)            = cntn->curerr;
    return ligSECD_stack_push(self, cell);
  }
}
/* ========================================================================= */
/*@null@*/ ligCell *ligSECD_work(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_work");
  LIGEIA_ASSERT(self);
  return ligSECD_reg(self, REG_work);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_work_to_stack(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_work_to_stack");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_work(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_work(self)));
  return ligSECD_reg_to_reg(self, REG_work, REG_stack);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_wts_splice(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_wts_splice");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_work(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_work(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_work_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_work_top(self)));
  {
    size_t count = 0u;
    for (; !ligCell_p_is_nil(ligSECD_work_top(self)); ++count) {
      ligRetExp(ligSECD_work_head(self));
      ligRetExp(ligSECD_work_swap(self));
      LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_work_top(self)) ||
                    ligCell_is_cons(ligSECD_work_top(self)));
    }
    ligRetExp(ligSECD_work_pop(self));
    for (; 0 != count; --count) {
      ligRetExp(ligSECD_work_to_stack(self));
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_work_to_env(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_work_to_env");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_work(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_work(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_work_top(self)));
  return ligSECD_reg_to_reg(self, REG_work, REG_env);
}
/* ========================================================================= */
intptr_t ligSECD_qqlv_int(const ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_qqlv");
  LIGEIA_ASSERT(self);
  return ligCell_int(ligSECD_qqlv(self));
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_qqlv_inc(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_qqlv_inc");
  LIGEIA_ASSERT(self);
  ++ligCell_int(ligSECD_qqlv(self));
  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_qqlv_dec(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_qqlv_dec");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(0 != ligSECD_qqlv_int(self));
  --ligCell_int(ligSECD_qqlv(self));
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_list(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_list");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  ligRetExp(ligSECD_stack_push(self, ligSECD_stack(self)));
  return ligCons_set_cdr(ligSECD_stack(self), NULL);
}
/* ========================================================================= */
ligReturn ligSECD_list_to_vector(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_list_to_vector");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
  {
    ligConsResult consres =
        ligCons_result(ligSECD_stack_top(self), NULL, NULL);
    size_t len = ligConsResult_length(consres);
    size_t i   = 0u;
    LIGEIA_ASSERT(LIGEIA_CONS_TYPE_PROPER == ligConsResult_type(consres));
    ligRetExp(ligSECD_stack_push_vector(self, len));
    for (i = 0u; i < len; ++i) {
      ligRetExp(ligVector_setx(ligSECD_stack_top(self), i,
                               ligCell_caadr(ligSECD_stack(self))));
      ligRetExp(ligCons_set_car(ligCell_cdr(ligSECD_stack(self)),
                                ligCell_cdadr(ligSECD_stack(self))));
    }
    return ligSECD_stack_press(self);
  }
}
/* ========================================================================= */
ligReturn ligSECD_list_last(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_list_last");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  {
    ligCell *cell = ligCons_list_last(ligSECD_stack_top(self));
    if (!cell) {
      LIGEIA_TRACE_ALWAYS("ligSECD_list_last: invalid args");
      return LIGEIA_RETURN_INVALID_ARGS;
    }
    return ligCons_set_car(ligSECD_stack(self), cell);
  }
}
/* ========================================================================= */
ligReturn ligSECD_define_impl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_define_impl");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_symbol(ligSECD_stack_top(self)) ||
                ligCell_is_varspec(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_env_top(self)) ||
                ligCell_is_cons(ligSECD_env_top(self)));
  {
    ligRetExp(ligSECD_stack_cons(self));
    {
      ligCell *cell = ligSECD_stack(self);
      ligRetExp(ligSECD_stack_pop(self));
      ligRetExp(ligCons_set_cdr(cell, ligSECD_env_top(self)));
      ligCell_reset_immutable(ligSECD_env(self));
      ligRetExp(ligCons_set_car(ligSECD_env(self), cell));
    }
    return LIGEIA_RETURN_OK;
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_define(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_define");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_symbol(ligSECD_stack_top(self)) ||
                ligCell_is_cons(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_env_top(self)) ||
                ligCell_is_cons(ligSECD_env_top(self)));
  if (ligCell_is_symbol(ligSECD_stack_top(self))) {
    ligReturn ret = ligSECD_find_variable(self);
    switch (ret) {
      case LIGEIA_RETURN_OK:
        ligRetExp(ligSECD_setx_variable(self));
        return ligSECD_stack_push_bool(self, true);
      case LIGEIA_RETURN_UNBOUND:
        ligRetExp(ligSECD_define_impl(self));
        return ligSECD_stack_push_bool(self, true);
      default:
        return ret;
    }
  } else if (ligCell_is_cons(ligSECD_stack_top(self))) {
    ligRetExp(ligSECD_stack_head(self));
    ligRetExp(ligSECD_stack_to_work(self));
    ligRetExp(ligSECD_lambda(self));
    ligRetExp(ligSECD_work_to_stack(self));
    return ligSECD_define(self);
  } else {
    LIGEIA_TRACE_ALWAYS("Symbols are required for define");
    return LIGEIA_RETURN_INVALID_ARGS;
  }
}
/* ========================================================================= */
ligReturn ligSECD_setx_variable(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_setx_variable");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)) &&
                ligCell_is_cons(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  {
    ligRetExp(ligCons_set_cdr(ligSECD_stack_top(self),
                              ligCell_cadr(ligSECD_stack(self))));
    ligRetExp(ligSECD_stack_pop(self));
    return ligSECD_stack_pop(self);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_setx(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_setx");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_symbol(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_env_top(self)));
  {
    ligReturn ret = ligSECD_find_variable(self);
    switch (ret) {
      case LIGEIA_RETURN_OK:
        ligRetExp(ligSECD_setx_variable(self));
        return ligSECD_stack_push_bool(self, true);
      default:
        return ret;
    }
  }
}
/* ========================================================================= */
ligReturn ligSECD_find_variable(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_find_variable");
  LIGEIA_ASSERT(self);
  {
    ligCell *env = ligSECD_env(self);
    for (; !ligCell_p_is_nil(env); env = ligCell_cdr(env)) {
      LIGEIA_ASSERT(ligCell_is_cons(env));
      {
        ligCell *cons = ligCell_car(env);
        for (; !ligCell_p_is_nil(cons); cons = ligCell_cdr(cons)) {
          LIGEIA_ASSERT(ligCell_is_cons(cons));
          {
            ligCell *var = ligCell_car(cons);
            LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_car(var)) &&
                          ligCell_is_symbol(ligCell_car(var)));
            if (ligCell_is_eqv(ligSECD_stack_top(self), ligCell_car(var))) {
              ligRetExp(ligSECD_stack_pop(self));
              return ligSECD_stack_push(self, var);
            }
          }
        }
      }
    }
  }
  return LIGEIA_RETURN_UNBOUND;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_find(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_find");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_symbol(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_env(self)));
  {
    ligReturn ret = ligSECD_find_variable(self);
    switch (ret) {
      case LIGEIA_RETURN_OK:
        return ligCons_set_car(ligSECD_stack(self),
                               ligCell_cdr(ligSECD_stack_top(self)));
      case LIGEIA_RETURN_UNBOUND:
        LIGEIA_TRACE_ALWAYS("ligSECD_find: unbound symbol");
        (void)putc('\t', stdout);
        ligCell_display(stdout, ligSECD_stack_top(self));
        (void)putc('\n', stdout);
        (void)fflush(stdout);
        return LIGEIA_RETURN_UNBOUND;
      default:
        return ret;
    }
  }
}
/* ========================================================================= */
ligReturn ligSECD_bind_rest(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_bind_rest");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)) &&
                ligCell_is_cons(ligSECD_stack(self)));
  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_01;
  } else {
    LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_bind_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_define_impl));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_press));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_head));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_head));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_head));
    return LIGEIA_RETURN_CODE_PUSH_09;
  }
}
/* ========================================================================= */
ligReturn ligSECD_lambda(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_lambda");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_stack_top(self)) ||
                ligCell_is_cons(ligSECD_stack_top(self)) ||
                ligCell_is_symbol(ligSECD_stack_top(self)));
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_PROCEDURE));
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    {
      ligCell *formals = ligSECD_stack_top(self);
      ligRetExp(ligSECD_stack_pop(self));
      ligRetExp(ligProcedure_new(cell, ligSECD_env(self), formals,
                                 ligSECD_stack(self)));
      ligRetExp(ligSECD_stack_pop(self));
      ligRetExp(ligSECD_stack_push(self, cell));
      return ligSECD_proc_close(self);
    }
  }
}
/* ========================================================================= */
ligReturn ligSECD_mksynclo(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_mksynclo");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_caddr(ligSECD_stack(self))));
  if (ligCell_p_is_nil(ligCell_car(ligSECD_stack(self))) ||
      !ligCell_is_envspec(ligCell_car(ligSECD_stack(self)))) {
    LIGEIA_TRACE_ALWAYS("make-syntactic-closure: need environment specifire");
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_SYNCLO));
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    ligSynClo_new(cell, ligCell_car(ligSECD_stack(self)),
                  ligCell_cadr(ligSECD_stack(self)),
                  ligCell_caddr(ligSECD_stack(self)));
    ligRetExp(ligSECD_stack_pop(self));
    ligRetExp(ligSECD_stack_pop(self));
    ligRetExp(ligSECD_stack_pop(self));
    return ligSECD_stack_push(self, cell);
  }
}
/* ========================================================================= */
ligReturn ligSECD_mul(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_mul");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));

  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_mul: invalid args");
    return LIGEIA_RETURN_INVALID_ARGS;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE:
        LIGEIA_TRACE_ALWAYS("ligSECD_mul: invalid args");
        return LIGEIA_RETURN_INVALID_ARGS;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        if (!LIGEIA_VTBL[cnct]->foldl || !LIGEIA_VTBL[cnct]->mul) {
          LIGEIA_TRACE_ALWAYS("ligSECD_mul: invalid args");
          return LIGEIA_RETURN_INVALID_ARGS;
        }
        return LIGEIA_VTBL[cnct]->foldl(self, LIGEIA_VTBL[cnct]->mul);
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_div(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_div");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));

  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_div: invalid args");
    return LIGEIA_RETURN_INVALID_ARGS;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE:
        LIGEIA_TRACE_ALWAYS("ligSECD_div: invalid args");
        return LIGEIA_RETURN_INVALID_ARGS;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        if (!LIGEIA_VTBL[cnct]->foldl || !LIGEIA_VTBL[cnct]->div) {
          LIGEIA_TRACE_ALWAYS("ligSECD_div: invalid args");
          return LIGEIA_RETURN_INVALID_ARGS;
        }
        return LIGEIA_VTBL[cnct]->foldl(self, LIGEIA_VTBL[cnct]->div);
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_add(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_add");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));

  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_add: invalid args");
    return LIGEIA_RETURN_INVALID_ARGS;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE:
        LIGEIA_TRACE_ALWAYS("ligSECD_add: invalid args");
        return LIGEIA_RETURN_INVALID_ARGS;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        if (!LIGEIA_VTBL[cnct]->foldl || !LIGEIA_VTBL[cnct]->add) {
          LIGEIA_TRACE_ALWAYS("ligSECD_add: invalid args");
          return LIGEIA_RETURN_INVALID_ARGS;
        }
        return LIGEIA_VTBL[cnct]->foldl(self, LIGEIA_VTBL[cnct]->add);
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_sub(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_sub");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));

  if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_sub: invalid args");
    return LIGEIA_RETURN_INVALID_ARGS;
  } else {
    const ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE:
        LIGEIA_TRACE_ALWAYS("ligSECD_sub: invalid args");
        return LIGEIA_RETURN_INVALID_ARGS;
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        if (!LIGEIA_VTBL[cnct]->foldl || !LIGEIA_VTBL[cnct]->sub) {
          LIGEIA_TRACE_ALWAYS("ligSECD_sub: invalid args");
          return LIGEIA_RETURN_INVALID_ARGS;
        }
        return LIGEIA_VTBL[cnct]->foldl(self, LIGEIA_VTBL[cnct]->sub);
      }
    }
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
/*@null@*/ ligCell *ligSECD_new_cell(ligSECD *self, size_t size) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_cell");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(0 < size);
#ifdef LIGEIA_DEBUG_GC
  ligSECD_gc(self);
#endif
  return ligMemory_cell(self->memory, self->regs, REG_SIZE_, size);
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_cons");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_CONS));
    if (!cell) {
      return NULL;
    }
    ligCell_reset_immutable(cell);
    ligCell_set_cons(cell);
    ligRetExpNull(ligCons_set_car(cell, NULL));
    ligRetExpNull(ligCons_set_cdr(cell, NULL));
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_envspec(ligSECD *self, ligCell *env) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_envspec");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(env));
  {
    ligSECD_tmp(self) = env;
    {
      ligCell *cell =
          ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_ENVSPEC));
      if (!cell) {
        goto ligSECD_new_spec_exit;
      }
      ligEnvSpec_new(cell, ligSECD_tmp(self));
    ligSECD_new_spec_exit:
      ligSECD_tmp(self) = NULL;
      return cell;
    }
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_varspec(ligSECD *self, ligCell *a_Cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_varspec");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(a_Cell) && ligCell_is_cons(a_Cell));
  {
    ligSECD_tmp(self) = a_Cell;
    {
      ligCell *cell =
          ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_VARSPEC));
      if (!cell) {
        goto ligSECD_new_spec_exit;
      }
      ligVarSpec_new(cell, ligSECD_tmp(self));
    ligSECD_new_spec_exit:
      ligSECD_tmp(self) = NULL;
      return cell;
    }
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_bool(ligSECD *self, bool a_Bool) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_bool");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell = ligSECD_new_cell(
        self,
        ligCell_cnum_cst(NULL, a_Bool ? LIGEIA_CST_TRUE : LIGEIA_CST_FALSE));
    if (!cell) {
      return NULL;
    }
    a_Bool ? ligCell_set_true(cell) : ligCell_set_false(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_float(ligSECD *self, float a_Float) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_float");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_FLOAT));
    if (!cell) {
      return NULL;
    }
    ligFloat_new(cell, a_Float);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_int(ligSECD *self, intptr_t a_Int) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_int");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_INT));
    if (!cell) {
      return NULL;
    }
    ligInt_new(cell, a_Int);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_op(ligSECD *self, ligOperator a_op) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_op");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_OPERATOR));
    if (!cell) {
      return NULL;
    }
    ligOperator_new(cell, a_op);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_vector(ligSECD *self, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_vector");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell = ligSECD_new_cell(self, ligVector_cnum(a_Size));
    if (!cell) {
      return NULL;
    }
    ligVector_new(cell, a_Size);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_constvec(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_constvec");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_CONSTVEC));
    if (!cell) {
      return NULL;
    }
    ligConstVec_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_cstr(ligSECD *self, const char *a_Str,
                                     size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_cstr");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell = ligSECD_new_cell(self, ligCStr_cnum(a_Len));
    if (!cell) {
      return NULL;
    }
    ligCStr_new(cell, a_Str, a_Len);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_symbol(ligSECD *self, const char *a_Str,
                                       size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_symbol");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell = NULL;

    ligSECD_tmp(self) = ligSECD_new_cstr(self, a_Str, a_Len);
    if (!ligSECD_tmp(self)) {
      goto ligSECD_new_symbol_exit;
    }

    cell = ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_SYMBOL));
    if (!cell) {
      goto ligSECD_new_symbol_exit;
    }

    ligSymbol_new(cell, ligSECD_tmp(self));

  ligSECD_new_symbol_exit:
    ligSECD_tmp(self) = NULL;
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_let(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_let");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_LET));
    if (!cell) {
      return NULL;
    }
    ligLet_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_define(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_define");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_DEFINE));
    if (!cell) {
      return NULL;
    }
    ligDefine_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_setx(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_setx");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_SETX));
    if (!cell) {
      return NULL;
    }
    ligSetx_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_lambda(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_lambda");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_LAMBDA));
    if (!cell) {
      return NULL;
    }
    ligLambda_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_continue(ligSECD *self, size_t n_stack,
                                         size_t n_code) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_continue");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_CONTINUE));
    if (!cell) {
      return NULL;
    }
    {
      ligCell *stack = ligSECD_stack(self), *code = ligSECD_code(self);
      size_t   n;
      for (n = 0; n < n_stack; ++n) {
        stack = ligCell_cdr(stack);
      }
      for (n = 0; n < n_code; ++n) {
        code = ligCell_cdr(code);
      }
      ligContinue_new(cell, stack, ligSECD_env(self), code, ligSECD_dump(self),
                      ligSECD_work(self), ligSECD_qqlv_int(self),
                      ligSECD_curin(self), ligSECD_curout(self),
                      ligSECD_curerr(self));
      return cell;
    }
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_ellipsis(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_ellipsis");
  LIGEIA_ASSERT(self);
  {
    /* TODO(hanepjiv): ellipsis
       ligCell *cell =
       ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_DOT3));
       if (!cell) { return NULL; }
       ligEllipsis_new(cell);
       return cell;
    */
    LIGEIA_ASSERT_ALWAYS(0 && NULL != self);
    return NULL;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_quote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_quote");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_QUOTE));
    if (!cell) {
      return NULL;
    }
    ligQuote_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_quasiquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_quasiquote");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_QUASIQUOTE));
    if (!cell) {
      return NULL;
    }
    ligQuasiquote_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_unquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_unquote");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_UNQUOTE));
    if (!cell) {
      return NULL;
    }
    ligUnquote_new(cell);
    return cell;
  }
}
/* ------------------------------------------------------------------------- */
/*@null@*/ ligCell *ligSECD_new_unqspl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_new_unqspl");
  LIGEIA_ASSERT(self);
  {
    ligCell *cell =
        ligSECD_new_cell(self, ligCell_cnum_cst(NULL, LIGEIA_CST_UNQSPL));
    if (!cell) {
      return NULL;
    }
    ligUnqSpl_new(cell);
    return cell;
  }
}
/* ========================================================================= */
ligReturn ligSECD_nop(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_nop");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_reg_set_imm(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_set_imm");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg_top(self, reg)));
  if (ligCell_is_cons(ligSECD_reg_top(self, reg))) {
    ligCons_set_immutable(ligSECD_reg_top(self, reg)); /* cons */
  } else {
    ligCell_set_immutable(ligSECD_reg_top(self, reg)); /* cell */
  }
  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_pop(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_pop");
  LIGEIA_ASSERT(self);
  ligSECD_reg(self, reg) = ligCell_cdr(ligSECD_reg(self, reg));
  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_head(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_head");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg_top(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg_top(self, reg)));
  { /* strip immutable */
    ligRetExp(ligSECD_reg_copy(self, reg));
    ligRetExp(ligSECD_reg_press(self, reg));
  }
  {
    ligCell *cell = ligCell_car(ligSECD_reg_top(self, reg));
    ligRetExp(ligCons_set_car(ligSECD_reg(self, reg),
                              ligCell_cdr(ligSECD_reg_top(self, reg))));
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push(ligSECD *self, ligReg reg,
                           /*@NULL@*/ ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push");
  LIGEIA_ASSERT(self);
  {
    ligReturn ret     = LIGEIA_RETURN_OK;
    ligSECD_tmp(self) = cell;
    {
      ligCell *cons = ligSECD_new_cons(self);
      if (!cons) {
        ligGotoExp(LIGEIA_RETURN_NO_MEMORY, ret, exit_ligSECD_reg_push);
      }
      ligGotoExp(ligCons_set_car(cons, ligSECD_tmp(self)), ret,
                 exit_ligSECD_reg_push);
      ligGotoExp(ligCons_set_cdr(cons, ligSECD_reg(self, reg)), ret,
                 exit_ligSECD_reg_push);
      ligSECD_reg(self, reg) = cons;
    }
  exit_ligSECD_reg_push:
    ligSECD_tmp(self) = NULL;
    return ret;
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_nil(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_nil");
  return ligSECD_reg_push(self, reg, NULL);
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_cons(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_cons");
  {
    ligCell *cell = ligSECD_new_cons(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_envspec(ligSECD *self, ligReg reg, ligCell *env) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_envspec");
  {
    ligCell *cell = ligSECD_new_envspec(self, env);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_varspec(ligSECD *self, ligReg reg,
                                   ligCell *a_Cell) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_varspec");
  {
    ligCell *cell = ligSECD_new_varspec(self, a_Cell);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_bool(ligSECD *self, ligReg reg, bool a_Bool) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_bool");
  {
    ligCell *cell = ligSECD_new_bool(self, a_Bool);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_float(ligSECD *self, ligReg reg, float a_Float) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_float");
  {
    ligCell *cell = ligSECD_new_float(self, a_Float);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_int(ligSECD *self, ligReg reg, intptr_t a_Int) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_int");
  {
    ligCell *cell = ligSECD_new_int(self, a_Int);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_op(ligSECD *self, ligReg reg, ligOperator a_op) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_op");
  {
    ligCell *cell = ligSECD_new_op(self, a_op);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_vector(ligSECD *self, ligReg reg, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_vector");
  {
    ligCell *cell = ligSECD_new_vector(self, a_Size);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_constvec(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_constvec");
  {
    ligCell *cell = ligSECD_new_constvec(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_cstr(ligSECD *self, ligReg reg, const char *a_Name,
                                size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_cstr");
  {
    ligCell *cell = ligSECD_new_cstr(self, a_Name, a_Len);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_symbol(ligSECD *self, ligReg reg,
                                  const char *a_Name, size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_symbol");
  {
    ligCell *cell = ligSECD_new_symbol(self, a_Name, a_Len);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_let(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_let");
  {
    ligCell *cell = ligSECD_new_let(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_define(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_define");
  {
    ligCell *cell = ligSECD_new_define(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_setx(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_setx");
  {
    ligCell *cell = ligSECD_new_setx(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_lambda(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_lambda");
  {
    ligCell *cell = ligSECD_new_lambda(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_reg_push_continue(ligSECD *self, ligReg reg,
                                           size_t n_stack, size_t n_code) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_continue");
  {
    ligCell *cell = ligSECD_new_continue(self, n_stack, n_code);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_ellipsis(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_ellipsis");
  {
    ligCell *cell = ligSECD_new_ellipsis(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_quote(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_quote");
  {
    ligCell *cell = ligSECD_new_quote(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_quasiquote(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_quasiquote");
  {
    ligCell *cell = ligSECD_new_quasiquote(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_unquote(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_unquote");
  {
    ligCell *cell = ligSECD_new_unquote(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_push_unqspl(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_push_unqspl");
  {
    ligCell *cell = ligSECD_new_unqspl(self);
    if (!cell) {
      return LIGEIA_RETURN_NO_MEMORY;
    }
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_copy(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_copy");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  return ligSECD_reg_push(self, reg, ligSECD_reg_top(self, reg));
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_press(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_press");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  if (ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg)))) {
    return LIGEIA_RETURN_OK;
  }
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cdr(ligSECD_reg(self, reg))));
  return ligCons_set_cdr(ligSECD_reg(self, reg),
                         ligCell_cddr(ligSECD_reg(self, reg)));
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_swap(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_swap");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cdr(ligSECD_reg(self, reg))));
  {
    ligCell *cell = ligCell_cadr(ligSECD_reg(self, reg));
    ligRetExp(ligSECD_reg_press(self, reg));
    return ligSECD_reg_push(self, reg, cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_cons(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_cons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg))));
  {
    ligCell *cell          = ligSECD_reg(self, reg);
    ligSECD_reg(self, reg) = ligCell_cdr(cell);
    ligRetExp(ligCons_set_cdr(cell, ligCell_car(ligSECD_reg(self, reg))));
    return ligCons_set_car(ligSECD_reg(self, reg), cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_xcons(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_xcons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg))));
  {
    ligCell *cell = ligCell_cdr(ligSECD_reg(self, reg));
    ligRetExp(ligCons_set_cdr(ligSECD_reg(self, reg), ligCell_cdr(cell)));
    ligRetExp(ligCons_set_cdr(cell, ligCell_car(ligSECD_reg(self, reg))));
    return ligCons_set_car(ligSECD_reg(self, reg), cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_unlist(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_unlist");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligSECD_reg_top(self, reg)) ||
                ligCell_is_cons(ligSECD_reg_top(self, reg)));
  {
    ligSECD_reg_set(self, reg, ligSECD_reg_top(self, reg));
    return LIGEIA_RETURN_OK;
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_append(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_append");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_car(ligSECD_reg(self, reg))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_reg(self, reg))));
  {
    ligCell *cell = ligCell_cadr(ligSECD_reg(self, reg));
    ligRetExp(ligCons_append(&ligCell_car(ligSECD_reg(self, reg)), cell));
    return ligSECD_reg_press(self, reg);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_xappend(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_xappend");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, reg)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cdr(ligSECD_reg(self, reg))));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_car(ligSECD_reg(self, reg))) ||
                ligCell_is_cons(ligCell_car(ligSECD_reg(self, reg))));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_reg(self, reg))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_reg(self, reg))));
  {
    ligCell *cell = ligCell_car(ligSECD_reg(self, reg));
    ligRetExp(ligSECD_reg_pop(self, reg));
    return ligCons_append(&ligCell_car(ligSECD_reg(self, reg)), cell);
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_car(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_car");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
  return ligCons_set_car(ligSECD_reg(self, reg),
                         ligCell_car(ligSECD_reg_top(self, reg)));
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_cdr(ligSECD *self, ligReg reg) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_cdr");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
  return ligCons_set_car(ligSECD_reg(self, reg),
                         ligCell_cdr(ligSECD_reg_top(self, reg)));
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_reg_to_reg(ligSECD *self, ligReg src, ligReg dest) {
  LIGEIA_TRACE_DEBUG("ligSECD_reg_to_reg");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(src != dest);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_reg(self, src)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_reg(self, src)));
  {
    ligRetExp(ligSECD_reg_push(self, dest, ligSECD_reg_top(self, src)));
    ligRetExp(ligSECD_reg_pop(self, src));
    return LIGEIA_RETURN_OK;
  }
}
