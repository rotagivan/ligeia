/** -*- mode:bison; coding:utf-8-unix; -*-
 *  @file ligReader.lex.l
 *  @brief ligReader.lex.l
 *
 *  ligReader.lex.l
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/02
 *  @date 2018/07/19
 */

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
%option reentrant
%option prefix="ligLexer_"
%option outfile="libligeia_ligReader_la-ligReader.lex.c"
%option header-file="libligeia_ligReader_la-ligReader.lex.h"
%option never-interactive
%option noyywrap
%option stack
/* ========================================================================= */
%{
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "../debug.h"
#include "../ligTypes.h"
#include "../ligSECD.h"
#include "../ligReader.h"

#include "./ligReader.yl.h"
#include "./libligeia_ligReader_la-ligReader.yacc.h"

#define YY_USER_ACTION {                                        \
     yylocp->first_line       = yylineno;                       \
     yylocp->first_column     = yycolumn;                       \
     yycolumn                += yyleng;                         \
     yylocp->last_line        = yylineno;                       \
     yylocp->last_column      = yycolumn;                       \
}

#define LIGEIA_LIGREADER_NEWLINE do {                           \
     ++yylineno;                                                \
     yycolumn                 = 1;                              \
     yylocp->last_line        = yylineno;                       \
     yylocp->last_column      = yycolumn;                       \
} while(0)

#define LIGEIA_LIGREADER_STR_LEN(len) do {                      \
     if ((LIGEIA_LIGREADER_STRING_LENGTH - 1) < (len)) {        \
       lex_error("Too long Symbol / Text");                     \
       yyterminate();                                           \
     }                                                          \
} while(0)

#define lex_error(msg) ligParser_error(yylocp, yyscanner, secd, msg)
%}
/* ========================================================================= */
SPECIAL                         [\-!$%&*+./:<=>?@^_~]
SYMBOL_CHAR                     {SPECIAL}|[[:alnum:]]
/* ========================================================================= */
%x BLOCK_COMMENT
%x STRING_LITERAL

%%  /* ///////////////////////////////////////////////////////////////////// */

        /* white space  ==================================================== */
\n                              { LIGEIA_LIGREADER_NEWLINE; }
[[:blank:]]*
        /* line comment  =================================================== */
\;.*\n                          { LIGEIA_LIGREADER_NEWLINE; }
        /* datum comment  ================================================== */
\#\;                            { return COMMENT_DATUM; }
        /* block comment  ================================================== */
<INITIAL>\|\#                   { lex_error("The end of the block comment "
                                            "is incorrect");
                                  yyterminate(); }
<INITIAL>\#\|                   { yy_push_state(BLOCK_COMMENT, yyscanner); }
<BLOCK_COMMENT>\#\|             { yy_push_state(BLOCK_COMMENT, yyscanner); }
<BLOCK_COMMENT>\|\#             { yy_pop_state(yyscanner);}
<BLOCK_COMMENT><<EOF>>          { lex_error("EOF in comment");
                                  yyterminate(); }
<BLOCK_COMMENT>\n               { LIGEIA_LIGREADER_NEWLINE; }
<BLOCK_COMMENT>.[^\#\|\n]*
<BLOCK_COMMENT>.                { lex_error("<BLOCK_COMMENT> "
                                           "Illegal character");
                                  yyterminate(); }
        /* keywords  ======================================================= */
"#!fold-case"                   { return FOLD_CASE; }
"#!no-fold-case"                { return NO_FOLD_CASE; }
"#t"                            { yylval->bool_ = true;  return BOOL; }
"#true"                         { yylval->bool_ = true;  return BOOL; }
"#f"                            { yylval->bool_ = false; return BOOL; }
"#false"                        { yylval->bool_ = false; return BOOL; }
"#("                            { return CONST_VEC; }
"("                             { return '('; }
")"                             { return ')'; }
"nil"                           { return NIL; }
"."                             { return '.'; }
"let"                           { return LET; }
"define"                        { return DEFINE; }
"set!"                          { return SETX; }
"lambda"                        { return LAMBDA; }
"quote"                         { return QUOTE; }
"'"                             { return '\''; }
"quasiquote"                    { return QUASIQUOTE; }
"`"                             { return '`'; }
"unquote"                       { return UNQUOTE; }
","                             { return ','; }
"unquote-splicing"              { return UNQUOTE_SPLICING; }
",@"                            { return COMMA_AT; }
        /* float  ======================================================== */
([+-]?[[:digit:]]*\.[[:digit:]]+) |
([+-]?[[:digit:]]+\.[[:digit:]]*) {
                                  yylval->float_ = atof(yytext);
                                  return FLOAT; }
        /* integer  ======================================================== */
[+-]?[[:digit:]]+               { yylval->intptr_t_ = atoi(yytext);
                                  return INTEGER; }
        /* symbol  ========================================================= */
{SYMBOL_CHAR}+                  { LIGEIA_LIGREADER_STR_LEN(yyleng);
                                  strncpy(yylval->text.str, yytext, yyleng);
                                  yylval->text.len = yyleng;
                                  yylval->text.str[yylval->text.len] = '\0';
                                  return SYMBOL; }
        /* string literal  ================================================= */
<INITIAL>\"                     { yylval->text.len = 0;
                                  yy_push_state(STRING_LITERAL, yyscanner); }
<STRING_LITERAL>\"              {
        LIGEIA_LIGREADER_STR_LEN(yylval->text.len + yyleng - 1);
        strncpy(yylval->text.str + yylval->text.len, yytext, yyleng - 1);
        yylval->text.len += yyleng - 1;
        yylval->text.str[yylval->text.len] = '\0';
        yy_pop_state(yyscanner);
        return STRING;
}
<STRING_LITERAL>\n              { lex_error("Unterminated string");
                                  yyterminate(); }
<STRING_LITERAL>\\[\"\\nrftab0] {
        LIGEIA_LIGREADER_STR_LEN(yylval->text.len + yyleng - 1);
        strncpy(yylval->text.str + yylval->text.len, yytext, yyleng - 2);
        yylval->text.len += yyleng - 2;
        switch (yytext[yyleng - 1]) {
          case '"' : yylval->text.str[yylval->text.len] = '"';  break;
          case '\\': yylval->text.str[yylval->text.len] = '\\'; break;
          case 'n' : yylval->text.str[yylval->text.len] = '\n'; break;
          case 'r' : yylval->text.str[yylval->text.len] = '\r'; break;
          case 'f' : yylval->text.str[yylval->text.len] = '\f'; break;
          case 't' : yylval->text.str[yylval->text.len] = '\t'; break;
          case 'a' : yylval->text.str[yylval->text.len] = '\a'; break;
          case 'b' : yylval->text.str[yylval->text.len] = '\b'; break;
          case '0' : yylval->text.str[yylval->text.len] = '\0'; break;
          default: lex_error("Unknown escape"); yyterminate();  break;
        }
        ++yylval->text.len;
        yylval->text.str[yylval->text.len] = '\0';
}
<STRING_LITERAL>\\[[:blank:]]*\n[[:blank:]]* {
        size_t len = yyleng;
        for (char c = yytext[len-1]; c != '\\'; --len, c = yytext[len-1]) {}
        --len;
        LIGEIA_LIGREADER_STR_LEN(yylval->text.len + len);
        strncpy(yylval->text.str+yylval->text.len, yytext, len);
        yylval->text.len += len;
        yylval->text.str[yylval->text.len] = '\0';
        LIGEIA_LIGREADER_NEWLINE;
}
<STRING_LITERAL><<EOF>>         { lex_error("EOF in string");
                                  yyterminate(); }
<STRING_LITERAL>[^\"\n\\]*      { yymore(); }
<STRING_LITERAL>.               { yytext[yyleng-1] = '\0';
                                  lex_error("<STRING_LITERAL> "
                                            "Illegal character");
                                  yyterminate(); }
        /* illegal  ======================================================== */
.                               { lex_error("Illegal character");
                                  yyterminate(); }

%%  /* ///////////////////////////////////////////////////////////////////// */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  ligReader* ligReader_new(void) {
    yyscan_t yyscanner;
    if (0 != ligLexer_lex_init(&yyscanner) || NULL == yyscanner) {
      LIGEIA_TRACE_ALWAYS("error! : fail ligLexer_lex_init");
      return NULL;
    }
    return (ligReader*)yyscanner;
  }
  /* ======================================================================= */
  void ligReader_delete(ligReader *self) {
    if (!self) { return; }
    ligLexer_lex_destroy((yyscan_t)self);
  }

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */
