/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligReader.yl.h
 *  @brief ligReader.yl.h
 *
 *  ligReader.yl.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/03
 *  @date 2018/06/02
 */

#ifndef LIGEIA_LIGREADER_LIGREADER_YL_H_
#define LIGEIA_LIGREADER_LIGREADER_YL_H_

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
# define LIGEIA_LIGREADER_STRING_LENGTH (256)
  /* ======================================================================= */
# ifndef YY_TYPEDEF_YY_SCANNER_T
# define YY_TYPEDEF_YY_SCANNER_T typedef void* yyscan_t
  YY_TYPEDEF_YY_SCANNER_T;
# endif
  /* ======================================================================= */
  union YYSTYPE;
  struct YYLTYPE;
  /* ======================================================================= */
# undef YY_DECL
# define YY_DECL int ligParser_lex(union YYSTYPE *yylval,               \
                                   struct YYLTYPE *yylocp,              \
                                   yyscan_t yyscanner, ligSECD *secd)
  extern YY_DECL;
  /* ======================================================================= */
  extern void ligParser_error(const struct YYLTYPE * const yylocp,
                              yyscan_t yyscanner, ligSECD *secd,
                              const char *msg);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGREADER_LIGREADER_YL_H_ */
