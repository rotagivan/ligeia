/** -*- mode:bison; coding:utf-8-unix; -*-
 *  @file ligReader.yacc.y
 *  @brief ligReader.yacc.y
 *
 *  ligReader.yacc.y
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/02
 *  @date 2018/07/26
 */

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
%{
# ifdef HAVE_CONFIG_H
#   include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdio.h>
# include <math.h>
# include <string.h>

# include "../debug.h"
# include "../ligTypes.h"
# include "../ligReturn.h"
# include "../ligReader.h"
# include "../ligCell.h"
# include "../ligCell/ligOperator.h"
# include "../ligSECD.h"

# include "./ligReader.yl.h"
# include "./libligeia_ligReader_la-ligReader.lex.h"

# define ligRetYYABORT___(expr, line, rettype, retval)                  \
do {                                                                    \
  rettype retval = (expr);                                              \
  if (LIGEIA_RETURN_OK != retval) {                                     \
    fprintf(stderr, PACKAGE_STRING ": " __FILE__ "(" #line "): "        \
            "parser error! (%i, %i): %s\n",                             \
            ligLexer_get_lineno(yyscanner),                             \
            ligLexer_get_column(yyscanner),                             \
            ligReturnStrings[retval]);                                  \
    (void) fflush(stderr);                                              \
    YYABORT;                                                            \
  }                                                                     \
} while (0)
# define ligRetYYABORT__(expr, line)    \
        ligRetYYABORT___(expr, line, ligReturn, retval_##line)
# define ligRetYYABORT_(expr, line)     ligRetYYABORT__(expr, line)
# define ligRetYYABORT(expr)            ligRetYYABORT_((expr), __LINE__)
%}
/* ========================================================================= */
%define api.pure full
%locations
%parse-param            { yyscan_t yyscanner }
%lex-param              { yyscan_t yyscanner }
%param                  { ligSECD *secd }
/* ========================================================================= */
%union {
  size_t                size_t_;
  struct {
    char                        str[LIGEIA_LIGREADER_STRING_LENGTH];
    size_t                      len;
  }                     text;
  bool                  bool_;
  float                 float_;
  intptr_t              intptr_t_;
}
/* ========================================================================= */
%token                  COMMENT_DATUM
%token                  NIL
%token  <text>          STRING
%token  <text>          SYMBOL
%token  <float_>        FLOAT
%token  <intptr_t_>     INTEGER
%token  <bool_>         BOOL
%token                  FOLD_CASE
%token                  NO_FOLD_CASE
%token                  CONST_VEC
%token                  LET
%token                  DEFINE
%token                  SETX
%token                  LAMBDA
%token                  QUOTE
%token                  QUASIQUOTE
%token                  UNQUOTE
%token                  UNQUOTE_SPLICING
%token                  COMMA_AT
/* ========================================================================= */
%type   <size_t_>       datum
%type   <size_t_>       list
%type   <size_t_>       list_tail

%%  /* ///////////////////////////////////////////////////////////////////// */

/* ========================================================================= */
input
: expr_tail
;
/* ========================================================================= */
expr_tail
:
| expr expr_tail
;
/* ========================================================================= */
expr
: directive
| datum                 {
  if (0 != $1) {
    ligRetYYABORT(ligSECD_code_push_op(secd, LIGEIA_OPERATOR_stack_press));
    ligRetYYABORT(ligSECD_code_push_op(secd, LIGEIA_OPERATOR_dump_load));
    ligRetYYABORT(ligSECD_code_push_op(secd, LIGEIA_OPERATOR_eval));
    ligRetYYABORT(ligSECD_code_push_op(secd, LIGEIA_OPERATOR_dump_save_1_2));
    ligRetYYABORT(ligSECD_run(secd));
  }
}
;
/* ========================================================================= */
directive
: FOLD_CASE
| NO_FOLD_CASE
;
/* ========================================================================= */
datum
: comment_datum         { $$ = 0; }
| cell                  { $$ = 1; }
;
/* ========================================================================= */
comment_datum
: COMMENT_DATUM cell    { ligRetYYABORT(ligSECD_stack_pop(secd)); }
;
/* ========================================================================= */
cell
: NIL                   { ligRetYYABORT(ligSECD_stack_push_nil(secd)); }
| STRING                { ligRetYYABORT(ligSECD_stack_push_cstr(secd, $1.str,
                                                                $1.len)); }
| SYMBOL                { ligRetYYABORT(ligSECD_stack_push_symbol(secd, $1.str,
                                                                  $1.len)); }
| BOOL                  { ligRetYYABORT(ligSECD_stack_push_bool(secd, $1)); }
| FLOAT                 { ligRetYYABORT(ligSECD_stack_push_float(secd,
                                                                 (float)$1)); }
| INTEGER               { ligRetYYABORT(ligSECD_stack_push_int(secd,
                                                              (intptr_t)$1)); }
| LET                   { ligRetYYABORT(ligSECD_stack_push_let(secd)); }
| DEFINE                { ligRetYYABORT(ligSECD_stack_push_define(secd)); }
| SETX                  { ligRetYYABORT(ligSECD_stack_push_setx(secd)); }
| LAMBDA                { ligRetYYABORT(ligSECD_stack_push_lambda(secd)); }
| QUOTE                 { ligRetYYABORT(ligSECD_stack_push_quote(secd)); }
| QUASIQUOTE            { ligRetYYABORT(ligSECD_stack_push_quasiquote(secd)); }
| UNQUOTE               { ligRetYYABORT(ligSECD_stack_push_unquote(secd)); }
| UNQUOTE_SPLICING      { ligRetYYABORT(ligSECD_stack_push_unqspl(secd)); }
| '\'' cell             { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          ligRetYYABORT(ligSECD_stack_xcons(secd));
                          ligRetYYABORT(ligSECD_stack_push_quote(secd));
                          ligRetYYABORT(ligSECD_stack_cons(secd)); }
| '`' cell              { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          ligRetYYABORT(ligSECD_stack_xcons(secd));
                          ligRetYYABORT(ligSECD_stack_push_quasiquote(secd));
                          ligRetYYABORT(ligSECD_stack_cons(secd)); }
| ',' cell              { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          ligRetYYABORT(ligSECD_stack_xcons(secd));
                          ligRetYYABORT(ligSECD_stack_push_unquote(secd));
                          ligRetYYABORT(ligSECD_stack_cons(secd)); }
| COMMA_AT cell         { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          ligRetYYABORT(ligSECD_stack_xcons(secd));
                          ligRetYYABORT(ligSECD_stack_push_unqspl(secd));
                          ligRetYYABORT(ligSECD_stack_cons(secd)); }
| '(' list              { for (; 0 < $2; --$2) {
                            ligRetYYABORT(ligSECD_stack_xcons(secd));
                          } }
| CONST_VEC list        { for (; 0 < $2; --$2) {
                            ligRetYYABORT(ligSECD_stack_xcons(secd));
                          }
                          ligRetYYABORT(ligSECD_stack_push_constvec(secd));
                          ligRetYYABORT(ligSECD_stack_cons(secd)); }
;
/* ========================================================================= */
list
: ')'                   { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          $$ = 0; }
| comment_datum list    { $$ = $2; }
| cell list_tail        { $$ = 1 + $2; }
;
/* ========================================================================= */
list_tail
: ')'                   { ligRetYYABORT(ligSECD_stack_push_nil(secd));
                          $$ = 0; }
| '.' improper_tail     { $$ = 0; }
| datum list_tail       { $$ = $1 + $2; }
;
/* ========================================================================= */
improper_tail
: comment_datum improper_tail
| cell ')'
;

%%  /* ///////////////////////////////////////////////////////////////////// */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  ligReturn ligSECD_load_file(ligSECD *self, FILE *file, int close) {
    LIGEIA_ASSERT(self);
    LIGEIA_ASSERT(ligSECD_reader(self));
    if (NULL == file) {
      LIGEIA_TRACE_ALWAYS("ligSECD_load_file: invalid file");
      return LIGEIA_RETURN_INVALID_ARGS;
    }
    {
      ligReturn ret = LIGEIA_RETURN_UNKNOWN;
      {
        ligGotoExp(ligSECD_top_level(self), ret, final);
      }
      {
        yyscan_t yyscanner = (yyscan_t)ligSECD_reader(self);
        ligLexer_restart(file, yyscanner);
        ret = (0 == yyparse(yyscanner, self) ?
               LIGEIA_RETURN_OK : LIGEIA_RETURN_PARSE_ERROR);
      }
   final:
      if (0 != close) {
        if (0 != fclose(file)) {
          fprintf(stderr, "ligSECD_load_file: failed fclose\n");
          (void) fflush(stderr);
          /* ignore fclose error */
        }
      }
      return ret;
    }
  }
  /* ======================================================================= */
  ligReturn ligSECD_load_path(ligSECD *self, const char* path) {
    LIGEIA_ASSERT(self);
    LIGEIA_ASSERT(ligSECD_reader(self));
    if (NULL == path) {
      fprintf(stderr, "ligSECD_load_path: path == NULL\n");
      (void) fflush(stderr);
      return LIGEIA_RETURN_INVALID_ARGS;
    }
    {
      FILE *file = fopen(path, "r");
      if (NULL == file) {
        fprintf(stderr, "ligSECD_load_path: failed open \"%s\"\n", path);
        (void) fflush(stderr);
        return LIGEIA_RETURN_FAILED_FOPEN;
      }
      return ligSECD_load_file(self, file, 1);
    }
  }
  /* ======================================================================= */
  ligReturn ligSECD_load_str(ligSECD *self, const char* str) {
    LIGEIA_ASSERT(self);
    LIGEIA_ASSERT(ligSECD_reader(self));
    LIGEIA_ASSERT(str);
    ligRetExp(ligSECD_top_level(self));
    {
      yyscan_t yyscanner = (yyscan_t)ligSECD_reader(self);
      ligLexer__switch_to_buffer(ligLexer__scan_string(str, yyscanner),
                                  yyscanner);
      return 0 == yyparse(yyscanner, self) ?
          LIGEIA_RETURN_OK : LIGEIA_RETURN_PARSE_ERROR;
    }
  }
  /* ======================================================================= */
  void ligParser_error(const struct YYLTYPE * const yylocp,
                       yyscan_t yyscanner, ligSECD *secd, const char *msg) {
    (void) yylocp;
    (void) secd;

    fprintf(stderr, "ligParser error! (%i, %i): %s\n",
            ligLexer_get_lineno(yyscanner), ligLexer_get_column(yyscanner),
            msg);
    (void) fflush(stderr);
  }

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */
