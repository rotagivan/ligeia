/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligReader.c
 *  @brief ligReader.c
 *
 *  ligReader.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/02
 *  @date 2017/07/02
 */

#include "./ligReader.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
