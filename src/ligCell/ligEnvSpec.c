/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligEnvSpec.c
 *  @brief ligEnvSpec.c
 *
 *  ligEnvSpec.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/29
 *  @date 2018/07/04
 */

#include "./ligEnvSpec.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <stdio.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligEnvSpec_cnum_cst(/*@null@*/ const ligCell*);
static void             ligEnvSpec_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligEnvSpec_migration(ligCell*, ligMemory*);
static bool             ligEnvSpec_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_EnvSpec(ligSECD*);
static ligReturn        ligSECD_eval_EnvSpec(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligEnvSpec_VTBL = {
  &ligEnvSpec_cnum_cst,
  &ligEnvSpec_display,
  &ligEnvSpec_migration,
  &ligEnvSpec_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_EnvSpec,
  &ligSECD_eval_EnvSpec,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligEnvSpec_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligEnvSpec_cnum_cst");
  LIGEIA_ASSERT(ligCell_p_is_nil(self) || !ligCell_p_is_nil(self));
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligEnvSpec_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligEnvSpec_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  (void) fprintf(f, "#<environment specifier>");
}
/* ========================================================================= */
ligReturn ligEnvSpec_migration(/*@unused@*/ ligCell *self,
                               /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligEnvSpec_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  return ligMemory_migration(memory, &(ligCell_envspec_env(self)));
}
/* ========================================================================= */
bool ligEnvSpec_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligEnvSpec_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_envspec(l));
  LIGEIA_ASSERT(0); /* TODO */
  return (ligCell_is_envspec(r) &&
          (ligCell_envspec_env(l) == ligCell_envspec_env(r)));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_EnvSpec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_EnvSpec");
  LIGEIA_ASSERT(self);
  (void) self;
  LIGEIA_ASSERT(0); /* TODO */
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_EnvSpec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_EnvSpec");
  LIGEIA_ASSERT(self);
  (void) self;
  LIGEIA_ASSERT(0); /* TODO */
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligEnvSpec_new(ligCell *self, ligCell* a_cell) {
  LIGEIA_TRACE_DEBUG("ligEnvSpec_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(!ligCell_p_is_nil(a_cell) && ligCell_is_cons(a_cell));
  {
    ligCell_set_envspec(self);
    ligCell_envspec_env(self) = a_cell;
  }
}
