/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligUnqSpl.c
 *  @brief ligUnqSpl.c
 *
 *  ligUnqSpl.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/02
 */

#include "./ligUnqSpl.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligUnqSpl_cnum_cst(/*@null@*/ const ligCell*);
static void             ligUnqSpl_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligUnqSpl_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_eval_UnqSpl(ligSECD*);
static ligReturn        ligSECD_call_UnqSpl(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligUnqSpl_VTBL = {
  &ligUnqSpl_cnum_cst,
  &ligUnqSpl_display,
  &ligUnqSpl_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  NULL,         /* quasi */
  &ligSECD_eval_UnqSpl,
  &ligSECD_call_UnqSpl,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligUnqSpl_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnqSpl_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligUnqSpl_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnqSpl_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "unquote-unqspl");
}
/* ========================================================================= */
ligReturn ligUnqSpl_migration(/*@unused@*/ ligCell *self,
                                /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligUnqSpl_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_eval_UnqSpl(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_UnqSpl");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_UnqSpl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_UnqSpl");
  LIGEIA_ASSERT(self);
  if (ligSECD_qqlv_int(self) < 1) {
    LIGEIA_TRACE_ALWAYS("unquote unqspl "
                        "appear with invalid quasiquote level.");
    return LIGEIA_RETURN_QQ_INVALID_LV;
  } else if (ligSECD_qqlv_int(self) == 1) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    return LIGEIA_RETURN_CODE_PUSH_05;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    return LIGEIA_RETURN_CODE_PUSH_07;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligUnqSpl_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnqSpl_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_unqspl(self);
  }
}
