/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligVarSpec.h
 *  @brief ligVarSpec.h
 *
 *  ligVarSpec.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2018/06/09
 *  @date 2018/07/03
 */

#ifndef LIGEIA_LIGCELL_LIGVARSPEC_H_
#define LIGEIA_LIGCELL_LIGVARSPEC_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligVarSpec_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligVarSpec_new(ligCell*, ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGVARSPEC_H_ */
