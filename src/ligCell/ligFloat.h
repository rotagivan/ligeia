/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligFloat.h
 *  @brief ligFloat.h
 *
 *  ligFloat.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2018/05/22
 */

#ifndef LIGEIA_LIGCELL_LIGFLOAT_H_
#define LIGEIA_LIGCELL_LIGFLOAT_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligFloat_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligFloat_new(ligCell*, float);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGFLOAT_H_ */
