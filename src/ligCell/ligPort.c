/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligPort.c
 *  @brief ligPort.c
 *
 *  ligPort.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/10
 *  @date 2017/07/15
 */

#include "./ligPort.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include "../debug.h"
