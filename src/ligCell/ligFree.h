/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligFree.h
 *  @brief ligFree.h
 *
 *  ligFree.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2017/06/28
 */

#ifndef LIGEIA_LIGCELL_LIGFREE_H_
#define LIGEIA_LIGCELL_LIGFREE_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligFree_VTBL;

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGFREE_H_ */
