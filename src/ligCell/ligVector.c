/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligVector.c
 *  @brief ligVector.c
 *
 *  ligVector.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/19
 *  @date 2018/06/29
 */

#include "./ligVector.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligVector_cnum_cst(/*@null@*/ const ligCell*);
static void             ligVector_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligVector_migration(ligCell*, ligMemory*);
static bool             ligVector_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_Vector(ligSECD*);
static ligReturn        ligSECD_eval_Vector(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligVector_VTBL = {
  &ligVector_cnum_cst,
  &ligVector_display,
  &ligVector_migration,
  &ligVector_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_Vector,
  &ligSECD_eval_Vector,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligVector_cnum_cst(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligVector_cnum_cst");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  return ligVector_cnum(ligCell_vector_ptr(self)->size);
}
/* ========================================================================= */
void ligVector_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligVector_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  fprintf(f, "#(");
  {
    const ligVector* const v    = ligCell_vector_ptr(self);
    size_t i                    = 1u;
    ligCell_display(f, v->vals[0]);
    for (; i < v->size; ++i) {
      (void) putc(' ', f);
      ligCell_display(f, v->vals[i]);
    }
  }
  (void) putc(')', f);
}
/* ========================================================================= */
ligReturn ligVector_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligVector_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  {
    ligVector* const v  = ligCell_vector_ptr(self);
    size_t i            = 0u;
    for (; i < v->size; ++i) {
      ligRetExp(ligMemory_migration(memory, &v->vals[i]));
    }
    return LIGEIA_RETURN_OK;
  }
}
/* ========================================================================= */
bool ligVector_is_eqv(/*@unused@*/ const ligCell *l,
                      /*@unused@*/ const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligVector_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_vector(l));
  LIGEIA_ASSERT_ALWAYS(0);
  (void) l;
  (void) r;
  /* TODO(hanepjiv): UNDERCONSTRUCT */
  return false;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Vector(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Vector");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Vector(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Vector");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligVector_new(ligCell *self, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligVector_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(0 < a_Size);
  {
    ligCell_set_vector(self);
    ligCell_vector_ptr(self)->size = a_Size;
    memset(ligCell_vector_ptr(self)->vals, 0, sizeof(ligCell*) * a_Size);
  }
}
/* ========================================================================= */
size_t ligVector_cnum(size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligVector_cnum");
  LIGEIA_ASSERT(0 < a_Size);
  --a_Size;                     /* value[0] */
  a_Size *= sizeof(ligCell*);
  a_Size += sizeof(ligVector);
  a_Size -= ligCell_value_size;
  return ligCell_cnum_size(a_Size);
}
/* ========================================================================= */
ligReturn ligVector_setx(ligCell *self, size_t idx, ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligVector_setx");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_vector(self));
  if (ligCell_is_immutable(self)) {
    LIGEIA_TRACE_ALWAYS("ERROR!: ligVector_setx: immutable.");
    ligCell_display(stderr, self);
    (void) putc('\n', stderr);
    (void) fflush(stderr);
    return LIGEIA_RETURN_IMMUTABLE;
  }
  {
    ligVector* const v = ligCell_vector_ptr(self);
    if (v->size <= idx) {
      LIGEIA_TRACE_ALWAYS("ERROR!: ligVector_setx: invalid index.");
      return LIGEIA_RETURN_INVALID_ARGS;
    }
    v->vals[idx] = ligCell_p_notag(cell);
    return LIGEIA_RETURN_OK;
  }
}
/* ========================================================================= */
size_t ligVector_size_impl(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligVector_setx");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_vector(self));
  return ligCell_vector_ptr(self)->size;
}
