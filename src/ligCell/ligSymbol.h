/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSymbol.h
 *  @brief ligSymbol.h
 *
 *  ligSymbol.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/10
 *  @date 2018/06/29
 */

#ifndef LIGEIA_LIGCELL_LIGSYMBOL_H_
#define LIGEIA_LIGCELL_LIGSYMBOL_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct ligSymbol_ {
    ligCell*    name;
    uint32_t    hash;
  } ligSymbol;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligSymbol_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligSymbol_new(ligCell*, ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGSYMBOL_H_ */
