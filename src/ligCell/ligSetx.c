/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSetx.c
 *  @brief ligSetx.c
 *
 *  ligSetx.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/02
 */

#include "./ligSetx.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligSetx_cnum_cst(/*@null@*/ const ligCell*);
static void             ligSetx_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligSetx_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_quasi_Setx(ligSECD*);
static ligReturn        ligSECD_eval_Setx(ligSECD*);
static ligReturn        ligSECD_call_Setx(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligSetx_VTBL = {
  &ligSetx_cnum_cst,
  &ligSetx_display,
  &ligSetx_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  &ligSECD_quasi_Setx,
  &ligSECD_eval_Setx,
  &ligSECD_call_Setx,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligSetx_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSetx_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligSetx_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSetx_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "setx");
}
/* ========================================================================= */
ligReturn ligSetx_migration(/*@unused@*/ ligCell *self,
                           /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligSetx_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Setx(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Setx");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Setx(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Setx");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Setx(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Setx");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_setx(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_setx));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_06;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligSetx_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSetx_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_setx(self);
  }
}
