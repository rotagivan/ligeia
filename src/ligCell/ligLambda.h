/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligLambda.h
 *  @brief ligLambda.h
 *
 *  ligLambda.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/31
 *  @date 2018/05/22
 */

#ifndef LIGEIA_LIGCELL_LIGLAMBDA_H_
#define LIGEIA_LIGCELL_LIGLAMBDA_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligLambda_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligLambda_new(ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGLAMBDA_H_ */
