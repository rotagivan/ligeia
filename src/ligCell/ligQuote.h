/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligQuote.h
 *  @brief ligQuote.h
 *
 *  ligQuote.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2017/07/12
 */

#ifndef LIGEIA_LIGCELL_LIGQUOTE_H_
#define LIGEIA_LIGCELL_LIGQUOTE_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligQuote_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligQuote_new(ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGQUOTE_H_ */
