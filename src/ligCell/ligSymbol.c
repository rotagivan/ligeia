/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSymbol.c
 *  @brief ligSymbol.c
 *
 *  ligSymbol.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/10
 *  @date 2018/06/29
 */

#include "./ligSymbol.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligHash.h"
#include "../ligReturn.h"
#include "../ligCell.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "./ligCStr.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligSymbol_cnum_cst(/*@null@*/ const ligCell*);
static void             ligSymbol_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligSymbol_migration(ligCell*, ligMemory*);
static bool             ligSymbol_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_Symbol(ligSECD*);
static ligReturn        ligSECD_eval_Symbol(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligSymbol_VTBL = {
  &ligSymbol_cnum_cst,
  &ligSymbol_display,
  &ligSymbol_migration,
  &ligSymbol_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_Symbol,
  &ligSECD_eval_Symbol,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligSymbol_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSymbol_cnum_cst");
  (void) self;
  return ligCell_cnum_size(sizeof(ligSymbol) - ligCell_value_size);
}
/* ========================================================================= */
void ligSymbol_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSymbol_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "%s", ligCell_cstr(ligCell_symbol_ptr(self)->name));
}
/* ========================================================================= */
ligReturn ligSymbol_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligSymbol_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  return ligMemory_migration(memory, &(ligCell_symbol_ptr(self)->name));
}
/* ========================================================================= */
bool ligSymbol_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligSymbol_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_symbol(l));
  return (ligCell_is_symbol(r) &&
          (ligCell_symbol_ptr(l)->hash == ligCell_symbol_ptr(r)->hash) &&
          ligCStr_VTBL.is_eqv(ligCell_symbol_ptr(l)->name,
                              ligCell_symbol_ptr(r)->name));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Symbol(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Symbol");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Symbol(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Symbol");
  LIGEIA_ASSERT(self);
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_find));
  return LIGEIA_RETURN_CODE_PUSH_01;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligSymbol_new(ligCell *self, ligCell* a_CStr) {
  LIGEIA_TRACE_DEBUG("ligSymbol_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(!ligCell_p_is_nil(a_CStr));
  ligCell_set_symbol(self);
  ligCell_symbol_ptr(self)->name = a_CStr;
  ligCell_symbol_ptr(self)->hash =
      ligHash_combine(0u, (uint8_t*)ligCell_cstr(a_CStr),
                      strlen(ligCell_cstr(a_CStr)));
}
