/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligInt.h
 *  @brief ligInt.h
 *
 *  ligInt.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2017/07/29
 */

#ifndef LIGEIA_LIGCELL_LIGINT_H_
#define LIGEIA_LIGCELL_LIGINT_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligInt_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligInt_new(ligCell*, intptr_t);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGINT_H_ */
