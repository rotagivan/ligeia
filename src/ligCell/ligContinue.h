/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligContinue.h
 *  @brief ligContinue.h
 *
 *  ligContinue.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/02
 */

#ifndef LIGEIA_LIGCELL_LIGCONTINUE_H_
#define LIGEIA_LIGCELL_LIGCONTINUE_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct ligContinue_ {
    ligCell     *stack;
    ligCell     *env;
    ligCell     *code;
    ligCell     *dump;
    ligCell     *work;
    intptr_t    qqlv;
    ligCell     *curin;
    ligCell     *curout;
    ligCell     *curerr;
  } ligContinue;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligContinue_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligContinue_new(ligCell *self,
                                            ligCell *a_stack,
                                            ligCell *a_env,
                                            ligCell *a_code,
                                            ligCell *a_dump,
                                            ligCell *a_work,
                                            intptr_t a_qqlv,
                                            ligCell *a_curin,
                                            ligCell *a_curout,
                                            ligCell *a_curerr);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGCONTINUE_H_ */
