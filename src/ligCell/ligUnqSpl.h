/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligUnqSpl.h
 *  @brief ligUnqSpl.h
 *
 *  ligUnqSpl.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2017/07/12
 */

#ifndef LIGEIA_LIGCELL_LIGSPLICING_H_
#define LIGEIA_LIGCELL_LIGSPLICING_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligUnqSpl_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligUnqSpl_new(ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGSPLICING_H_ */
