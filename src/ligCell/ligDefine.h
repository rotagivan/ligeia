/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligDefine.h
 *  @brief ligDefine.h
 *
 *  ligDefine.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/31
 *  @date 2018/05/22
 */

#ifndef LIGEIA_LIGCELL_LIGDEFINE_H_
#define LIGEIA_LIGCELL_LIGDEFINE_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligDefine_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligDefine_new(ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGDEFINE_H_ */
