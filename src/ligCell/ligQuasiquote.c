/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligQuasiquote.c
 *  @brief ligQuasiquote.c
 *
 *  ligQuasiquote.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/19
 */

#include "./ligQuasiquote.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligQuasiquote_cnum_cst(/*@null@*/ const ligCell *);
static void      ligQuasiquote_display(FILE *,
                                       /*@null@*/ const ligCell *);
static ligReturn ligQuasiquote_migration(ligCell *, ligMemory *);
static ligReturn ligSECD_quasi_Quasiquote(ligSECD *);
static ligReturn ligSECD_eval_Quasiquote(ligSECD *);
static ligReturn ligSECD_call_Quasiquote(ligSECD *);
/* ========================================================================= */
const LIGEIA_VTBL_t ligQuasiquote_VTBL = {
    &ligQuasiquote_cnum_cst,
    &ligQuasiquote_display,
    &ligQuasiquote_migration,
    NULL, /* is_eqv */
    NULL, /* expand */
    &ligSECD_quasi_Quasiquote,
    &ligSECD_eval_Quasiquote,
    &ligSECD_call_Quasiquote,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligQuasiquote_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuasiquote_cnum_cst");
  (void)self;
  return 1u;
}
/* ========================================================================= */
void ligQuasiquote_display(FILE *                                 f,
                           /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuasiquote_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void)self;
  fprintf(f, "quasiquote");
}
/* ========================================================================= */
ligReturn ligQuasiquote_migration(/*@unused@*/ ligCell *  self,
                                  /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligQuasiquote_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void)self;
  (void)memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Quasiquote(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Quasiquote");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Quasiquote(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Quasiquote");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Quasiquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Quasiquote");
  LIGEIA_ASSERT(self);
  if (ligSECD_qqlv_int(self) < 1) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    return LIGEIA_RETURN_CODE_PUSH_05;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    return LIGEIA_RETURN_CODE_PUSH_07;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligQuasiquote_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuasiquote_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  { ligCell_set_quasiquote(self); }
}
