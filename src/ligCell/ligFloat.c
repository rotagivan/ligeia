/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligFloat.c
 *  @brief ligFloat.c
 *
 *  ligFloat.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2018/06/29
 */

#include "./ligFloat.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <stdio.h>
#include <math.h>
#include <float.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligCell.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligFloat_cnum_cst(/*@null@*/ const ligCell*);
static void             ligFloat_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligFloat_migration(ligCell*, ligMemory*);
static bool             ligFloat_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_Float(ligSECD*);
static ligReturn        ligSECD_eval_Float(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligFloat_VTBL = {
  &ligFloat_cnum_cst,
  &ligFloat_display,
  &ligFloat_migration,
  &ligFloat_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_Float,
  &ligSECD_eval_Float,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligFloat_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligFloat_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligFloat_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligFloat_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "%f", ligCell_float(self));
}
/* ========================================================================= */
ligReturn ligFloat_migration(/*@unused@*/ ligCell *self,
                             /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligFloat_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
bool ligFloat_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligFloat_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l)); LIGEIA_ASSERT(ligCell_is_float(l));
  return (ligCell_is_float(r) &&
          (fabsf(ligCell_float(l) - ligCell_float(r)) < FLT_EPSILON) );
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Float(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Float");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Float(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Float");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligFloat_new(ligCell *self, float a_Float) {
  LIGEIA_TRACE_DEBUG("ligFloat_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_float(self);
    ligCell_float(self) = a_Float;
  }
}
