/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligUnquote.c
 *  @brief ligUnquote.c
 *
 *  ligUnquote.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/19
 */

#include "./ligUnquote.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligUnquote_cnum_cst(/*@null@*/ const ligCell *);
static void      ligUnquote_display(FILE *, /*@null@*/ const ligCell *);
static ligReturn ligUnquote_migration(ligCell *, ligMemory *);
static ligReturn ligSECD_eval_Unquote(ligSECD *);
static ligReturn ligSECD_call_Unquote(ligSECD *);
/* ========================================================================= */
const LIGEIA_VTBL_t ligUnquote_VTBL = {
    &ligUnquote_cnum_cst,
    &ligUnquote_display,
    &ligUnquote_migration,
    NULL, /* is_eqv */
    NULL, /* expand */
    NULL, /* quasi */
    &ligSECD_eval_Unquote,
    &ligSECD_call_Unquote,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligUnquote_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnquote_cnum_cst");
  (void)self;
  return 1u;
}
/* ========================================================================= */
void ligUnquote_display(FILE *f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnquote_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void)self;
  fprintf(f, "unquote");
}
/* ========================================================================= */
ligReturn ligUnquote_migration(/*@unused@*/ ligCell *  self,
                               /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligUnquote_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void)self;
  (void)memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Unquote(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Unquote");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Unquote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Unquote");
  LIGEIA_ASSERT(self);
  if (ligSECD_qqlv_int(self) < 1) {
    LIGEIA_TRACE_ALWAYS("unquote appear with invalid quasiquote level.");
    return LIGEIA_RETURN_QQ_INVALID_LV;
  } else if (ligSECD_qqlv_int(self) == 1) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    return LIGEIA_RETURN_CODE_PUSH_05;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_inc));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_qqlv_dec));
    return LIGEIA_RETURN_CODE_PUSH_07;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligUnquote_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligUnquote_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  { ligCell_set_unquote(self); }
}
