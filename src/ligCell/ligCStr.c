/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCStr.c
 *  @brief ligCStr.c
 *
 *  ligCStr.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/27
 *  @date 2018/07/03
 */

#include "./ligCStr.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligCell.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligCStr_cnum_cst(/*@null@*/ const ligCell*);
static void             ligCStr_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligCStr_migration(ligCell*, ligMemory*);
static bool             ligCStr_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_CStr(ligSECD*);
static ligReturn        ligSECD_eval_CStr(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligCStr_VTBL = {
  &ligCStr_cnum_cst,
  &ligCStr_display,
  &ligCStr_migration,
  &ligCStr_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_CStr,
  &ligSECD_eval_CStr,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligCStr_cnum_cst(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCStr_cnum_cst");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  return ligCStr_cnum(strlen(ligCell_cstr(self)));
}
/* ========================================================================= */
void ligCStr_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCStr_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  fprintf(f, "\"%s\"", ligCell_cstr(self));
}
/* ========================================================================= */
ligReturn ligCStr_migration(/*@unused@*/ ligCell *self,
                            /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligCStr_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
bool ligCStr_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligCStr_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_cstr(l));
  return (0 == strcmp(ligCell_cstr(l), ligCell_cstr(r)));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_CStr(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_CStr");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_CStr(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_CStr");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligCStr_new(ligCell *self, const char *a_cstr, size_t a_Size) {
  LIGEIA_TRACE_DEBUG("ligCStr_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_cstr(self) != a_cstr);
  LIGEIA_ASSERT(0 < a_Size);
  {
    ligCell_set_cstr(self);
    strncpy(ligCell_cstr(self), a_cstr, a_Size);
    ligCell_cstr(self)[a_Size] = '\0';
  }
}
/* ========================================================================= */
size_t ligCStr_cnum(size_t a_Len) {
  LIGEIA_TRACE_DEBUG("ligCStr_cnum");
  LIGEIA_ASSERT(0 < a_Len);
  {
    ++a_Len;  /* '\0' */
    a_Len -= ligCell_value_size;
    return ligCell_cnum_size(a_Len);
  }
}
