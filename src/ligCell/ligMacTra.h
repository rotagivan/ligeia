/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligMacTra.h
 *  @brief ligMacTra.h
 *
 *  ligMacTra.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/08/04
 *  @date 2018/06/29
 */

#ifndef LIGEIA_LIGCELL_LIGMACTRA_H_
#define LIGEIA_LIGCELL_LIGMACTRA_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct ligMacTra_ {
    ligCell*    env_mac;
    ligCell*    transformer;
  } ligMacTra;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligMacTra_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligMacTra_new(ligCell*, ligCell*, ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELLLIGMACTRA_H_ */
