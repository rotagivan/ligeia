/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligVector.h
 *  @brief ligVector.h
 *
 *  ligVector.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/19
 *  @date 2018/06/29
 */

#ifndef LIGEIA_LIGCELL_LIGVECTOR_H_
#define LIGEIA_LIGCELL_LIGVECTOR_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct ligVector_ {
    size_t      size;
    ligCell     *vals[1];
  } ligVector;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligVector_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligVector_new(ligCell*, size_t size);
  extern LIGEIA_DECLSPEC
  size_t        LIGEIA_CALL ligVector_cnum(size_t);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  ligReturn     LIGEIA_CALL ligVector_setx(ligCell*,
                                           size_t, /*@null@*/ ligCell*);
  extern LIGEIA_DECLSPEC
  size_t        LIGEIA_CALL ligVector_size_impl(ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGVECTOR_H_ */
