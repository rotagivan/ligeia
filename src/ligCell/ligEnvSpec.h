/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligEnvSpec.h
 *  @brief ligEnvSpec.h
 *
 *  ligEnvSpec.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/29
 *  @date 2017/07/29
 */

#ifndef LIGEIA_LIGCELL_LIGENVSPEC_H_
#define LIGEIA_LIGCELL_LIGENVSPEC_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligEnvSpec_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligEnvSpec_new(ligCell*, ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGENVSPEC_H_ */
