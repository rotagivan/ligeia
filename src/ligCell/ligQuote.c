/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligQuote.c
 *  @brief ligQuote.c
 *
 *  ligQuote.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/06/29
 */

#include "./ligQuote.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligQuote_cnum_cst(/*@null@*/ const ligCell*);
static void             ligQuote_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligQuote_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_quasi_Quote(ligSECD*);
static ligReturn        ligSECD_eval_Quote(ligSECD*);
static ligReturn        ligSECD_call_Quote(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligQuote_VTBL = {
  &ligQuote_cnum_cst,
  &ligQuote_display,
  &ligQuote_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  &ligSECD_quasi_Quote,
  &ligSECD_eval_Quote,
  &ligSECD_call_Quote,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligQuote_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuote_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligQuote_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuote_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "quote");
}
/* ========================================================================= */
ligReturn ligQuote_migration(/*@unused@*/ ligCell *self,
                             /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligQuote_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Quote(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Quote");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Quote(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Quote");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Quote(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Quote");
  LIGEIA_ASSERT(self);
  {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_02;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligQuote_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligQuote_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  ligCell_set_quote(self);
}
