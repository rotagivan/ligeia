/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligOperator.h
 *  @brief ligOperator.h
 *
 *  ligOperator.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/06
 *  @date 2018/07/18
 */

#ifndef LIGEIA_LIGCELL_LIGOPERATOR_H_
#define LIGEIA_LIGCELL_LIGOPERATOR_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligOperator_ {
    LIGEIA_OPERATOR_BEGIN_      = 0,

    LIGEIA_OPERATOR_nop         = LIGEIA_OPERATOR_BEGIN_,
    LIGEIA_OPERATOR_display,
    /*    LIGEIA_OPERATOR_displayln,*/
    LIGEIA_OPERATOR_newline,
    LIGEIA_OPERATOR_stack_pop,
    LIGEIA_OPERATOR_stack_head,
    LIGEIA_OPERATOR_stack_copy,
    LIGEIA_OPERATOR_stack_press,
    LIGEIA_OPERATOR_stack_swap,
    LIGEIA_OPERATOR_stack_cons,
    LIGEIA_OPERATOR_stack_xcons,
    LIGEIA_OPERATOR_stack_unlist,
    LIGEIA_OPERATOR_stack_append,
    LIGEIA_OPERATOR_stack_xappend,
    LIGEIA_OPERATOR_stack_car,
    LIGEIA_OPERATOR_stack_cdr,
    LIGEIA_OPERATOR_stack_set_imm,
    LIGEIA_OPERATOR_env_pop,
    LIGEIA_OPERATOR_env_push_nil,
    LIGEIA_OPERATOR_dump_save_1_2,
    LIGEIA_OPERATOR_dump_save_1_4,
    LIGEIA_OPERATOR_dump_load,
    LIGEIA_OPERATOR_stack_to_work,
    LIGEIA_OPERATOR_work_to_stack,
    LIGEIA_OPERATOR_wts_splice,
    LIGEIA_OPERATOR_qqlv_inc,
    LIGEIA_OPERATOR_qqlv_dec,
    LIGEIA_OPERATOR_quasi,
    LIGEIA_OPERATOR_quasi_rest,
    LIGEIA_OPERATOR_eval,
    LIGEIA_OPERATOR_eval_rest,
    LIGEIA_OPERATOR_call,
    LIGEIA_OPERATOR_proc_pre,
    LIGEIA_OPERATOR_begin_impl,
    LIGEIA_OPERATOR_list,
    LIGEIA_OPERATOR_list_to_vector,
    LIGEIA_OPERATOR_list_last,
    LIGEIA_OPERATOR_define_impl,
    LIGEIA_OPERATOR_define,
    LIGEIA_OPERATOR_bind_rest,
    LIGEIA_OPERATOR_setx,
    LIGEIA_OPERATOR_find,
    LIGEIA_OPERATOR_lambda,
    LIGEIA_OPERATOR_mksynclo,
    LIGEIA_OPERATOR_mul,
    LIGEIA_OPERATOR_div,
    LIGEIA_OPERATOR_add,
    LIGEIA_OPERATOR_sub,
    LIGEIA_OPERATOR_cons,

    LIGEIA_OPERATOR_END_,
    LIGEIA_OPERATOR_SIZE_       = LIGEIA_OPERATOR_END_ - LIGEIA_OPERATOR_BEGIN_
  } ligOperator;
  /* ======================================================================= */
  typedef struct ligOperatorInfo_ ligOperatorInfo;
  struct ligOperatorInfo_ {
    /*@null@*/ const ligSECD_func_t     func;
    const int                           argc;
    const char                          *name;
    /*@null@*/ const char               *export;
    /*@null@*/ const char               *emit;
  };
  /* ----------------------------------------------------------------------- */
  /*
   * @see       ligSECD.c
   */
  extern LIGEIA_DECLSPEC
  const ligOperatorInfo LIGEIA_OPERATOR_INFO[LIGEIA_OPERATOR_SIZE_];
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  const LIGEIA_VTBL_t   ligOperator_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void                  LIGEIA_CALL ligOperator_new(ligCell*, ligOperator);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGOPERATOR_H_ */
