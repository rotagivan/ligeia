/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSynClo.h
 *  @brief ligSynClo.h
 *
 *  ligSynClo.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/06/29
 */

#ifndef LIGEIA_LIGCELL_LIGSYNCLO_H_
#define LIGEIA_LIGCELL_LIGSYNCLO_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef struct ligSynClo_ {
    ligCell     *envspec;
    ligCell     *freevar;
    ligCell     *form;
  } ligSynClo;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligSynClo_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligSynClo_new(ligCell*,
                                          ligCell*, ligCell*, ligCell*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGSYNCLO_H_ */
