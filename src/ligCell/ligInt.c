/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligInt.c
 *  @brief ligInt.c
 *
 *  ligInt.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2018/06/30
 */

#include "./ligInt.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <stdio.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligCell.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligInt_cnum_cst(/*@null@*/ const ligCell*);
static void             ligInt_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligInt_migration(ligCell*, ligMemory*);
static bool             ligInt_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_Int(ligSECD*);
static ligReturn        ligSECD_eval_Int(ligSECD*);
static ligReturn        ligSECD_foldl_Int(ligSECD*, const ligCell_fold_func_t);
static ligReturn        ligInt_mul(ligCell*, const ligCell*, const ligCell*);
static ligReturn        ligInt_div(ligCell*, const ligCell*, const ligCell*);
static ligReturn        ligInt_add(ligCell*, const ligCell*, const ligCell*);
static ligReturn        ligInt_sub(ligCell*, const ligCell*, const ligCell*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligInt_VTBL = {
  &ligInt_cnum_cst,
  &ligInt_display,
  &ligInt_migration,
  &ligInt_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_Int,
  &ligSECD_eval_Int,
  NULL,         /* call */
  NULL,         /* run */
  &ligSECD_foldl_Int,
  &ligInt_mul,
  &ligInt_div,
  &ligInt_add,
  &ligInt_sub,
};
/* ========================================================================= */
size_t ligInt_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligInt_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligInt_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligInt_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "%ld", ligCell_int(self));
}
/* ========================================================================= */
ligReturn ligInt_migration(/*@unused@*/ ligCell *self,
                           /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligInt_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
bool ligInt_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligInt_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_int(l));
  return (ligCell_is_int(r) && (ligCell_int(l) == ligCell_int(r)));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Int(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Int");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Int(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Int");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_foldl_Int(ligSECD *self, const ligCell_fold_func_t func) {
  LIGEIA_TRACE_DEBUG("ligSECD_foldl_Int");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(func);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_int(ligSECD_stack_top(self)));

  if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
    LIGEIA_TRACE_ALWAYS("ligSECD_foldl_Int: 2 or more args are required.");
    return LIGEIA_RETURN_INVALID_ARGC;
  }

  {  /* guard clone */
    ligRetExp(ligSECD_stack_push_int(self,
                                     ligCell_int(ligSECD_stack_top(self))));
    ligRetExp(ligSECD_stack_press(self));
  }

  while (!ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) {
    LIGEIA_ASSERT(ligCell_is_cons(ligCell_cdr(ligSECD_stack(self))));
    ligRetExp(func(ligSECD_stack_top(self),
                   ligSECD_stack_top(self),
                   ligCell_cadr(ligSECD_stack(self))));
    ligRetExp(ligSECD_stack_press(self));
  }

  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligInt_mul(ligCell *ret, const ligCell *rhs, const ligCell *lhs) {
  LIGEIA_TRACE_DEBUG("ligInt_mul");
  LIGEIA_ASSERT(ret); LIGEIA_ASSERT(rhs); LIGEIA_ASSERT(lhs);

  if (!ligCell_is_int(ret) || !ligCell_is_int(rhs) || !ligCell_is_int(lhs)) {
    LIGEIA_TRACE_ALWAYS("ligInt_mul: "
                        "The type is different. An integer is required.");
    return LIGEIA_RETURN_INVALID_ARGS;
  }

  ligCell_int(ret) = ligCell_int(rhs) * ligCell_int(lhs);

  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligInt_div(ligCell *ret, const ligCell *rhs, const ligCell *lhs) {
  LIGEIA_TRACE_DEBUG("ligInt_div");
  LIGEIA_ASSERT(ret); LIGEIA_ASSERT(rhs); LIGEIA_ASSERT(lhs);

  if (!ligCell_is_int(ret) || !ligCell_is_int(rhs) || !ligCell_is_int(lhs)) {
    LIGEIA_TRACE_ALWAYS("ligInt_div: "
                        "The type is different. An integer is required.");
    return LIGEIA_RETURN_INVALID_ARGS;
  }

  ligCell_int(ret) = ligCell_int(rhs) / ligCell_int(lhs);

  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligInt_add(ligCell *ret, const ligCell *rhs, const ligCell *lhs) {
  LIGEIA_TRACE_DEBUG("ligInt_add");
  LIGEIA_ASSERT(ret); LIGEIA_ASSERT(rhs); LIGEIA_ASSERT(lhs);

  if (!ligCell_is_int(ret) || !ligCell_is_int(rhs) || !ligCell_is_int(lhs)) {
    LIGEIA_TRACE_ALWAYS("ligInt_add: "
                        "The type is different. An integer is required.");
    return LIGEIA_RETURN_INVALID_ARGS;
  }

  ligCell_int(ret) = ligCell_int(rhs) + ligCell_int(lhs);

  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligInt_sub(ligCell *ret, const ligCell *rhs, const ligCell *lhs) {
  LIGEIA_TRACE_DEBUG("ligInt_sub");
  LIGEIA_ASSERT(ret); LIGEIA_ASSERT(rhs); LIGEIA_ASSERT(lhs);

  if (!ligCell_is_int(ret) || !ligCell_is_int(rhs) || !ligCell_is_int(lhs)) {
    LIGEIA_TRACE_ALWAYS("ligInt_sub: "
                        "The type is different. An integer is required.");
    return LIGEIA_RETURN_INVALID_ARGS;
  }

  ligCell_int(ret) = ligCell_int(rhs) - ligCell_int(lhs);

  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligInt_new(ligCell *self, intptr_t a_Int) {
  LIGEIA_TRACE_DEBUG("ligInt_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_int(self);
    ligCell_int(self) = a_Int;
  }
}
