/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCons.c
 *  @brief ligCons.c
 *
 *  ligCons.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/28
 *  @date 2018/07/19
 */

#include "./ligCons.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligCons_display(FILE *f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_cons(self));
  {
    bool r = true;
    if (ligCell_p_is_nil(ligCell_car(self))) {
      (void)putc('(', f);
    } else {
      const ligCell_type_t cst = ligCell_cst(ligCell_car(self));
      switch (cst) {
        case LIGEIA_CST_FALSE: /* FALLTHROUGH */
        case LIGEIA_CST_CONS:  /* FALLTHROUGH */
        case LIGEIA_CST_TRUE:
          (void)putc('(', f);
          break;
        default:
          switch (ligCell_cst2cnct(cst)) {
            case LIGEIA_CNCT_QUOTE:
              (void)putc('\'', f);
              r    = false;
              self = ligCell_cdr(self);
              break;
            case LIGEIA_CNCT_QUASIQUOTE:
              (void)putc('`', f);
              r    = false;
              self = ligCell_cdr(self);
              break;
            case LIGEIA_CNCT_UNQUOTE:
              (void)putc(',', f);
              r    = false;
              self = ligCell_cdr(self);
              break;
            case LIGEIA_CNCT_UNQSPL:
              fprintf(f, ",@");
              r    = false;
              self = ligCell_cdr(self);
              break;
            default:
              (void)putc('(', f);
              break;
          }
          break;
      }
    }

    for (;;) {
      ligCell_display(f, ligCell_car(self));
      self = ligCell_cdr(self);
      if (ligCell_p_is_nil(self)) {
        break;
      }
      if (!ligCell_is_cons(self)) {
        fprintf(f, " . ");
        ligCell_display(f, self);
        break;
      }
      (void)putc(' ', f);
    }

    if (r) {
      (void)putc(')', f);
    }
  }
}
/* ========================================================================= */
ligReturn ligCons_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligCons_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_cons(self));
  LIGEIA_ASSERT(memory);
  ligRetExp(ligMemory_migration(memory, &(ligCell_car(self))));
  return ligMemory_migration(memory, &(ligCell_cdr(self)));
}
/* ========================================================================= */
bool ligCons_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligCons_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_cons(l));
  LIGEIA_ASSERT(!ligCell_p_is_nil(r));
  return (ligCell_is_cons(r) &&
          ligCell_is_eqv(ligCell_car(l), ligCell_car(r)) &&
          ligCell_is_eqv(ligCell_cdr(l), ligCell_cdr(r)));
}
/* ========================================================================= */
static ligReturn ligSECD_quasi_Cons_impl(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Cons_impl");
  LIGEIA_ASSERT(self);

#define quasi_Cons                                                   \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));       \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest)); \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist))

  if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) { /* tail */
    quasi_Cons;
    return LIGEIA_RETURN_CODE_PUSH_03;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_dump_load));
    quasi_Cons;
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_dump_save_1_4));
    return LIGEIA_RETURN_CODE_PUSH_05;
  }

#undef quasi_Cons
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Cons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
  if (ligCell_p_is_nil(ligCell_car(ligSECD_stack_top(self)))) {
    LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cdr(ligSECD_stack_top(self))));
    return LIGEIA_RETURN_CODE_PUSH_00;
  } else if (ligSECD_qqlv_int(self) < 1) {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));
    return LIGEIA_RETURN_CODE_PUSH_01;
  } else {
    const ligCell_type_t cst =
        ligCell_cst(ligCell_car(ligSECD_stack_top(self)));
    switch (cst) {
      case LIGEIA_CST_FALSE: /* FALLTHROUGH */
      case LIGEIA_CST_CONS:  /* FALLTHROUGH */
      case LIGEIA_CST_TRUE: {
        return ligSECD_quasi_Cons_impl(self);
      }
      default: {
        const ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        switch (cnct) {
          case LIGEIA_CNCT_CONSTVEC:   /* FALLTHROUGH */
          case LIGEIA_CNCT_QUASIQUOTE: /* FALLTHROUGH */
          case LIGEIA_CNCT_UNQUOTE:    /* FALLTHROUGH */
          case LIGEIA_CNCT_UNQSPL: {
            return ligSECD_eval_Cons(self);
          }
          default:
            return ligSECD_quasi_Cons_impl(self);
        }
      }
    }
  }
}
/* ========================================================================= */
ligReturn ligSECD_eval_Cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Cons");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));

#define eval_Cons                                              \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_call)); \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval)); \
  ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_head))

  if (ligCell_p_is_nil(ligCell_cdr(ligSECD_stack(self)))) { /* tail */
    eval_Cons;
    return LIGEIA_RETURN_CODE_PUSH_03;
  } else {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_dump_load));
    eval_Cons;
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_dump_save_1_4));
    return LIGEIA_RETURN_CODE_PUSH_05;
  }

#undef eval_Cons
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligCons_set_immutable(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_set_immutable");
  for (; !ligCell_p_is_nil(self) && !ligCell_is_immutable(self);
       self = ligCell_cdr(self)) {
    ligCell_set_immutable(self);
    if (!ligCell_is_cons(self)) {
      break;
    }
  }
}
/* ========================================================================= */
ligReturn ligCons_set_car(ligCell *self, ligCell *car) {
  LIGEIA_TRACE_DEBUG("ligCons_set_car");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_cons(self));
  if (ligCell_is_immutable(self)) {
    LIGEIA_TRACE_ALWAYS("ERROR! ligCons_set_car: immutable err!");
    (void)putc('\t', stderr);
    ligCons_display(stderr, self);
    (void)putc('\n', stderr);
    (void)fflush(stderr);
    return LIGEIA_RETURN_IMMUTABLE;
  }
  ligCell_car(self) = ligCell_p_mix(car, ligCell_p_tag(ligCell_car(self)));
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligCons_set_cdr(ligCell *self, /*@NULL@*/ ligCell *cdr) {
  LIGEIA_TRACE_DEBUG("ligCons_set_cdr");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_cons(self));
  if (ligCell_is_immutable(self)) {
    LIGEIA_TRACE_ALWAYS("ERROR! ligCons_set_cdr: immutable err!");
    (void)putc('\t', stderr);
    ligCons_display(stderr, self);
    (void)putc('\n', stderr);
    (void)fflush(stderr);
    return LIGEIA_RETURN_IMMUTABLE;
  }
  ligCell_cdr(self) = ligCell_p_notag(cdr);
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligCons_append(ligCell **self, ligCell *cell) {
  LIGEIA_TRACE_DEBUG("ligCons_append");
  LIGEIA_ASSERT(self);
  if (ligCell_p_is_nil(cell)) {
    return LIGEIA_RETURN_OK;
  }
  LIGEIA_ASSERT(ligCell_is_cons(cell));
  if (ligCell_p_is_nil(*self)) {
    *self = ligCell_p_mix(ligCell_p_notag(cell), ligCell_p_tag(*self));
    return LIGEIA_RETURN_OK;
  }
  LIGEIA_ASSERT(ligCell_is_cons(*self));
  while (!ligCell_p_is_nil(ligCell_cdr(*self))) {
    self = &(ligCell_cdr(*self));
    LIGEIA_ASSERT(ligCell_is_cons(*self));
  };
  return ligCons_set_cdr(*self, cell);
}
/* ========================================================================= */
bool ligCons_is_pair(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_is_pair");
  return (!ligCell_p_is_nil(self) && ligCell_is_cons(self) &&
          !ligCell_p_is_nil(ligCell_cdr(self)));
}
/* ========================================================================= */
static ligConsResult ligCons_result_pred_(/*@null@*/ const ligCell *self,
                                          ligCons_result_pred_t     pred,
                                          /*@null@*/ void *         userd) {
  LIGEIA_TRACE_DEBUG("ligCons_result_pred_");
  LIGEIA_ASSERT(pred);
  if (!pred(NULL, ligCell_car(self), userd)) {
    return ligConsResult_make(0, LIGEIA_CONS_TYPE_ERROR);
  }
  {
    ligCell *current = ligCell_cdr(self);
    size_t   idx     = 1;
    for (;; current = ligCell_cdr(current), ++idx) {
      if (ligCell_p_is_nil(current)) {
        return ligConsResult_make(idx, LIGEIA_CONS_TYPE_PROPER);
      }
      {
        const ligCell *c = self;
        size_t         i = 0;
        if (!ligCell_is_cons(current)) {
          for (; i < idx; ++i, c = ligCell_cdr(c)) {
            if (!pred(ligCell_car(c), current, userd)) {
              return ligConsResult_make(0, LIGEIA_CONS_TYPE_ERROR);
            }
          }
          return ligConsResult_make(idx + 1, LIGEIA_CONS_TYPE_IMPROPER);
        } else {
          for (; i < idx; ++i, c = ligCell_cdr(c)) {
            if (ligCell_is_eq(c, current)) {
              LIGEIA_TRACE_ALWAYS("ligCons_result: cyclic.");
              return ligConsResult_make(idx, LIGEIA_CONS_TYPE_CYCLIC);
            }
            if (!pred(ligCell_car(c), ligCell_car(current), userd)) {
              return ligConsResult_make(0, LIGEIA_CONS_TYPE_ERROR);
            }
          }
        }
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
static ligConsResult ligCons_result_no_pred_(/*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_result_no_pred_");
  {
    ligCell *current = ligCell_cdr(self);
    size_t   idx     = 1;
    for (;; current = ligCell_cdr(current), ++idx) {
      if (ligCell_p_is_nil(current)) {
        return ligConsResult_make(idx, LIGEIA_CONS_TYPE_PROPER);
      }
      if (!ligCell_is_cons(current)) {
        return ligConsResult_make(idx + 1, LIGEIA_CONS_TYPE_IMPROPER);
      } else {
        const ligCell *c = self;
        size_t         i = 0;
        for (; i < idx; ++i, c = ligCell_cdr(c)) {
          if (ligCell_is_eq(c, current)) {
            LIGEIA_TRACE_ALWAYS("ligCons_result: cyclic.");
            return ligConsResult_make(idx, LIGEIA_CONS_TYPE_CYCLIC);
          }
        }
      }
    }
  }
}
/* ------------------------------------------------------------------------- */
ligConsResult ligCons_result(/*@null@*/ const ligCell *       self,
                             /*@null@*/ ligCons_result_pred_t pred,
                             /*@null@*/ void *                userd) {
  LIGEIA_TRACE_DEBUG("ligCons_result");
  if (ligCell_p_is_nil(self)) {
    return ligConsResult_make(0, LIGEIA_CONS_TYPE_PROPER);
  }
  if (!ligCell_is_cons(self)) {
    return ligConsResult_make(0, LIGEIA_CONS_TYPE_ERROR);
  }
  return (pred ? ligCons_result_pred_(self, pred, userd)
               : ligCons_result_no_pred_(self));
}
/* ------------------------------------------------------------------------- */
bool ligCons_is_proper(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_is_proper");
  return (LIGEIA_CONS_TYPE_PROPER ==
          ligConsResult_type(ligCons_result(self, NULL, NULL)));
}
/* ------------------------------------------------------------------------- */
bool ligCons_is_improper(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_is_improper");
  return (LIGEIA_CONS_TYPE_IMPROPER ==
          ligConsResult_type(ligCons_result(self, NULL, NULL)));
}
/* ========================================================================= */
ligCell *ligCons_list_last(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCons_list_last");
  if (ligCell_p_is_nil(self)) {
    return NULL;
  }
  for (;; self = ligCell_cdr(self)) {
    if (!ligCell_is_cons(self)) {
      LIGEIA_TRACE_ALWAYS("ligCons_list_last: improper list.");
      return NULL;
    }
    if (ligCell_p_is_nil(ligCell_cdr(self))) {
      break;
    }
  }
  return ligCell_car(self);
}
/* ========================================================================= */
ligReturn ligCons_list_chop_last(ligCell **self) {
  LIGEIA_TRACE_DEBUG("ligCons_list_chop_last");
  if (ligCell_p_is_nil(*self)) {
    return LIGEIA_RETURN_OK;
  }
  if (!ligCell_is_cons(*self)) {
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  if (ligCell_p_is_nil(ligCell_cdr(*self))) {
    *self = ligCell_p_mix(NULL, ligCell_p_tag(*self));
    return LIGEIA_RETURN_OK;
  }
  if (!ligCell_is_cons(ligCell_cdr(*self))) {
    LIGEIA_TRACE_ALWAYS("ligCons_list_chop_last: improper list.");
    return LIGEIA_RETURN_INVALID_ARGS;
  }
  {
    ligCell *current = *self;
    for (;; current = ligCell_cdr(current)) {
      if (ligCell_p_is_nil(ligCell_cddr(current))) {
        ligRetExp(ligCons_set_cdr(current, NULL));
        return LIGEIA_RETURN_OK;
      }
      if (!ligCell_is_cons(ligCell_cddr(current))) {
        LIGEIA_TRACE_ALWAYS("ligCons_list_chop_last: improper list.");
        return LIGEIA_RETURN_INVALID_ARGS;
      }
    }
  }
}
