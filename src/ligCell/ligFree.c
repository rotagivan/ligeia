/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligFree.c
 *  @brief ligFree.c
 *
 *  ligFree.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/23
 *  @date 2018/06/29
 */

#include "./ligFree.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <stdio.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligCell.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligFree_cnum_cst(/*@null@*/ const ligCell*);
static void             ligFree_display(FILE*, /*@null@*/ const ligCell*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligFree_VTBL = {
  &ligFree_cnum_cst,
  &ligFree_display,
  NULL,         /* migration */
  NULL,         /* is_eqv */
  NULL,         /* expand */
  NULL,         /* quasi */
  NULL,         /* eval */
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligFree_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligFree_nun_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligFree_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligFree_display");
  (void) self;
  fprintf(f, "#<free>");
}
