/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligOperator.c
 *  @brief ligOperator.c
 *
 *  ligOperator.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/18
 *  @date 2018/07/08
 */

#include "./ligOperator.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligOperator_cnum_cst(/*@null@*/ const ligCell*);
static void             ligOperator_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligOperator_migration(ligCell*, ligMemory*);
static bool             ligOperator_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_Operator(ligSECD*);
static ligReturn        ligSECD_eval_Operator(ligSECD*);
static ligReturn        ligSECD_call_Operator(ligSECD*);
static ligReturn        ligSECD_run_Operator(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligOperator_VTBL = {
  &ligOperator_cnum_cst,
  &ligOperator_display,
  &ligOperator_migration,
  &ligOperator_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_Operator,
  &ligSECD_eval_Operator,
  &ligSECD_call_Operator,
  &ligSECD_run_Operator,
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligOperator_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligOperator_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligOperator_display(FILE* f, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligOperator_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "<%s>", LIGEIA_OPERATOR_INFO[ligCell_operator(self)].name);
}
/* ========================================================================= */
ligReturn ligOperator_migration(/*@unused@*/ ligCell *self,
                                /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligOperator_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
bool ligOperator_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligOperator_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_operator(l));
  return (ligCell_is_operator(r) &&
          (ligCell_operator(l) == ligCell_operator(r)));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Operator(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Operator");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Operator(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Operator");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Operator(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Operator");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_operator(ligSECD_stack_top(self)));
  {
    ligRetExp(ligSECD_code_push_op(self,
                                   ligCell_operator(ligSECD_stack_top(self))));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_04;
  }
}
/* ========================================================================= */
static bool ligOperator_check_argc(int argc, int len_stack);
static bool ligOperator_check_argc(int argc, int len_stack) {
  LIGEIA_TRACE_DEBUG("ligOperator_check_argc");
  switch(argc) {
    case -2:    return    0 <= len_stack;
    case -1:    return    1 <= len_stack;
    default:    return argc == len_stack;
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_run_Operator(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_run_Operator");
  LIGEIA_ASSERT(self);
  {
    const ligOperator op = ligCell_operator(ligSECD_code_top(self));
    const ligOperatorInfo* const info = &LIGEIA_OPERATOR_INFO[op];
    ligReturn ret = LIGEIA_RETURN_OK;
    LIGEIA_ASSERT(info->func);
    ligRetExp(ligSECD_code_pop(self));
    {
#     if defined(LIGEIA_DEBUG)
      int len_code =
          (int)ligConsResult_length(ligCons_result(ligSECD_code(self),
                                                   NULL, NULL));
#     endif

      int len_stack =
          (int)ligConsResult_length(ligCons_result(ligSECD_stack(self),
                                                   NULL, NULL));
      if (!ligOperator_check_argc(info->argc, len_stack)) {
        LIGEIA_TRACE_ALWAYS("error!: ligSECD_run_Operator: invalid argc.");
        fprintf(stderr, "\t%s: require=%d but argc=%d.\n",
                info->name, info->argc, len_stack);
        (void) fflush(stderr);

#       if defined(LIGEIA_DEBUG)
        ligSECD_print(self);
#       endif

        return LIGEIA_RETURN_INVALID_ARGC;
      }

      ret = (info->func)(self);
      switch (ret) {
        case LIGEIA_RETURN_OK: {
#         if 0
          /* TODO(hanepjiv): emitter */
          printf("ligSECD_%s(self);\n",
                 NULL != info->emit ? info->emit : info->name);
          (void) fflush(stdout);
#         endif
          break;
        }
        case LIGEIA_RETURN_CODE_PUSH_00:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_01:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_02:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_03:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_04:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_05:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_06:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_07:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_08:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_09:  /* FALLTHROUGH */
        case LIGEIA_RETURN_CODE_PUSH_10:  {
#         if defined(LIGEIA_DEBUG)
          {
            int num_push = (int)ret - (int)LIGEIA_RETURN_CODE_PUSH_00;
            len_code =
                (int)ligConsResult_length(ligCons_result(ligSECD_code(self),
                                                         NULL, NULL))
                - len_code;
            if (num_push != len_code) {
              LIGEIA_TRACE_ALWAYS("error!: ligSECD_run_Operator: code push.");
              fprintf(stderr, "\t%s: %d,%d\n", info->name, len_code, num_push);
              (void) fflush(stderr);
              LIGEIA_ASSERT_ALWAYS(num_push == len_code);
            }
          }
#         endif
          /* TODO(hanepjiv): optimize */
          ret = LIGEIA_RETURN_OK;
          break;
        }
        default:
          break;
      }
      return ret;
    }
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligOperator_new(ligCell *self, ligOperator a_Operator) {
  LIGEIA_TRACE_DEBUG("ligOperator_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  ligCell_set_operator(self);
  ligCell_operator(self) = a_Operator;
}
