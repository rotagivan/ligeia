/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligSynClo.c
 *  @brief ligSynClo.c
 *
 *  ligSynClo.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/05
 */

#include "./ligSynClo.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligSynClo_cnum_cst(/*@null@*/ const ligCell *);
static void      ligSynClo_display(FILE *, /*@null@*/ const ligCell *);
static ligReturn ligSynClo_migration(ligCell *, ligMemory *);
static bool      ligSynClo_is_eqv(const ligCell *, const ligCell *);
static ligReturn ligSECD_quasi_SynClo(ligSECD *);
static ligReturn ligSECD_eval_SynClo(ligSECD *);
static ligReturn ligSECD_call_SynClo(ligSECD *);
/* ========================================================================= */
const LIGEIA_VTBL_t ligSynClo_VTBL = {
    &ligSynClo_cnum_cst,
    &ligSynClo_display,
    &ligSynClo_migration,
    &ligSynClo_is_eqv,
    NULL, /* expand */
    &ligSECD_quasi_SynClo,
    &ligSECD_eval_SynClo,
    &ligSECD_call_SynClo,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligSynClo_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSynClo_cnum_cst");
  (void)self;
  return ligCell_cnum_size(sizeof(ligSynClo) - ligCell_value_size);
}
/* ========================================================================= */
void ligSynClo_display(FILE *f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligSynClo_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_synclo(self));
  (void)self;
  (void)fprintf(f, "#<syntactic closure>");
}
/* ========================================================================= */
ligReturn ligSynClo_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligSynClo_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_is_synclo(self));
  LIGEIA_ASSERT(memory);
  {
    ligSynClo *synclo = ligCell_synclo_ptr(self);
    ligRetExp(ligMemory_migration(memory, &synclo->envspec));
    ligRetExp(ligMemory_migration(memory, &synclo->freevar));
    return ligMemory_migration(memory, &synclo->form);
  }
}
/* ========================================================================= */
bool ligSynClo_is_eqv(/*@unused@*/ const ligCell *l,
                      /*@unused@*/ const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligSynClo_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_synclo(l));
  LIGEIA_ASSERT(!ligCell_p_is_nil(r));
  LIGEIA_ASSERT(ligCell_is_synclo(r));
  LIGEIA_ASSERT_ALWAYS(0);
  (void)l;
  (void)r;
  return false;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_SynClo(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_SynClo");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_SynClo(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_SynClo");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_SynClo(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_SynClo");
  LIGEIA_ASSERT(self);

  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_synclo(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));

  { /* TODO(hanepjiv): */
    LIGEIA_ASSERT_ALWAYS(0 && NULL != self);
    return LIGEIA_RETURN_UNKNOWN;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligSynClo_new(ligCell *self, ligCell *envspec, ligCell *freevar,
                   ligCell *form) {
  LIGEIA_TRACE_DEBUG("ligSynClo_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(!ligCell_p_is_nil(envspec) && ligCell_is_envspec(envspec));
  LIGEIA_ASSERT(ligCell_p_is_nil(freevar) || ligCell_is_cons(freevar));
  LIGEIA_ASSERT(!ligCell_p_is_nil(form));
  {
    ligSynClo *synclo = ligCell_synclo_ptr(self);
    ligCell_set_synclo(self);
    synclo->envspec = envspec;
    synclo->freevar = freevar;
    synclo->form    = form;
  }
}
