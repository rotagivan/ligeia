/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligLet.c
 *  @brief ligLet.c
 *
 *  ligLet.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/01
 */

#include "./ligLet.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligLet_cnum_cst(/*@null@*/ const ligCell*);
static void             ligLet_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligLet_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_quasi_Let(ligSECD*);
static ligReturn        ligSECD_eval_Let(ligSECD*);
static ligReturn        ligSECD_call_Let(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligLet_VTBL = {
  &ligLet_cnum_cst,
  &ligLet_display,
  &ligLet_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  &ligSECD_quasi_Let,
  &ligSECD_eval_Let,
  &ligSECD_call_Let,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligLet_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLet_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligLet_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLet_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "let");
}
/* ========================================================================= */
ligReturn ligLet_migration(/*@unused@*/ ligCell *self,
                           /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligLet_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Let(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Let");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Let(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Let");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Let(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Let");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_let(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {
    ligSECD_print(self);
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_env_pop));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_begin_impl));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_bind_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_env_push_nil));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_06;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligLet_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLet_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_let(self);
  }
}
