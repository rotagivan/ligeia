/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligProcedure.h
 *  @brief ligProcedure.h
 *
 *  ligProcedure.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/04
 */

#ifndef LIGEIA_LIGCELL_LIGPROCEDURE_H_
#define LIGEIA_LIGCELL_LIGPROCEDURE_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligProcedureType_ {
    LIGEIA_PROCEDURE_TYPE_REND_         = -1,
    LIGEIA_PROCEDURE_TYPE_BEGIN_        =  0,

    LIGEIA_PROCEDURE_TYPE_FIXED         = LIGEIA_PROCEDURE_TYPE_BEGIN_,
    LIGEIA_PROCEDURE_TYPE_LIST,
    LIGEIA_PROCEDURE_TYPE_REST,

    LIGEIA_PROCEDURE_TYPE_END_,
    LIGEIA_PROCEDURE_TYPE_RBEGIN_       = LIGEIA_PROCEDURE_TYPE_END_ - 1,
    LIGEIA_PROCEDURE_TYPE_SIZE_         =
    LIGEIA_PROCEDURE_TYPE_END_ - LIGEIA_PROCEDURE_TYPE_BEGIN_
  } ligProcedureType;
  /* ======================================================================= */
  typedef struct ligProcedure_ {
    ligCell             *env;
    ligCell             *formals;
    ligCell             *body;
    int8_t              argc;
    uint8_t             type;
  } ligProcedure;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligProcedure_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligProcedure_new(ligCell*, ligCell*, ligCell*, ligCell*);
  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_proc_close(ligSECD*);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGPROCEDURE_H_ */
