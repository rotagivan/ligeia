/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligMacTra.c
 *  @brief ligMacTra.c
 *
 *  ligMacTra.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/06/29
 */

#include "./ligMacTra.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligMacTra_cnum_cst(/*@null@*/ const ligCell*);
static void             ligMacTra_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligMacTra_migration(ligCell*, ligMemory*);
static bool             ligMacTra_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_MacTra(ligSECD*);
static ligReturn        ligSECD_eval_MacTra(ligSECD*);
static ligReturn        ligSECD_call_MacTra(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligMacTra_VTBL = {
  &ligMacTra_cnum_cst,
  &ligMacTra_display,
  &ligMacTra_migration,
  &ligMacTra_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_MacTra,
  &ligSECD_eval_MacTra,
  &ligSECD_call_MacTra,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligMacTra_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligMacTra_cnum_cst");
  (void) self;
  return ligCell_cnum_size(sizeof(ligMacTra) - ligCell_value_size);
}
/* ========================================================================= */
void ligMacTra_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligMacTra_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "#<macro transformer>");
}
/* ========================================================================= */
ligReturn ligMacTra_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligMacTra_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  {
    ligMacTra *mactra = ligCell_mactra_ptr(self);
    ligRetExp(ligMemory_migration(memory, &mactra->env_mac));
    return ligMemory_migration(memory, &mactra->transformer);
  }
}
/* ========================================================================= */
bool ligMacTra_is_eqv(/*@unused@*/ const ligCell *l,
                      /*@unused@*/ const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligMacTra_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l)); LIGEIA_ASSERT(ligCell_is_mactra(l));
  LIGEIA_ASSERT(!ligCell_p_is_nil(r)); LIGEIA_ASSERT(ligCell_is_mactra(r));
  LIGEIA_ASSERT_ALWAYS(0);
  (void) l;
  (void) r;
  return false;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_MacTra(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_MacTra");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_MacTra(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_MacTra");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_MacTra(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_MacTra");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_mactra(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {  /* TODO(hanepjiv): */
    LIGEIA_ASSERT_ALWAYS(0 && NULL != self);
    return LIGEIA_RETURN_UNKNOWN;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligMacTra_new(ligCell *self,
                   ligCell *env_mac, ligCell *transformer) {
  LIGEIA_TRACE_DEBUG("ligMacTra_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(!ligCell_p_is_nil(env_mac)    && ligCell_is_envspec(env_mac));
  LIGEIA_ASSERT(ligCell_p_is_nil(transformer) ||
                ligCell_is_procedure(transformer));
  {
    ligMacTra *mactra   = ligCell_mactra_ptr(self);
    ligCell_set_mactra(self);
    ligCons_set_immutable(transformer);
    mactra->env_mac     = env_mac;
    mactra->transformer = transformer;
  }
}
