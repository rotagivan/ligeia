/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligProcedure.c
 *  @brief ligProcedure.c
 *
 *  ligProcedure.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/19
 */

#include "./ligProcedure.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdint.h>
#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligProcedure_cnum_cst(/*@null@*/ const ligCell *);
static void      ligProcedure_display(FILE *, /*@null@*/ const ligCell *);
static ligReturn ligProcedure_migration(ligCell *, ligMemory *);
static bool      ligProcedure_is_eqv(const ligCell *, const ligCell *);
static ligReturn ligSECD_quasi_Procedure(ligSECD *);
static ligReturn ligSECD_eval_Procedure(ligSECD *);
static ligReturn ligSECD_call_Procedure(ligSECD *);
/* ========================================================================= */
static ligReturn ligProcedure_formals_cons(ligProcedure *, ligCell *);
static bool      ligProcedure_formals_cons_pred(ligCell *, ligCell *, void *);
/* ------------------------------------------------------------------------- */
static ligReturn ligSECD_proc_bindings(ligSECD *, const int8_t);
/* ========================================================================= */
const LIGEIA_VTBL_t ligProcedure_VTBL = {
    &ligProcedure_cnum_cst,
    &ligProcedure_display,
    &ligProcedure_migration,
    &ligProcedure_is_eqv,
    NULL, /* expand */
    &ligSECD_quasi_Procedure,
    &ligSECD_eval_Procedure,
    &ligSECD_call_Procedure,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligProcedure_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligProcedure_cnum_cst");
  (void)self;
  return ligCell_cnum_size(sizeof(ligProcedure) - ligCell_value_size);
}
/* ========================================================================= */
void ligProcedure_display(FILE *f, const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligProcedure_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  fprintf(f, "(lambda ");
  ligCell_display(f, ligCell_procedure_ptr(self)->formals);
  (void)putc(' ', f);
  {
    ligCell *cell = ligCell_procedure_ptr(self)->body;
    for (;;) {
      ligCell_display(f, ligCell_car(cell));
      cell = ligCell_cdr(cell);
      if (ligCell_p_is_nil(cell)) {
        break;
      }
      (void)putc(' ', f);
    }
  }
  (void)putc(')', f);
}
/* ========================================================================= */
ligReturn ligProcedure_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligProcedure_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  {
    ligProcedure *procedure = ligCell_procedure_ptr(self);
    ligRetExp(ligMemory_migration(memory, &procedure->env));
    ligRetExp(ligMemory_migration(memory, &procedure->formals));
    return ligMemory_migration(memory, &procedure->body);
  }
}
/* ========================================================================= */
bool ligProcedure_is_eqv(/*@unused@*/ const ligCell *l,
                         /*@unused@*/ const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligProcedure_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_procedure(l));
  LIGEIA_ASSERT_ALWAYS(0);
  (void)l;
  (void)r;
  return false;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Procedure(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Procedure");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Procedure(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Procedure");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Procedure(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Procedure");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_procedure(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {
    ligReturn     ret  = LIGEIA_RETURN_CODE_PUSH_02;
    ligProcedure *proc = ligCell_procedure_ptr(ligSECD_stack_top(self));
    if (((LIGEIA_PROCEDURE_TYPE_FIXED != proc->type) || (0 != proc->argc))) {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_env_pop));
      ++ret;
    }
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_begin_impl));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_proc_pre));
    if (!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self)))) {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
      ret += 5;
    }
    return ret;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
bool ligProcedure_formals_cons_pred(ligCell *lhs, ligCell *rhs,
                                    /*@unused@*/ void *userd) {
  LIGEIA_TRACE_DEBUG("ligProcedure_formals_cons_pred");
  (void)userd;
  if (ligCell_p_is_nil(rhs)) {
    LIGEIA_TRACE_ALWAYS("ligProcedure_formals_cons_pred: "
                        "ligCell_p_is_nil(rhs)");
    return false;
  }
  if (!ligCell_is_symbol(rhs)) {
    LIGEIA_TRACE_ALWAYS("ligProcedure_formals_cons_pred: "
                        "!ligCell_is_symbol(rhs)");
    return false;
  }
  if (NULL != lhs && ligCell_is_eqv(lhs, rhs)) {
    LIGEIA_TRACE_ALWAYS("ligProcedure_formals_cons_pred: "
                        "ligCell_is_eqv(lhs, rhs)");
    return false;
  }
  return true;
}
/* ------------------------------------------------------------------------- */
ligReturn ligProcedure_formals_cons(ligProcedure *procedure,
                                    ligCell *     a_formals) {
  LIGEIA_TRACE_DEBUG("ligProcedure_formals_cons");
  LIGEIA_ASSERT(procedure);
  LIGEIA_ASSERT(!ligCell_p_is_nil(a_formals));
  LIGEIA_ASSERT(ligCell_is_cons(a_formals));
  {
    ligConsResult cons_res =
        ligCons_result(a_formals, &ligProcedure_formals_cons_pred, NULL);
    size_t len = ligConsResult_length(cons_res);
    if (INT8_MAX < len) {
      LIGEIA_TRACE_ALWAYS("ligProcedure_formals_cons: invalid formals.");
      return LIGEIA_RETURN_INVALID_ARGS;
    }
    procedure->argc = (int8_t)len;
    switch (ligConsResult_type(cons_res)) {
      case LIGEIA_CONS_TYPE_PROPER:
        procedure->type = LIGEIA_PROCEDURE_TYPE_FIXED;
        return LIGEIA_RETURN_OK;
      case LIGEIA_CONS_TYPE_IMPROPER:
        procedure->type = LIGEIA_PROCEDURE_TYPE_REST;
        --procedure->argc;
        return LIGEIA_RETURN_OK;
      default:
        LIGEIA_TRACE_ALWAYS("ligProcedure_formals_cons: invalid formals.");
        return LIGEIA_RETURN_INVALID_ARGS;
    }
  }
}
/* ------------------------------------------------------------------------- */
ligReturn ligProcedure_new(ligCell *self, ligCell *a_env, ligCell *a_formals,
                           ligCell *a_body) {
  LIGEIA_TRACE_DEBUG("ligProcedure_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_env) || ligCell_is_cons(a_env));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_formals) || ligCell_is_cons(a_formals) ||
                ligCell_is_symbol(a_formals));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_body) || ligCell_is_cons(a_body));
  {
    ligProcedure *procedure = ligCell_procedure_ptr(self);
    ligCell_set_procedure(self);
    ligCons_set_immutable(a_env);
    ligCons_set_immutable(a_formals);
    ligCons_set_immutable(a_body);
    procedure->env     = a_env;
    procedure->formals = a_formals;
    procedure->body    = a_body;
    if (ligCell_p_is_nil(a_formals)) {
      procedure->type = LIGEIA_PROCEDURE_TYPE_FIXED;
      procedure->argc = 0;
    } else {
      const ligCell_type_t cst = ligCell_cst(a_formals);
      switch (cst) {
        case LIGEIA_CST_FALSE:
          LIGEIA_TRACE_ALWAYS("ligProcedure_new: invalid formals.");
          return LIGEIA_RETURN_INVALID_ARGS;
        case LIGEIA_CST_CONS:
          return ligProcedure_formals_cons(procedure, a_formals);
        case LIGEIA_CST_TRUE:
          LIGEIA_TRACE_ALWAYS("ligProcedure_new: invalid formals.");
          return LIGEIA_RETURN_INVALID_ARGS;
        default:
          switch (ligCell_cst2cnct(cst)) {
            case LIGEIA_CNCT_SYMBOL:
              procedure->type = LIGEIA_PROCEDURE_TYPE_LIST;
              procedure->argc = -1;
              break;
            default:
              LIGEIA_TRACE_ALWAYS("ligProcedure_new: invalid formals.");
              return LIGEIA_RETURN_INVALID_ARGS;
          }
          break;
      }
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_proc_bindings(ligSECD *self, const int8_t proc_argc) {
  LIGEIA_TRACE_DEBUG("ligSECD_proc_bindings");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_procedure(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  ligRetExp(ligSECD_env_push_nil(self));
  {
    int8_t i = 0;
    for (; i < proc_argc; ++i) {
      { /* argv */
        ligCell *c = ligCell_cadr(ligSECD_stack(self));
        int8_t   j = 0;
        for (; j < i; ++j) {
          c = ligCell_cdr(c);
        }
        ligRetExp(ligSECD_stack_push(self, ligCell_car(c))); /* invalid */
      }
      { /* formals */
        ligCell *c =
            ligCell_procedure_ptr(ligCell_cadr(ligSECD_stack(self)))->formals;
        int8_t j = 0;
        for (; j < i; ++j) {
          c = ligCell_cdr(c);
        }
        ligRetExp(ligSECD_stack_push(self, ligCell_car(c))); /* invalid */
      }
      ligRetExp(ligSECD_define_impl(self));
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_proc_pre(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_proc_pre");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_procedure(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))) ||
                ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  { /*
     * When ligSECD_***_push_*** is called, proc and argv are invalid.
     */
    ligProcedure *proc      = ligCell_procedure_ptr(ligSECD_stack_top(self));
    int8_t        proc_argc = proc->argc;

    ligCell *     argv     = ligCell_cadr(ligSECD_stack(self));
    ligConsResult cons_res = ligCons_result(argv, NULL, NULL);
    size_t        argc     = ligConsResult_length(cons_res);
    {
      ligConsType arg_type = ligConsResult_type(cons_res);
      if (LIGEIA_CONS_TYPE_PROPER != arg_type) {
        LIGEIA_TRACE_ALWAYS("ligSECD_proc_pre: invalid args.");
        return LIGEIA_RETURN_INVALID_ARGS;
      }
      if (INT8_MAX < argc) {
        LIGEIA_TRACE_ALWAYS("ligProcedure_proc_pre: too long args.");
        return LIGEIA_RETURN_INVALID_ARGC;
      }
    }

    ligSECD_env_set(self, proc->env);
    switch (proc->type) {
      case LIGEIA_PROCEDURE_TYPE_FIXED:
        if ((int8_t)argc != proc_argc) {
          LIGEIA_TRACE_ALWAYS("ligSECD_proc_pre: wrong number args.");
          return LIGEIA_RETURN_INVALID_ARGC;
        }
        if (0 != proc_argc) {
          ligRetExp(ligSECD_proc_bindings(self, proc_argc)); /* invalid */
        }
        break;
      case LIGEIA_PROCEDURE_TYPE_LIST:
        ligRetExp(ligSECD_stack_swap(self)); /* invalid */
        ligRetExp(ligSECD_stack_push(
            self, ligCell_procedure_ptr(ligCell_cadr(ligSECD_stack(self)))
                      ->formals));
        ligRetExp(ligSECD_env_push_nil(self));
        ligRetExp(ligSECD_define_impl(self));
        break;
      case LIGEIA_PROCEDURE_TYPE_REST:
        if (argc < (size_t)proc_argc) {
          LIGEIA_TRACE_ALWAYS("ligSECD_proc_pre: wrong number args.");
          return LIGEIA_RETURN_INVALID_ARGC;
        }
        ligRetExp(ligSECD_proc_bindings(self, proc_argc)); /* invalid */
        {                                                  /* rest */
          {                                                /* argv */
            ligCell *cell = ligCell_cadr(ligSECD_stack(self));
            int8_t   j    = 0;
            for (; j < proc_argc; ++j) {
              cell = ligCell_cdr(cell);
            }
            ligRetExp(ligSECD_stack_push(self, cell));
          }
          { /* formals */
            ligCell *cell =
                ligCell_procedure_ptr(ligCell_cadr(ligSECD_stack(self)))
                    ->formals;
            int8_t j = 0;
            for (; j < proc_argc; ++j) {
              cell = ligCell_cdr(cell);
            }
            ligRetExp(ligSECD_stack_push(self, cell));
          }
          ligRetExp(ligSECD_define_impl(self));
        }
        break;
      default:
        LIGEIA_ASSERT_ALWAYS(0 && NULL != "ligSECD_proc_pre: not reachable.");
        return LIGEIA_RETURN_UNKNOWN;
    }
    ligSECD_stack_set(self,
                      ligCell_procedure_ptr(ligSECD_stack_top(self))->body);
    return LIGEIA_RETURN_OK;
  }
}
/* ========================================================================= */
static ligReturn ligSECD_proc_close_cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_proc_close_cons");
  LIGEIA_ASSERT(self);
#if 1
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack_top(self)));
  {
    size_t cnt = 0;
    for (; !ligCell_p_is_nil(ligSECD_stack_top(self)); ++cnt) {
      ligRetExp(ligSECD_stack_head(self));
      if (ligCell_p_is_nil(ligSECD_stack_top(self))) {
        /* PASS */
      } else {
        ligCell_type_t cst = ligCell_cst(ligSECD_stack_top(self));
        switch (cst) {
          case LIGEIA_CST_CONS: {
            ligRetExp(ligSECD_proc_close_cons(self)); /* C recursive */
            break;
          }
          case LIGEIA_CST_SYMBOL: {
            /* TODO */
            /*
              fprintf(stdout, " * ");
              ligCell_display(stdout, ligSECD_stack_top(self));
              (void) putc('\n', stdout);
              (void) fflush(stdout);
            */
            break;
          }
          default:
            break;
        }
      }
      ligRetExp(ligSECD_stack_to_work(self));
    }
    for (; 0 != cnt; --cnt) {
      ligRetExp(ligSECD_work_to_stack(self));
      ligRetExp(ligSECD_stack_cons(self));
    }
    return LIGEIA_RETURN_OK;
  }
#else
  (void)self;
  return LIGEIA_RETURN_OK;
#endif
}
/* ------------------------------------------------------------------------- */
ligReturn ligSECD_proc_close(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_proc_close");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(ligCell_is_cons(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_procedure(ligSECD_stack_top(self)));
  {
    /*
      ligSECD_env_set(
      self, ligCell_procedure_ptr(ligSECD_stack_top(self))->env);
    */
    ligRetExp(ligSECD_stack_push(
        self, ligCell_procedure_ptr(ligSECD_stack_top(self))->body));
    ligRetExp(ligSECD_proc_close_cons(self));
    ligRetExp(ligSECD_stack_pop(self));
    return LIGEIA_RETURN_OK;
  }
}
