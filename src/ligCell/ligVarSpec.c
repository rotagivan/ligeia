/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligVarSpec.c
 *  @brief ligVarSpec.c
 *
 *  ligVarSpec.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/09
 *  @date 2018/07/04
 */

#include "./ligVarSpec.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <stdio.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligVarSpec_cnum_cst(/*@null@*/ const ligCell*);
static void             ligVarSpec_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligVarSpec_migration(ligCell*, ligMemory*);
static bool             ligVarSpec_is_eqv(const ligCell*, const ligCell*);
static ligReturn        ligSECD_quasi_VarSpec(ligSECD*);
static ligReturn        ligSECD_eval_VarSpec(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligVarSpec_VTBL = {
  &ligVarSpec_cnum_cst,
  &ligVarSpec_display,
  &ligVarSpec_migration,
  &ligVarSpec_is_eqv,
  NULL,         /* expand */
  &ligSECD_quasi_VarSpec,
  &ligSECD_eval_VarSpec,
  NULL,         /* call */
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligVarSpec_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligVarSpec_cnum_cst");
  LIGEIA_ASSERT(ligCell_p_is_nil(self) || !ligCell_p_is_nil(self));
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligVarSpec_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligVarSpec_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  ligCell_display(f, ligCell_car(ligCell_varspec_cell(self)));
}
/* ========================================================================= */
ligReturn ligVarSpec_migration(/*@unused@*/ ligCell *self,
                               /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligVarSpec_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  return ligMemory_migration(memory, &(ligCell_varspec_cell(self)));
}
/* ========================================================================= */
bool ligVarSpec_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligVarSpec_is_eqv");
  LIGEIA_ASSERT(!ligCell_p_is_nil(l));
  LIGEIA_ASSERT(ligCell_is_varspec(l));
  /* TODO(hanepjiv): use symbol is_eqv */
  return (ligCell_is_varspec(r) &&
          ligCell_is_eqv(ligCell_varspec_cell(l), ligCell_varspec_cell(r)));
}
/* ========================================================================= */
ligReturn ligSECD_quasi_VarSpec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_VarSpec");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_VarSpec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_VarSpec");
  LIGEIA_ASSERT(self);
  (void) self;
  LIGEIA_ASSERT(0); /* TODO */
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligVarSpec_new(ligCell *self, ligCell* a_Cell) {
  LIGEIA_TRACE_DEBUG("ligVarSpec_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(!ligCell_p_is_nil(a_Cell) && ligCell_is_cons(a_Cell));
  {
    LIGEIA_ASSERT(0); /* TODO */
    ligCell_set_varspec(self);
    ligCell_varspec_cell(self) = a_Cell;
  }
}
