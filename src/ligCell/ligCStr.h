/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCStr.h
 *  @brief ligCStr.h
 *
 *  ligCStr.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/27
 *  @date 2018/05/22
 */

#ifndef LIGEIA_LIGCELL_LIGCSTR_H_
#define LIGEIA_LIGCELL_LIGCSTR_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC const LIGEIA_VTBL_t ligCStr_VTBL;
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  void          LIGEIA_CALL ligCStr_new(ligCell*, const char*, size_t);
  extern LIGEIA_DECLSPEC
  size_t        LIGEIA_CALL ligCStr_cnum(size_t);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGCSTR_H_ */
