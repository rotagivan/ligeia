/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligDefine.c
 *  @brief ligDefine.c
 *
 *  ligDefine.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/18
 */

#include "./ligDefine.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligDefine_cnum_cst(/*@null@*/ const ligCell*);
static void             ligDefine_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligDefine_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_quasi_Define(ligSECD*);
static ligReturn        ligSECD_eval_Define(ligSECD*);
static ligReturn        ligSECD_call_Define(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligDefine_VTBL = {
  &ligDefine_cnum_cst,
  &ligDefine_display,
  &ligDefine_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  &ligSECD_quasi_Define,
  &ligSECD_eval_Define,
  &ligSECD_call_Define,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligDefine_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligDefine_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligDefine_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligDefine_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "define");
}
/* ========================================================================= */
ligReturn ligDefine_migration(/*@unused@*/ ligCell *self,
                           /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligDefine_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Define(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Define");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Define(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Define");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Define(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Define");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_define(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {
    ligReturn ret = LIGEIA_RETURN_CODE_PUSH_03;
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_define));
    if (!ligCell_is_cons(ligCell_caadr(ligSECD_stack(self)))) {
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_work_to_stack));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval_rest));
      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_to_work));
      ret += 3;
    }
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return ret;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligDefine_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligDefine_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_define(self);
  }
}
