/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligContinue.c
 *  @brief ligContinue.c
 *
 *  ligContinue.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/05
 */

#include "./ligContinue.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligContinue_cnum_cst(/*@null@*/ const ligCell *);
static void      ligContinue_display(FILE *, /*@null@*/ const ligCell *);
static ligReturn ligContinue_migration(ligCell *, ligMemory *);
static ligReturn ligSECD_quasi_Continue(ligSECD *);
static ligReturn ligSECD_eval_Continue(ligSECD *);
static ligReturn ligSECD_call_Continue(ligSECD *);
/* ========================================================================= */
const LIGEIA_VTBL_t ligContinue_VTBL = {
    &ligContinue_cnum_cst,
    &ligContinue_display,
    &ligContinue_migration,
    NULL, /* is_eqv */
    NULL, /* expand */
    &ligSECD_quasi_Continue,
    &ligSECD_eval_Continue,
    &ligSECD_call_Continue,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligContinue_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligContinue_cnum_cst");
  (void)self;
  return ligCell_cnum_size(sizeof(ligContinue) - ligCell_value_size);
}
/* ========================================================================= */
void ligContinue_display(FILE *                                 f,
                         /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligContinue_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void)self;
  fprintf(f, "#<continuation>");
}
/* ========================================================================= */
ligReturn ligContinue_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligContinue_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  {
    ligContinue *cntn = ligCell_continue_ptr(self);
    ligRetExp(ligMemory_migration(memory, &cntn->stack));
    ligRetExp(ligMemory_migration(memory, &cntn->env));
    ligRetExp(ligMemory_migration(memory, &cntn->code));
    ligRetExp(ligMemory_migration(memory, &cntn->dump));
    ligRetExp(ligMemory_migration(memory, &cntn->work));
    ligRetExp(ligMemory_migration(memory, &cntn->curin));
    ligRetExp(ligMemory_migration(memory, &cntn->curout));
    return ligMemory_migration(memory, &cntn->curerr);
  }
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Continue(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Continue");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Continue(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Continue");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Continue(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Continue");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_continue(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  { /* TODO(hanepjiv): */
    (void)self;
    LIGEIA_ASSERT_ALWAYS(0);
    return LIGEIA_RETURN_INVALID_APPLICABLE;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligContinue_new(ligCell *self, ligCell *a_stack, ligCell *a_env,
                     ligCell *a_code, ligCell *a_dump, ligCell *a_work,
                     intptr_t a_qqlv, ligCell *a_curin, ligCell *a_curout,
                     ligCell *a_curerr) {
  LIGEIA_TRACE_DEBUG("ligContinue_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_stack) || ligCell_is_cons(a_stack));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_env) || ligCell_is_cons(a_env));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_code) || ligCell_is_cons(a_code));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_dump) || ligCell_is_cons(a_dump));
  LIGEIA_ASSERT(ligCell_p_is_nil(a_work) || ligCell_is_cons(a_work));
  LIGEIA_ASSERT(0 <= a_qqlv);
  /* TODO(hanepjiv): port
     LIGEIA_ASSERT(!ligCell_p_is_nil(a_curin)      &&
     ligCell_is_port(a_curin)); LIGEIA_ASSERT(!ligCell_p_is_nil(a_curout)
     && ligCell_is_port(a_curout)); LIGEIA_ASSERT(!ligCell_p_is_nil(a_curerr)
     && ligCell_is_port(a_curerr));
  */
  {
    ligContinue *cntn = ligCell_continue_ptr(self);
    ligCell_set_continue(self);
    ligCons_set_immutable(a_stack);
    ligCons_set_immutable(a_env);
    ligCons_set_immutable(a_code);
    ligCons_set_immutable(a_dump);
    ligCons_set_immutable(a_work);
    cntn->stack  = ligCell_p_notag(a_stack);
    cntn->env    = ligCell_p_notag(a_env);
    cntn->code   = ligCell_p_notag(a_code);
    cntn->dump   = ligCell_p_notag(a_dump);
    cntn->work   = ligCell_p_notag(a_work);
    cntn->qqlv   = a_qqlv;
    cntn->curin  = ligCell_p_notag(a_curin);
    cntn->curout = ligCell_p_notag(a_curout);
    cntn->curerr = ligCell_p_notag(a_curerr);
  }
}
