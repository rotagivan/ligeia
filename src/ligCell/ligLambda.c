/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligLambda.c
 *  @brief ligLambda.c
 *
 *  ligLambda.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/06/29
 */

#include "./ligLambda.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligReturn.h"
#include "../ligMemory.h"
#include "../ligSECD.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t           ligLambda_cnum_cst(/*@null@*/ const ligCell*);
static void             ligLambda_display(FILE*, /*@null@*/ const ligCell*);
static ligReturn        ligLambda_migration(ligCell*, ligMemory*);
static ligReturn        ligSECD_quasi_Lambda(ligSECD*);
static ligReturn        ligSECD_eval_Lambda(ligSECD*);
static ligReturn        ligSECD_call_Lambda(ligSECD*);
/* ========================================================================= */
const LIGEIA_VTBL_t ligLambda_VTBL = {
  &ligLambda_cnum_cst,
  &ligLambda_display,
  &ligLambda_migration,
  NULL,         /* is_eqv */
  NULL,         /* expand */
  &ligSECD_quasi_Lambda,
  &ligSECD_eval_Lambda,
  &ligSECD_call_Lambda,
  NULL,         /* run */
  NULL,         /* foldl */
  NULL,         /* mul */
  NULL,         /* div */
  NULL,         /* add */
  NULL,         /* sub */
};
/* ========================================================================= */
size_t ligLambda_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLambda_cnum_cst");
  (void) self;
  return 1u;
}
/* ========================================================================= */
void ligLambda_display(FILE* f, /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLambda_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void) self;
  fprintf(f, "lambda");
}
/* ========================================================================= */
ligReturn ligLambda_migration(/*@unused@*/ ligCell *self,
                              /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligLambda_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void) self;
  (void) memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_Lambda(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_Lambda");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_Lambda(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_Lambda");
  LIGEIA_ASSERT(self);
  (void) self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_Lambda(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_Lambda");
  LIGEIA_ASSERT(self);
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(ligCell_is_lambda(ligSECD_stack_top(self)));
  LIGEIA_ASSERT(!ligCell_p_is_nil(ligCell_cadr(ligSECD_stack(self))));
  LIGEIA_ASSERT(ligCell_is_cons(ligCell_cadr(ligSECD_stack(self))));
  {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_lambda));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_03;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligLambda_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligLambda_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    ligCell_set_lambda(self);
  }
}
