/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligConstVec.c
 *  @brief ligConstVec.c
 *
 *  ligConstVec.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/09
 *  @date 2018/07/05
 */

#include "./ligConstVec.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <string.h>

#include "../debug.h"
#include "../ligCell.h"
#include "../ligCell/ligCons.h"
#include "../ligCell/ligVector.h"
#include "../ligMemory.h"
#include "../ligReturn.h"
#include "../ligSECD.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static size_t    ligConstVec_cnum_cst(/*@null@*/ const ligCell *);
static void      ligConstVec_display(FILE *, /*@null@*/ const ligCell *);
static ligReturn ligConstVec_migration(ligCell *, ligMemory *);
static ligReturn ligSECD_quasi_ConstVec(ligSECD *);
static ligReturn ligSECD_eval_ConstVec(ligSECD *);
static ligReturn ligSECD_call_ConstVec(ligSECD *);
/* ========================================================================= */
const LIGEIA_VTBL_t ligConstVec_VTBL = {
    &ligConstVec_cnum_cst,
    &ligConstVec_display,
    &ligConstVec_migration,
    NULL, /* is_eqv */
    NULL, /* expand */
    &ligSECD_quasi_ConstVec,
    &ligSECD_eval_ConstVec,
    &ligSECD_call_ConstVec,
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ========================================================================= */
size_t ligConstVec_cnum_cst(/*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligConstVec_cnum_cst");
  (void)self;
  return 1u;
}
/* ========================================================================= */
void ligConstVec_display(FILE *                                 f,
                         /*@null@*/ /*@unused@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligConstVec_display");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  (void)self;
  fprintf(f, "const-vec");
}
/* ========================================================================= */
ligReturn ligConstVec_migration(/*@unused@*/ ligCell *  self,
                                /*@unused@*/ ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligConstVec_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  (void)self;
  (void)memory;
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
ligReturn ligSECD_quasi_ConstVec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_quasi_ConstVec");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_eval_ConstVec(/*@unused@*/ ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_eval_ConstVec");
  LIGEIA_ASSERT(self);
  (void)self;
  return LIGEIA_RETURN_CODE_PUSH_00;
}
/* ========================================================================= */
ligReturn ligSECD_call_ConstVec(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("ligSECD_call_ConstVec");
  LIGEIA_ASSERT(self);
  {
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_set_imm));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list_to_vector));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_list));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_quasi_rest));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_unlist));
    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_stack_pop));
    return LIGEIA_RETURN_CODE_PUSH_06;
  }
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void ligConstVec_new(ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligConstVec_new");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  { ligCell_set_constvec(self); }
}
