/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligPort.h
 *  @brief ligPort.h
 *
 *  ligPort.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/06
 *  @date 2018/05/22
 */

#ifndef LIGEIA_LIGCELL_LIGPORT_H_
#define LIGEIA_LIGCELL_LIGPORT_H_

#include <stdio.h>

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligPortType_ {
    LIGEIA_PORT_TYPE_BEGIN_     = 0,

    LIGEIA_PORT_TYPE_STRING     = LIGEIA_PORT_TYPE_BEGIN_,
    LIGEIA_PORT_TYPE_FILE,

    LIGEIA_PORT_TYPE_END_
  } ligPortType;
  /* ======================================================================= */
  typedef struct ligPortString_ ligPortString;
  struct ligPortString_ {
    ligPortType         type;
    ligCell*            cstr;
    size_t              length;
    size_t              cursor;
  };
  /* ======================================================================= */
  typedef struct ligPortFile_ ligPortFile;
  struct ligPortFile_ {
    ligPortType         type;
    FILE*               file;
  };
  /* ======================================================================= */
  union ligPort {
    ligPortType         type;
    ligPortString       string;
    ligPortFile         file;
  };

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGPORT_H_ */
