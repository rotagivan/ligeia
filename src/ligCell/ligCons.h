/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCons.h
 *  @brief ligCons.h
 *
 *  ligCons.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/28
 *  @date 2018/06/08
 */

#ifndef LIGEIA_LIGCELL_LIGCONS_H_
#define LIGEIA_LIGCELL_LIGCONS_H_

#include <ligeia/ligBegin.h>

#include "../ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  typedef enum ligConsType_ {
    LIGEIA_CONS_LENGTH_SHIFT_   = 2u,
    LIGEIA_CONS_TYPE_MASK_      = 3u, /* 0b0011 */
    LIGEIA_CONS_TYPE_ERROR      = 0u, /* 0b0000 */
    LIGEIA_CONS_TYPE_CYCLIC     = 1u, /* 0b0001 */
    LIGEIA_CONS_TYPE_PROPER     = 2u, /* 0b0010 */
    LIGEIA_CONS_TYPE_IMPROPER   = 3u  /* 0b0011 */
  } ligConsType;
  /* ----------------------------------------------------------------------- */
  typedef uint32_t ligConsResult;
  /* ----------------------------------------------------------------------- */
# define ligConsResult_make(r, t)                                       \
  ((ligConsResult)((((ligConsResult)(r)) << LIGEIA_CONS_LENGTH_SHIFT_) | (t)))
# define ligConsResult_type(r)                                          \
  ((ligConsType)(((ligConsResult)(r)) & LIGEIA_CONS_TYPE_MASK_))
# define ligConsResult_length(r)                                        \
  ((size_t)(((ligConsResult)(r)) >> LIGEIA_CONS_LENGTH_SHIFT_))
  /* ----------------------------------------------------------------------- */
  typedef bool (*ligCons_result_pred_t)(/*@null@*/ ligCell *lhs,
                                        /*@null@*/ ligCell *rhs,
                                        /*@null@*/ void *userd);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligCons_display(FILE*, /*@null@*/ const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCons_migration(ligCell*, ligMemory*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC bool
  LIGEIA_CALL ligCons_is_eqv(const ligCell*, const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_quasi_Cons(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligSECD_eval_Cons(ligSECD*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC void
  LIGEIA_CALL ligCons_set_immutable(ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCons_set_car(ligCell*, /*@null@*/ ligCell*);
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCons_set_cdr(ligCell*, /*@null@*/ ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCons_append(ligCell**, ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC bool
  LIGEIA_CALL ligCons_is_pair(/*@null@*/ const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligConsResult
  LIGEIA_CALL ligCons_result(/*@null@*/ const ligCell*,
                             /*@null@*/ ligCons_result_pred_t pred,
                             /*@null@*/ void* userd);
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC bool
  LIGEIA_CALL ligCons_is_proper(/*@null@*/ const ligCell*);
  /* ----------------------------------------------------------------------- */
  extern LIGEIA_DECLSPEC bool
  LIGEIA_CALL ligCons_is_improper(/*@null@*/ const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC /*@null@*/ ligCell*
  LIGEIA_CALL ligCons_list_last(/*@null@*/ const ligCell*);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC ligReturn
  LIGEIA_CALL ligCons_list_chop_last(ligCell**);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGCELL_LIGCONS_H_ */
