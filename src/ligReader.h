/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligReader.h
 *  @brief ligReader.h
 *
 *  ligReader.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/02
 *  @date 2018/06/13
 */

#ifndef LIGEIA_LIGREADER_H_
#define LIGEIA_LIGREADER_H_

# include <stdio.h>

#include <ligeia/ligBegin.h>

#include "./ligTypes.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  ligReader*     LIGEIA_CALL ligReader_new(void);
  extern LIGEIA_DECLSPEC
  void           LIGEIA_CALL ligReader_delete(/*@null@*/ ligReader *self);
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  ligReturn     LIGEIA_CALL ligSECD_load_file(ligSECD *self,
                                              FILE* file, int close);
  extern LIGEIA_DECLSPEC
  ligReturn     LIGEIA_CALL ligSECD_load_path(ligSECD *self, const char* path);
  extern LIGEIA_DECLSPEC
  ligReturn     LIGEIA_CALL ligSECD_load_str(ligSECD *self, const char* str);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGREADER_H_ */
