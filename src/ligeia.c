/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligeia.c
 *  @brief ligeia.c
 *
 *  ligeia.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/05/31
 *  @date 2018/07/23
 */

#include <ligeia/ligeia.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "./debug.h"
#include "./ligSECD.h"
#include "./ligReader.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  LIGEIA_TRACE_DEBUG("ligeia");
  if (2 != argc) { return 0; }
  {
    ligReturn ret= LIGEIA_RETURN_OK;
    ligSECD *self = ligSECD_new(256*1024);
    if (!self) {
      fprintf(stderr, PACKAGE_STRING ": Failed ligSECD_new\n");
      (void) fflush(stderr);
      goto error_ligSECD_new;
    }
    ret = ligSECD_load_path(self, argv[1]);
    if (LIGEIA_RETURN_OK != ret)  {
      fprintf(stderr,
              PACKAGE_STRING ": Failed ligSECD_load_path(\"%s\")\n", argv[1]);
      (void) fflush(stderr);
      goto error_ligSECD_load_path;
    }
 error_ligSECD_load_path:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
