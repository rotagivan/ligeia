/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligCell.c
 *  @brief ligCell.c
 *
 *  ligCell.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/03
 *  @date 2018/07/19
 */

#include "./ligCell.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "./debug.h"
#include "./ligCell/ligCStr.h"
#include "./ligCell/ligCons.h"
#include "./ligCell/ligConstVec.h"
#include "./ligCell/ligContinue.h"
#include "./ligCell/ligDefine.h"
#include "./ligCell/ligEnvSpec.h"
#include "./ligCell/ligFloat.h"
#include "./ligCell/ligFree.h"
#include "./ligCell/ligInt.h"
#include "./ligCell/ligLambda.h"
#include "./ligCell/ligLet.h"
#include "./ligCell/ligMacTra.h"
#include "./ligCell/ligOperator.h"
#include "./ligCell/ligProcedure.h"
#include "./ligCell/ligQuasiquote.h"
#include "./ligCell/ligQuote.h"
#include "./ligCell/ligSetx.h"
#include "./ligCell/ligSymbol.h"
#include "./ligCell/ligSynClo.h"
#include "./ligCell/ligUnqSpl.h"
#include "./ligCell/ligUnquote.h"
#include "./ligCell/ligVarSpec.h"
#include "./ligCell/ligVector.h"
#include "./ligMemory.h"
#include "./ligReturn.h"
#include "./ligTypes.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
/*
 * get Cell cst (cell slide type).
 *
 * Do *NOT* convert to macro.
 *  In this code 'self' appears twice. This is a problem with macro.
 *  So, we had to write this code as a function.
 */
ligCell_type_t ligCell_cst(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCell_cst");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  {
    const ligCell_type_t type = ligCell_type(self);
    return ((ligCell_type_t)(
        (type & (((type & LIGEIA_CT_CONS_MASK) - 1u) | LIGEIA_CT_CONS_MASK)) >>
        LIGEIA_CST_SHIFT_));
  }
}
/* ========================================================================= */
size_t ligCell_cnum(const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCell_cnum");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  return ligCell_cnum_cst(self, ligCell_cst(self));
}
/* ========================================================================= */
size_t ligCell_cnum_size(size_t size) {
  LIGEIA_TRACE_DEBUG("ligCell_cnum_size");
  size += sizeof(ligCell);
  --size;
  size /= sizeof(ligCell);
  ++size; /* add management cell num */
  return size;
}
/* ========================================================================= */
static const LIGEIA_VTBL_t LIGEIA_VTBL_empty = {
    NULL, /* cnum_cst */
    NULL, /* display */
    NULL, /* migration */
    NULL, /* is_eqv */
    NULL, /* expand */
    NULL, /* quasi */
    NULL, /* eval */
    NULL, /* call */
    NULL, /* run */
    NULL, /* foldl */
    NULL, /* mul */
    NULL, /* div */
    NULL, /* add */
    NULL, /* sub */
};
/* ------------------------------------------------------------------------- */
/* clang-format off */
const LIGEIA_VTBL_t* const LIGEIA_VTBL[LIGEIA_CNCT_SIZE_] = {
  /* LIGEIA_CNCT_DUMMY_ */              &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_FREE */                &ligFree_VTBL,
  /* LIGEIA_CNCT_LOCATION */            &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_ENVSPEC */             &ligEnvSpec_VTBL,
  /* LIGEIA_CNCT_VARSPEC */             &ligVarSpec_VTBL,
  /* LIGEIA_CNCT_ARGIDX */              &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_FLOAT */               &ligFloat_VTBL,
  /* LIGEIA_CNCT_INT */                 &ligInt_VTBL,
  /* LIGEIA_CNCT_CHAR */                &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_CSTR */                &ligCStr_VTBL,
  /* LIGEIA_CNCT_SYMBOL */              &ligSymbol_VTBL,
  /* LIGEIA_CNCT_CONTINUE */            &ligContinue_VTBL,
  /* LIGEIA_CNCT_PORT */                &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_LET */                 &ligLet_VTBL,
  /* LIGEIA_CNCT_DEFINE */              &ligDefine_VTBL,
  /* LIGEIA_CNCT_SETX */                &ligSetx_VTBL,
  /* LIGEIA_CNCT_LAMBDA */              &ligLambda_VTBL,
  /* LIGEIA_CNCT_PROCEDURE */           &ligProcedure_VTBL,
  /* LIGEIA_CNCT_FOREIGN */             &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_OPERATOR */            &ligOperator_VTBL,
  /* LIGEIA_CNCT_VECTOR */              &ligVector_VTBL,
  /* LIGEIA_CNCT_CONSTVEC */            &ligConstVec_VTBL,
  /* LIGEIA_CNCT_SYNCLO */              &ligSynClo_VTBL,
  /* LIGEIA_CNCT_MACTRA */              &ligMacTra_VTBL,
  /* LIGEIA_CNCT_UNDEFINED */           &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_QUOTE */               &ligQuote_VTBL,
  /* LIGEIA_CNCT_QUASIQUOTE */          &ligQuasiquote_VTBL,
  /* LIGEIA_CNCT_QUOTE */               &ligUnquote_VTBL,
  /* LIGEIA_CNCT_QUOTE */               &ligUnqSpl_VTBL,
  /* LIGEIA_CNCT_ERROR */               &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_STRING */              &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_ELLIPSIS */            &LIGEIA_VTBL_empty,
  /* LIGEIA_CNCT_EOF */                 &LIGEIA_VTBL_empty,
};
/* clang-format on */
/* ========================================================================= */
size_t ligCell_cnum_cst(const ligCell *self, ligCell_type_t cst) {
  LIGEIA_TRACE_DEBUG("ligCell_cnum_cst");
  switch (cst) {
    case LIGEIA_CST_FALSE: {
      return 1;
    }
    case LIGEIA_CST_CONS: {
      return 1;
    }
    case LIGEIA_CST_TRUE: {
      return 1;
    }
    default: {
      ligCellNotConsType cnct = ligCell_cst2cnct(cst);
      LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
      LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->cnum_cst);
      return LIGEIA_VTBL[cnct]->cnum_cst(self);
    }
  }
}
/* ========================================================================= */
void ligCell_display(FILE *file, /*@null@*/ const ligCell *self) {
  LIGEIA_TRACE_DEBUG("ligCell_display");
  if (ligCell_p_is_nil(self)) {
    fprintf(file, "'()");
  } else {
    ligCell_type_t cst = ligCell_cst(self);
    switch (cst) {
      case LIGEIA_CST_FALSE: {
        fprintf(file, "#f");
        break;
      }
      case LIGEIA_CST_CONS: {
        ligCons_display(file, self);
        break;
      }
      case LIGEIA_CST_TRUE: {
        fprintf(file, "#t");
        break;
      }
      default: {
        ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->display);
        LIGEIA_VTBL[cnct]->display(file, self);
        break;
      }
    }
  }
}
/* ========================================================================= */
ligReturn ligCell_migration(ligCell *self, ligMemory *memory) {
  LIGEIA_TRACE_DEBUG("ligCell_migration");
  LIGEIA_ASSERT(!ligCell_p_is_nil(self));
  LIGEIA_ASSERT(memory);
  {
    ligCell_type_t cst = ligCell_cst(self);
    switch (cst) {
      case LIGEIA_CST_FALSE: {
        return LIGEIA_RETURN_OK;
      }
      case LIGEIA_CST_CONS: {
        return ligCons_migration(self, memory);
      }
      case LIGEIA_CST_TRUE: {
        return LIGEIA_RETURN_OK;
      }
      default: {
        ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->migration);
        return LIGEIA_VTBL[cnct]->migration(self, memory);
      }
    }
  }
}
/* ========================================================================= */
bool ligCell_is_eqv(const ligCell *l, const ligCell *r) {
  LIGEIA_TRACE_DEBUG("ligCell_is_eqv");
  if (ligCell_p_is_nil(l)) {
    return ligCell_p_is_nil(r);
  } else if (ligCell_p_is_nil(r)) {
    return ligCell_p_is_nil(l);
  } else {
    ligCell_type_t cst = ligCell_cst(l);
    switch (cst) {
      case LIGEIA_CST_FALSE: {
        return (LIGEIA_CST_FALSE == ligCell_cst(r));
      }
      case LIGEIA_CST_CONS: {
        return ligCons_is_eqv(l, r);
      }
      case LIGEIA_CST_TRUE: {
        return (LIGEIA_CST_TRUE == ligCell_cst(r));
      }
      default: {
        ligCellNotConsType cnct = ligCell_cst2cnct(cst);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]);
        LIGEIA_ASSERT(LIGEIA_VTBL[cnct]->is_eqv);
        return LIGEIA_VTBL[cnct]->is_eqv(l, r);
      }
    }
  }
}
