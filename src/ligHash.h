/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file ligHash.h
 *  @brief ligHash.h
 *
 *  ligHash.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/27
 *  @date 2018/06/29
 */

#ifndef LIGEIA_LIGHASH_H_
#define LIGEIA_LIGHASH_H_

#include <inttypes.h>
#include <stdlib.h>

#include <ligeia/ligBegin.h>

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

  /* /////////////////////////////////////////////////////////////////////// */
  /* ======================================================================= */
  extern LIGEIA_DECLSPEC
  uint32_t      LIGEIA_CALL ligHash_combine(uint32_t, const uint8_t*, size_t);

#ifdef __cplusplus
}  /* extern "C" */
#endif  /* __cplusplus */

#endif  /* LIGEIA_LIGHASH_H_ */
