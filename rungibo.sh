#! /bin/env sh

GIBO=gibo

${GIBO} -u | exit 1
${GIBO} \
    Archives \
    Cloud9 \
    Emacs \
    Linux \
    Mercurial \
    Ninja \
    TortoiseGit \
    Vim \
    VisualStudio \
    Windows \
    Xcode \
    macOS \
    Android \
    Autotools \
    CMake \
    C \
    C++ \
    Elisp \
    Python \
    TeX \
    > .gitignore
