;;;; -*- mode:scheme; coding:utf-8-unix; -*-

;;;; @file fib.scm
;;;; @brief fib.scm
;;;;
;;;; fib.scm
;;;;
;;;; Copyright 2018 hanepjiv
;;;;
;;;; @author hanepjiv <hanepjiv@gmail.com>
;;;; @since 2018/07/13
;;;; @date 2018/07/15

(import (chibi))

(letrec ((fib
          (lambda (n)
            (cond ((<= n 1)
                   n)
                  (else
                   (+ (fib (- n 2))
                      (fib (- n 1))))))))
  (display (fib 30))
  (newline))


#|
(define fib #f)
(set! fib (lambda (n) (cond ((<= n 1) n)
                            (else (+ (fib (- n 2)) (fib (- n 1)))))))
|#

#|
(display ((lambda (n)
            ((lambda (fib n)
               (set! fib
                     (lambda (n)
                       (cond ((<= n 1)
                              n)
                             (else
                              (+ (fib (- n 2))
                                 (fib (- n 1)))))))
               (fib n)) #f n)) 30))
|#
