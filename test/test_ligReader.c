/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligReader.c
 *  @brief test_ligReader.c
 *
 *  test_ligReader.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/07/19
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../src/debug.h"
#include "../src/ligCell.h"
#include "../src/ligSECD.h"
#include "../src/ligReader.h"

#include "./test.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static ligReturn test_reader(ligSECD *self);
ligReturn test_reader(ligSECD *self) {
  LIGEIA_TRACE_ALWAYS("test_reader");
  {
    ligRetExp(ligSECD_reset(self));
    {
      ligRetExp(ligSECD_load_str(self, "#| block comment #| nested |# |#\n"));
      ligTestExp(ligCell_p_is_nil(ligSECD_stack(self)));
    }
    { LIGEIA_TRACE_ALWAYS("line comment");
      ligRetExp(ligSECD_load_str(self, "; line comment\n"));
      ligTestExp(ligCell_p_is_nil(ligSECD_stack(self)));
    }
    { LIGEIA_TRACE_ALWAYS("datum comment");
      ligRetExp(ligSECD_load_str(self, "#; 1 -2"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_int(ligSECD_stack_top(self)));
      ligTestExp(-2 == ligCell_int(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("cons");
      ligRetExp(ligSECD_load_str(self, "(cons 1 ())"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("car, cdr #0");
      ligRetExp(ligSECD_load_str(self, "(cons (car (cons 1 \"ligeia\"))"
                              "            (cdr (cons \"ligeia\" 2)))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("car, cdr #1");
      ligRetExp(ligSECD_load_str(self, "((car (cons car cdr)) (cons #t #f))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_strict_true(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("vector");
      ligRetExp(ligSECD_load_str(self, "#(1 2)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_vector(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_immutable(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("quote #0");
      ligRetExp(ligSECD_load_str(self, "(quote 1)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_int(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("quote #1");
      ligRetExp(ligSECD_load_str(self, "'(1 2)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("quasiquote #0");
      ligRetExp(ligSECD_load_str(self, "(quasiquote 1)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_int(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("quasiquote #1");
      ligRetExp(ligSECD_load_str(self, "`(1 2)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("unquote #0");
      ligRetExp(ligSECD_load_str(self, "(quasiquote ((unquote (cons 1 2))))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(ligCell_p_is_nil(ligCell_cdr(ligSECD_stack_top(self))));
      ligTestExp(1 == ligCell_int(ligCell_caar(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdar(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("unquote #1");
      ligRetExp(ligSECD_load_str(self, "`(,(cons 1 2))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(ligCell_p_is_nil(ligCell_cdr(ligSECD_stack_top(self))));
      ligTestExp(1 == ligCell_int(ligCell_caar(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdar(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("unquote-splicing #0");
      ligRetExp(ligSECD_load_str(self, "`(1 (unquote-splicing (list 2)))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("unquote-splicing #1");
      ligRetExp(ligSECD_load_str(self, "`(1 ,@(list 2))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("unquote-splicing #2");
      ligRetExp(ligSECD_load_str(self, "`#(1 ,@(list 2))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_vector(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_immutable(ligSECD_stack_top(self)));
    }
    { LIGEIA_TRACE_ALWAYS("lambda #0");
      ligRetExp(ligSECD_load_str(self, "(let ((x (lambda () 1))"
                                 "            (y 2))"
                                 "           (cons (x) y))"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("lambda #1");
      ligRetExp(ligSECD_load_str(self, "((lambda (x y) (cons x y)) 1 2)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
    }
    { LIGEIA_TRACE_ALWAYS("define #0");
      ligRetExp(ligSECD_load_str(self, "(define myfunc"
                                 "              (lambda (a b) (cons a b)))"
                                 "      (define x 3)"
                                 "      (set! x 1)"
                                 "      (myfunc x 2)"));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(2 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
    }
    /*
      {
      ligRetExp(ligSECD_load_str(self, "(make-syntactic-closure '() '() 1)"));
      ligSECD_print(self);
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack(self)));
      ligTestExp(!ligCell_p_is_nil(ligSECD_stack_top(self)));
      }
    */
  }
  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    int ret = 1;
    ligSECD *self = ligSECD_new(512*1024);
    if (!self) {
      LIGEIA_TRACE_ALWAYS("failed ligSECD_new.");
      goto error_ligSECD_new;
    }

    if (LIGEIA_RETURN_OK != (ret = test_reader(self)))  { goto error_test; }

 error_test:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
