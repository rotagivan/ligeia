/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligCell.c
 *  @brief test_ligCell.c
 *
 *  test_ligCell.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/01
 *  @date 2018/07/05
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../src/ligCell.c"

#include "../src/debug.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static void checks_for_cell(void);
static void checks_for_cell_type(void);
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void checks_for_cell(void)
{
  ligCell c, *this_ = NULL;

  LIGEIA_ASSERT_ALWAYS(ligCell_p_is_nil(this_));

  this_ = &c;
  LIGEIA_ASSERT_ALWAYS(!ligCell_p_is_nil(this_));

  ligCell_reset_precious(this_);
  LIGEIA_ASSERT_ALWAYS(!ligCell_is_precious(this_));

  ligCell_set_precious(this_);
  LIGEIA_ASSERT_ALWAYS(ligCell_is_precious(this_));

  ligCell_reset_immutable(this_);
  LIGEIA_ASSERT_ALWAYS(!ligCell_is_immutable(this_));

  ligCell_set_immutable(this_);
  LIGEIA_ASSERT_ALWAYS(ligCell_is_immutable(this_));
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static const int
check_fct_ret_tbl[LIGEIA_CNCT_SIZE_+2][LIGEIA_CNCT_SIZE_+2] = {
  /* false */
  {1,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* true */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* cons */
  {0,1,1, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},

  /* free */
  {0,1,0, 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* location */
  {0,1,0, 0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* envspec */
  {0,1,0, 0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* varspec */
  {0,1,0, 0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* argidx */
  {0,1,0, 0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* float */
  {0,1,0, 0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* int */
  {0,1,0, 0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* char */
  {0,1,0, 0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* cstr */
  {0,1,0, 0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* symbol */
  {0,1,0, 0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* continue */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* port */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* let */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* define */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* setx */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* lambda */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* proc */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* foreign */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* operator */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0},
  /* vector */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
  /* constvec */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
  /* synclo */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},
  /* mactra */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0},
  /* undef */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
  /* quote */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
  /* qquote */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0},
  /* unquote */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
  /* unqspl */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
  /* error */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
  /* string */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},
  /* ellipsis */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
  /* eof */
  {0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
};
/* ========================================================================= */
#define checks_fct_list_entry(type, i0, i1)                     \
  LIGEIA_ASSERT_ALWAYS(ligCell_is_##type(this_) ==              \
                       (bool)(check_fct_ret_tbl[(i0)][(i1)]));
/* ------------------------------------------------------------------------- */
#define checks_fct_list_(i)                             \
  LIGEIA_ASSERT_ALWAYS(!ligCell_p_is_nil(this_));       \
  checks_fct_list_entry(false,        (i),  0);         \
  checks_fct_list_entry(broad_true,   (i),  1);         \
  checks_fct_list_entry(cons,         (i),  2);         \
  checks_fct_list_entry(free,         (i),  3);         \
  checks_fct_list_entry(location,     (i),  4);         \
  checks_fct_list_entry(envspec,      (i),  5);         \
  checks_fct_list_entry(varspec,      (i),  6);         \
  checks_fct_list_entry(argidx,       (i),  7);         \
  checks_fct_list_entry(float,        (i),  8);         \
  checks_fct_list_entry(int,          (i),  9);         \
  checks_fct_list_entry(char,         (i), 10);         \
  checks_fct_list_entry(cstr,         (i), 11);         \
  checks_fct_list_entry(symbol,       (i), 12);         \
  checks_fct_list_entry(continue,     (i), 13);         \
  checks_fct_list_entry(port,         (i), 14);         \
  checks_fct_list_entry(let,          (i), 15);         \
  checks_fct_list_entry(define,       (i), 16);         \
  checks_fct_list_entry(setx,         (i), 17);         \
  checks_fct_list_entry(lambda,       (i), 18);         \
  checks_fct_list_entry(procedure,    (i), 19);         \
  checks_fct_list_entry(foreign,      (i), 20);         \
  checks_fct_list_entry(operator,     (i), 21);         \
  checks_fct_list_entry(vector,       (i), 22);         \
  checks_fct_list_entry(constvec,     (i), 23);         \
  checks_fct_list_entry(synclo,       (i), 24);         \
  checks_fct_list_entry(mactra,       (i), 25);         \
  checks_fct_list_entry(undef,        (i), 26);         \
  checks_fct_list_entry(quote,        (i), 27);         \
  checks_fct_list_entry(quasiquote,   (i), 28);         \
  checks_fct_list_entry(unquote,      (i), 29);         \
  checks_fct_list_entry(unqspl,       (i), 30);         \
  checks_fct_list_entry(error,        (i), 31);         \
  checks_fct_list_entry(string,       (i), 32);         \
  checks_fct_list_entry(ellipsis,     (i), 33);         \
  checks_fct_list_entry(eof,          (i), 34);
/* ========================================================================= */
#define checks_fct_list(i)                              \
  checks_fct_list_(i)                                   \
  ligCell_set_precious(this_);   checks_fct_list_(i)    \
  ligCell_set_immutable(this_);  checks_fct_list_(i)    \
  ligCell_reset_precious(this_); checks_fct_list_(i)
/* ========================================================================= */
void checks_for_cell_type(void)
{
  ligCell c, *this_ = &c;
  this_->type = 0; ligCell_set_false(this_);        checks_fct_list( 0);
  this_->type = 0; ligCell_set_true(this_);         checks_fct_list( 1);
  this_->type = 0; ligCell_set_cons(this_);         checks_fct_list( 2);
  this_->type = 0; ligCell_set_free(this_);         checks_fct_list( 3);
  this_->type = 0; ligCell_set_location(this_);     checks_fct_list( 4);
  this_->type = 0; ligCell_set_envspec(this_);      checks_fct_list( 5);
  this_->type = 0; ligCell_set_varspec(this_);      checks_fct_list( 6);
  this_->type = 0; ligCell_set_argidx(this_);       checks_fct_list( 7);
  this_->type = 0; ligCell_set_float(this_);        checks_fct_list( 8);
  this_->type = 0; ligCell_set_int(this_);          checks_fct_list( 9);
  this_->type = 0; ligCell_set_char(this_);         checks_fct_list(10);
  this_->type = 0; ligCell_set_cstr(this_);         checks_fct_list(11);
  this_->type = 0; ligCell_set_symbol(this_);       checks_fct_list(12);
  this_->type = 0; ligCell_set_continue(this_);     checks_fct_list(13);
  this_->type = 0; ligCell_set_port(this_);         checks_fct_list(14);
  this_->type = 0; ligCell_set_let(this_);          checks_fct_list(15);
  this_->type = 0; ligCell_set_define(this_);       checks_fct_list(16);
  this_->type = 0; ligCell_set_setx(this_);         checks_fct_list(17);
  this_->type = 0; ligCell_set_lambda(this_);       checks_fct_list(18);
  this_->type = 0; ligCell_set_procedure(this_);    checks_fct_list(19);
  this_->type = 0; ligCell_set_foreign(this_);      checks_fct_list(20);
  this_->type = 0; ligCell_set_operator(this_);     checks_fct_list(21);
  this_->type = 0; ligCell_set_vector(this_);       checks_fct_list(22);
  this_->type = 0; ligCell_set_constvec(this_);     checks_fct_list(23);
  this_->type = 0; ligCell_set_synclo(this_);       checks_fct_list(24);
  this_->type = 0; ligCell_set_mactra(this_);       checks_fct_list(25);
  this_->type = 0; ligCell_set_undef(this_);        checks_fct_list(26);
  this_->type = 0; ligCell_set_quote(this_);        checks_fct_list(27);
  this_->type = 0; ligCell_set_quasiquote(this_);   checks_fct_list(28);
  this_->type = 0; ligCell_set_unquote(this_);      checks_fct_list(29);
  this_->type = 0; ligCell_set_unqspl(this_);       checks_fct_list(30);
  this_->type = 0; ligCell_set_error(this_);        checks_fct_list(31);
  this_->type = 0; ligCell_set_string(this_);       checks_fct_list(32);
  this_->type = 0; ligCell_set_ellipsis(this_);     checks_fct_list(33);
  this_->type = 0; ligCell_set_eof(this_);          checks_fct_list(34);
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    checks_for_cell();
    checks_for_cell_type();
    return 0;
  }
}
