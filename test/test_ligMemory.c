/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligMemory.c
 *  @brief test_ligMemory.c
 *
 *  test_ligMemory.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/09
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../src/ligMemory.c"

#include "../src/debug.h"
#include "../src/ligTypes.h"
#include "../src/ligCell.h"

#define POOL_SIZE 10

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static void     test_ligPool_synapse(void);
static void     test_ligPool_akasha(void);
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
void test_ligPool_synapse(void) {
  ligCell *cell = (ligCell*) malloc(sizeof(ligCell) * POOL_SIZE);
  LIGEIA_ASSERT_ALWAYS(cell);
  {
    ligPool pool;
    ligPool_synapse(&pool, cell, POOL_SIZE);
    {
      ligCell *pc = ligPool_synapse_new_cell(&pool, POOL_SIZE + 1);
      LIGEIA_ASSERT_ALWAYS(!pc);
    }
    ligPool_reset(&pool);
    {
      ligCell *pc = ligPool_synapse_new_cell(&pool, 1);
      LIGEIA_ASSERT_ALWAYS(pc);
      LIGEIA_ASSERT_ALWAYS(pc == cell);
      LIGEIA_ASSERT_ALWAYS(pool.next == &cell[1]);
      LIGEIA_ASSERT_ALWAYS(ligPool_is_living(&pool, pc));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, 0));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, pc-1));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, pc + POOL_SIZE));
    }
    ligPool_reset(&pool);
    {
      ligCell *pc = ligPool_synapse_new_cell(&pool, POOL_SIZE);
      LIGEIA_ASSERT_ALWAYS(pc);
      LIGEIA_ASSERT_ALWAYS(pc == cell);
      LIGEIA_ASSERT_ALWAYS(pool.next == &cell[POOL_SIZE]);
      LIGEIA_ASSERT_ALWAYS(NULL == ligPool_synapse_new_cell(&pool, 1));
    }
    ligPool_dispose(&pool);
  }
  free(cell);
}
/* ========================================================================= */
void test_ligPool_akasha(void) {
  ligCell *cell = (ligCell*)malloc(sizeof(ligCell)*POOL_SIZE);
  LIGEIA_ASSERT_ALWAYS(cell);
  {
    ligPool pool;

    ligPool_akasha(&pool, cell, POOL_SIZE);
    {
      ligCell *pc = ligPool_akasha_new_cell(&pool, POOL_SIZE+1);
      LIGEIA_ASSERT_ALWAYS(!pc);
    }
    ligPool_dispose(&pool);

    ligPool_akasha(&pool, cell, POOL_SIZE);
    {
      ligCell *pc = ligPool_akasha_new_cell(&pool, 1);
      LIGEIA_ASSERT_ALWAYS(pc);
      LIGEIA_ASSERT_ALWAYS(pc == cell);
      LIGEIA_ASSERT_ALWAYS(pool.next == &cell[1]);
      LIGEIA_ASSERT_ALWAYS(ligPool_is_living(&pool, pc));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, 0));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, pc - 1));
      LIGEIA_ASSERT_ALWAYS(!ligPool_is_living(&pool, pc + POOL_SIZE));
    }
    ligPool_dispose(&pool);

    ligPool_akasha(&pool, cell, POOL_SIZE);
    {
      ligCell *pc = ligPool_akasha_new_cell(&pool, POOL_SIZE);
      LIGEIA_ASSERT_ALWAYS(pc);
      LIGEIA_ASSERT_ALWAYS(pc == cell);
      LIGEIA_ASSERT_ALWAYS(!ligPool_akasha_new_cell(&pool, 1));
    }
    ligPool_dispose(&pool);
  }
  free(cell);
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    test_ligPool_synapse();
    test_ligPool_akasha();
    return 0;
  }
}
