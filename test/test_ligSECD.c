/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligSECD.c
 *  @brief test_ligSECD.c
 *
 *  test_ligSECD.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>
#include <string.h>

#include "../src/debug.h"
#include "../src/ligCell.h"
#include "../src/ligSECD.h"

#include "./test.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static ligReturn test_swap(ligSECD *self);
ligReturn test_swap(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_swap");

  ligRetExp(ligSECD_reset(self));
  { LIGEIA_TRACE_ALWAYS("stack_swap");
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligTestExp(77 == ligCell_int(ligSECD_stack_top(self)));

    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligTestExp(88 == ligCell_int(ligSECD_stack_top(self)));

    ligRetExp(ligSECD_stack_swap(self));
    ligTestExp(77 == ligCell_int(ligSECD_stack_top(self)));

    ligRetExp(ligSECD_stack_swap(self));
    ligTestExp(88 == ligCell_int(ligSECD_stack_top(self)));
  }
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
static ligReturn test_cons(ligSECD *self);
ligReturn test_cons(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_cons");

  ligRetExp(ligSECD_reset(self));
  { LIGEIA_TRACE_ALWAYS("stack_cons");
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligRetExp(ligSECD_stack_cons(self));

    ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
    ligTestExp(88 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    ligTestExp(77 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
  }

  ligRetExp(ligSECD_reset(self));
  { LIGEIA_TRACE_ALWAYS("stack_xcons");
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligRetExp(ligSECD_stack_xcons(self));

    ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
    ligTestExp(77 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    ligTestExp(88 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
  }
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
static ligReturn test_append(ligSECD *self);
ligReturn test_append(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_append");

  ligRetExp(ligSECD_reset(self));
  { LIGEIA_TRACE_ALWAYS("stack_append");
    ligRetExp(ligSECD_stack_push_nil(self));
    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_push_nil(self));
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_append(self));

    ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
    ligTestExp(77 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    ligTestExp(88 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
  }

  ligRetExp(ligSECD_reset(self));
  { LIGEIA_TRACE_ALWAYS("stack_xappend");
    ligRetExp(ligSECD_stack_push_nil(self));
    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_push_nil(self));
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_xappend(self));

    ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
    ligTestExp(88 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    ligTestExp(77 == ligCell_int(ligCell_cadr(ligSECD_stack_top(self))));
  }
  return LIGEIA_RETURN_OK;
}
/* ========================================================================= */
static ligReturn test_define(ligSECD *self);
ligReturn test_define(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_define");
  {
    const size_t PACKAGE_LENGTH = strlen(PACKAGE);

    ligRetExp(ligSECD_reset(self));
    { LIGEIA_TRACE_ALWAYS("define_impl");
      ligRetExp(ligSECD_stack_push_int(self, 77));
      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_define_impl(self));

      ligTestExp(ligCell_is_symbol(ligCell_caar(ligSECD_env_top(self))));
      ligTestExp(77 == ligCell_int(ligCell_cdar(ligSECD_env_top(self))));
    }

    { LIGEIA_TRACE_ALWAYS("find");
      ligRetExp(ligSECD_stack_push_int(self, 77));
      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_define_impl(self));

      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_find(self));

      ligTestExp(77 == ligCell_int(ligSECD_stack_top(self)));
    }

    ligRetExp(ligSECD_reset(self));
    { LIGEIA_TRACE_ALWAYS("define");
      ligRetExp(ligSECD_stack_push_int(self, 77));
      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_define(self));

      ligRetExp(ligSECD_stack_push_int(self, 88));
      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_define(self));

      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_find(self));

      ligTestExp(88 == ligCell_int(ligSECD_stack_top(self)));
    }
    return LIGEIA_RETURN_OK;
  }
}
/* ========================================================================= */
static ligReturn test_find(ligSECD *self);
ligReturn test_find(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_find");
  {
    const size_t PACKAGE_LENGTH = strlen(PACKAGE);

    ligRetExp(ligSECD_reset(self));
    { LIGEIA_TRACE_ALWAYS("find");
      ligRetExp(ligSECD_stack_push_int(self, 77));
      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_define_impl(self));

      ligRetExp(ligSECD_stack_push_symbol(self, PACKAGE, PACKAGE_LENGTH));
      ligRetExp(ligSECD_find(self));

      ligTestExp(77 == ligCell_int(ligSECD_stack_top(self)));
    }
    return LIGEIA_RETURN_OK;
  }
}
/* ========================================================================= */
static ligReturn test_eval(ligSECD *self);
ligReturn test_eval(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_eval");
  {
    ligRetExp(ligSECD_reset(self));

    ligRetExp(ligSECD_stack_push_nil(self));
    ligRetExp(ligSECD_stack_push_int(self, 77));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_push_int(self, 88));
    ligRetExp(ligSECD_stack_cons(self));
    ligRetExp(ligSECD_stack_push_op(self, LIGEIA_OPERATOR_stack_cons));
    ligRetExp(ligSECD_stack_cons(self));

    ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_eval));

    ligRetExp(ligSECD_run(self));

    ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
    ligTestExp(88 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
    ligTestExp(77 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
  }
  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    int ret = LIGEIA_RETURN_INNER_ERROR;
    ligSECD *self = ligSECD_new(1*1024*1024);
    if (!self) {
      LIGEIA_TRACE_ALWAYS("err! : failed ligSECD_new.");
      goto error_ligSECD_new;
    }

    if (LIGEIA_RETURN_OK != (ret = test_swap(self)))    { goto error_test; }
    if (LIGEIA_RETURN_OK != (ret = test_cons(self)))    { goto error_test; }
    if (LIGEIA_RETURN_OK != (ret = test_append(self)))  { goto error_test; }
    if (LIGEIA_RETURN_OK != (ret = test_define(self)))  { goto error_test; }
    if (LIGEIA_RETURN_OK != (ret = test_find(self)))    { goto error_test; }
    if (LIGEIA_RETURN_OK != (ret = test_eval(self)))    { goto error_test; }

 error_test:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
