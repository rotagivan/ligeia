/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligHash.c
 *  @brief test_ligHash.c
 *
 *  test_ligHash.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/27
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../src/debug.h"
#include "../src/ligHash.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    static const uint8_t data[16] = {
      0x2bu, 0x6cu, 0x81u, 0x58u,
      0xe8u, 0x0fu,
      0x11u, 0xe5u,
      0x82u, 0xf7u,
      0x00u, 0x03u, 0x0du, 0x80u, 0x79u, 0x67u
    };
    LIGEIA_ASSERT_ALWAYS(0x03d71136u == ligHash_combine(0u, data, 16));
    return 0;
  }
}
