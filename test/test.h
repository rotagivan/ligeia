/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test.h
 *  @brief test.h
 *
 *  test.h
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/18
 *  @date 2017/07/20
 */

#ifndef LIGEIA_TEST_TEST_H_
#define LIGEIA_TEST_TEST_H_

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
#define ligTestExp(exp)                                                 \
  do {                                                                  \
    ligRetExp((exp) ? LIGEIA_RETURN_OK : LIGEIA_RETURN_INNER_ERROR);    \
  } while(0)

#endif  /* LIGEIA_TEST_TEST_H_ */
