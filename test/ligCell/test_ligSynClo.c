/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligSynclo.c
 *  @brief test_ligSynclo.c
 *
 *  test_ligSynclo.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>
#include <string.h>

#include "../../src/debug.h"
#include "../../src/ligCell.h"
#include "../../src/ligSECD.h"

#include "../test.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static ligReturn test_synclo(ligSECD *self);
ligReturn test_synclo(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_synclo");
  {
    ligRetExp(ligSECD_reset(self));
    {
      {
        {
          ligRetExp(ligSECD_stack_push_int(self, 77));
          ligRetExp(ligSECD_stack_push_nil(self));
          ligRetExp(ligSECD_stack_push_envspec(self, ligSECD_env(self)));
          ligSECD_print(self);
        }
        ligRetExp(ligSECD_mksynclo(self));
      }
      /*
        ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_call));

        ligSECD_print(self);

        ligRetExp(ligSECD_run(self));
      */
      ligSECD_print(self);
      /*
        ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
        ligTestExp(1 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
        ligTestExp(2 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
      */
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    int ret = LIGEIA_RETURN_INNER_ERROR;
    ligSECD *self = ligSECD_new(1*1024*1024);
    if (!self) {
      LIGEIA_TRACE_ALWAYS("err! : failed ligSECD_new.");
      goto error_ligSECD_new;
    }

    if (LIGEIA_RETURN_OK != (ret = test_synclo(self)))  { goto error_test; }

 error_test:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
