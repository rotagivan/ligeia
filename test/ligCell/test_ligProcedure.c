/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligProcedure.c
 *  @brief test_ligProcedure.c
 *
 *  test_ligProcedure.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/06/16
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../../src/debug.h"
#include "../../src/ligCell.h"
#include "../../src/ligSECD.h"

#include "../test.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static ligReturn test_proc(ligSECD *self);
ligReturn test_proc(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_proc");
  {
    ligRetExp(ligSECD_reset(self));
    {
      {  /* procedure */
        { /* body */
          ligRetExp(ligSECD_stack_push_nil(self));
          ligRetExp(ligSECD_stack_push_symbol(self, "a1", 2));
          ligRetExp(ligSECD_stack_cons(self));
          ligRetExp(ligSECD_stack_push_symbol(self, "a0", 2));
          ligRetExp(ligSECD_stack_cons(self));
          ligRetExp(ligSECD_stack_push_symbol(self, "cons", 4));
          ligRetExp(ligSECD_stack_cons(self));
        }
        { /* keys */
          ligRetExp(ligSECD_stack_push_nil(self));
          ligRetExp(ligSECD_stack_push_symbol(self, "a1", 2));
          ligRetExp(ligSECD_stack_cons(self));
          ligRetExp(ligSECD_stack_push_symbol(self, "a0", 2));
          ligRetExp(ligSECD_stack_cons(self));
        }
        ligRetExp(ligSECD_lambda(self));
      }

      {  /* args */
        ligRetExp(ligSECD_stack_push_nil(self));
        ligRetExp(ligSECD_stack_push_int(self, 88));
        ligRetExp(ligSECD_stack_cons(self));
        ligRetExp(ligSECD_stack_push_int(self, 77));
        ligRetExp(ligSECD_stack_cons(self));
      }

      ligRetExp(ligSECD_stack_swap(self));

      ligRetExp(ligSECD_code_push_op(self, LIGEIA_OPERATOR_call));

      ligRetExp(ligSECD_run(self));

      ligTestExp(ligCell_is_cons(ligSECD_stack_top(self)));
      ligTestExp(77 == ligCell_int(ligCell_car(ligSECD_stack_top(self))));
      ligTestExp(88 == ligCell_int(ligCell_cdr(ligSECD_stack_top(self))));
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    int ret = LIGEIA_RETURN_INNER_ERROR;
    ligSECD *self = ligSECD_new(1*1024*1024);
    if (!self) {
      LIGEIA_TRACE_ALWAYS("err! : failed ligSECD_new.");
      goto error_ligSECD_new;
    }

    if (LIGEIA_RETURN_OK != (ret = test_proc(self)))      { goto error_test; }

 error_test:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
