/** -*- mode:c; coding:utf-8-unix; -*-
 *  @file test_ligVector.c
 *  @brief test_ligVector.c
 *
 *  test_ligVector.c
 *
 *  Copyright 2017 hanepjiv
 *
 *  @author hanepjiv <hanepjiv@gmail.com>
 *  @copyright The MIT License (MIT) / Apache License Version 2.0
 *  @since 2017/07/19
 *  @date 2018/06/29
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif  /* HAVE_CONFIG_H */

#include <locale.h>
#include <stdio.h>

#include "../../src/debug.h"
#include "../../src/ligTypes.h"
#include "../../src/ligReturn.h"
#include "../../src/ligSECD.h"
#include "../../src/ligCell.h"
#include "../../src/ligCell/ligVector.h"

#include "../test.h"

/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
static ligReturn test_vector(ligSECD *self);
ligReturn test_vector(ligSECD *self) {
  LIGEIA_TRACE_DEBUG("test_vector");
  {
    ligRetExp(ligSECD_reset(self));
    {
      ligRetExp(ligSECD_stack_push_vector(self, 8u));
      ligRetExp(ligSECD_stack_push_int(self, 77));
      ligRetExp(ligVector_setx(ligCell_cadr(ligSECD_stack(self)), 0u,
                               ligCell_car(ligSECD_stack(self))));
      ligRetExp(ligSECD_stack_pop(self));
      ligSECD_print(self);
    }
  }
  return LIGEIA_RETURN_OK;
}
/* ///////////////////////////////////////////////////////////////////////// */
/* ========================================================================= */
int main(/*@unused@*/ int argc, /*@unused@*/ char* argv[]) {
  (void) argc;
  (void) argv;

  (void) setlocale(LC_ALL, "");
  {
    int ret = LIGEIA_RETURN_INNER_ERROR;
    ligSECD *self = ligSECD_new(1*1024*1024);
    if (!self) {
      LIGEIA_TRACE_ALWAYS("err! : failed ligSECD_new.");
      goto error_ligSECD_new;
    }

    if (LIGEIA_RETURN_OK != (ret = test_vector(self)))  { goto error_test; }

 error_test:
    ligSECD_delete(self);
 error_ligSECD_new:
    return LIGEIA_RETURN_OK == ret ? 0 : 1;
  }
}
