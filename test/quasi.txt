'1                                                      => 1
''1                                                     => (quote 1)
'()                                                     => ()
'(1)                                                    => (1)

`1                                                      => 1
``1                                                     => (quasiquote 1)

`(,(+ 1 2))                                             => (3)
`(,@(list 1 2))                                         => (1 2)
`(',@(list 1 2))                                        => ((quote 1 2))

(quasiquote ((unquote (+ 1 2))))                        => (3)
(quasiquote ((unquote-unqspl (list 1 2))))            => (1 2)
(quasiquote ((quote (unquote-unqspl (list 1 2)))))    => ((quote 1 2))


'`(`,,1)                                                => `(`,,1)
`'(`,,1)                                                => '(`,1)
