;;;; -*- mode:scheme; coding:utf-8; -*-
;;;; @file test.scm;
;;;; @brief test.scm
;;;;
;;;; test.scm
;;;;
;;;; Copyright 2017 hanepjiv
;;;;
;;;; @author hanepjiv <hanepjiv@gmail.com>
;;;; @since 2017/07/14
;;;; @date 2018/07/26


;;;(import (scheme base) (scheme r5rs))

(define (displayln x)
  (display x)
  (newline))

#|
(define-library192 "mine"
  (export a))
|#

#;(test00 1
      go 2)

#|
(let ((x 1))
  (let-syntax ((test (syntax-rules (go)
                       ((_ a go b ...) (print a ", " b ...)))))
    (test x go x)))
|#

#|
(define (fib n)
  (cond ((eqv? n 0)     0)
        ((eqv? n 1)     1)
        (else           (+ (fib (- n 2)) (fib (- n 1))))))
(displayln (fib 30))
|#

;; ############################################################################
(displayln 0)
(displayln +0)
(displayln -0)
(displayln 00)
(displayln +00)
(displayln -00)
(displayln +123)
(displayln +123)
(displayln -123)
(displayln 1.5)
(displayln +1.5)
(displayln -1.5)
(displayln 5.)
(displayln +5.)
(displayln -5.)
(displayln .5)
(displayln +.5)
(displayln -.5)
(displayln "test")
(displayln #t)
(displayln #f)

;; ############################################################################
(displayln (list 1 2))
(displayln (cons (cons 1 2) (cons 1 2)))

;; ############################################################################
(displayln '1)
(displayln (quote 1))
(displayln 'test)
(displayln '#t)
(displayln (quote #t))
(displayln '#f)
(displayln '())
(displayln '( () ))
(displayln '( () () ))
(displayln '(1 2))
(displayln '(1 . 2))
(displayln '(cons 1 2))
(displayln (quote (cons 1 2)))
(displayln (car (cdr (cdr '(3 2 1.)))))
(displayln '(1 #; 2 3))
(displayln '(1 . #; 2 3))

(displayln `1)
(displayln `())
(displayln `(1 2))
(displayln (quasiquote (1)))
(displayln `(cons 1 2))
(displayln (quasiquote ,(cons 1 2)))
(displayln (quasiquote (unquote (cons 1 2))))
(displayln (quasiquote (cons ,(cons 2 3) ,(cons 1 1) 2)))
(displayln `(1 ,(cons 2 1)))
(displayln `(1 `(,1)))
(displayln `(1 ,@(list (cons 2 3) 4) 5))

;; ############################################################################
(displayln (+ 1 2 3 4 5))
(displayln (- 3 (+ 1 2)))
(displayln (* 1 2 3 4 5))
(displayln (/ 3 (+ 1 2)))

#|
(displayln (/ 6.0 9.0))
(displayln (/ 30.0 (* 5 2)))
|#

;; ############################################################################
(define name "LIG")
(displayln name)
(set! name "ligeia")
(displayln name)

;; ############################################################################
(displayln ((lambda () 100)))
(displayln ((lambda (x) x) 2))
(displayln ((lambda (x y) (+ x 500 y)) (+ 1000 100) 10))
(displayln ((lambda x x) 1 2 3 4 5))
(displayln ((lambda (x y . rest) rest) 1 2 3 4))

(define func00
  (lambda ()
    (+ 1 2)))
(displayln (func00))

(define (func01)
  (+ 2 3))
(displayln (func01))

(define (func02 x)
  (+ x 1))
(displayln (func02 2))

(define (func03 x y . rest)
  rest)
(displayln (func03 1000 100 10 1))

;; ############################################################################
(define (person_new age)
  (lambda () `(age= ,age)))

(define mine00
  (person_new 0))
(displayln (mine00))

(define mine01
  (person_new 1))
(displayln (mine01))

(define (person_age p)
  (p))
(displayln (person_age mine00))
(displayln (person_age mine01))

#|
;; ############################################################################
;; continuation
(define cc #f)
(let* ((x (call/cc (lambda (k) (set! cc k) '(#f))))
       (return (car x)))
  (if return
      (let* ((i (cadr x)))
        (return (+ 100 i)))))
(define (call i)
  (call/cc (lambda (k) (cc `(,k ,i)))))
(displayln (call (/ 2 3)))
(displayln (call (+ 2 3)))
|#

#|
;; ############################################################################
;;(foreign "test")
|#

#|
;; ############################################################################
;; syntax
(define-syntax mysyntax
  (syntax-rules ()
    ((_ ligeia) 1)
    ((_) 1)))
(mysyntax)
|#

#|
;; ############################################################################
(displayln (cond (#t "true")
                 (else "false")))
(displayln (cond (#t "true")))
(displayln (cond (#f "true")
                 (else "false")))
(displayln (cond (#f "true")))

(displayln (if #t "true" "false"))
(displayln (if #t "true"))
(displayln (if #f "true" "false"))
(displayln (if #f "true"))

;; ############################################################################
(displayln (== 0 0))
(displayln (!= 0 0))
(displayln (<  0 0))
(displayln (>  0 0))
(displayln (<= 0 0))
(displayln (>= 0 0))

(displayln (== 0 1))
(displayln (!= 0 1))
(displayln (<  0 1))
(displayln (>  0 1))
(displayln (<= 0 1))
(displayln (>= 0 1))

(displayln ((lambda (n)
              ((lambda (fib n)
                 (set! fib
                       (lambda (n)
                         (cond ((<= n 1)
                                n)
                               (else
                                (+ (fib (- n 2))
                                   (fib (- n 1)))))))
                 (fib n)) #f n)) 10))
|#
