#						-*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# =============================================================================
# version info
define([CURRENT],	[0])
define([AGE],		[0])

define([REVISION],	[1])
define([PRERELEASE],	[-alpha.2])
define([BUILDMETA],	[])

define([MAJOR],		[m4_eval(CURRENT-AGE)])
define([RELEASE],	[indir([MAJOR]).indir([AGE]).indir([REVISION])indir([PRERELEASE])indir([BUILDMETA])])
# =============================================================================
# autoconf
AC_PREREQ([2.71])
AC_INIT([ligeia],[RELEASE],[hanepjiv@gmail.com])

AC_CONFIG_SRCDIR([config.h.in])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_AUX_DIR([config])
AC_CONFIG_MACRO_DIR([m4])

AC_CANONICAL_TARGET
# =============================================================================
# automake
AM_INIT_AUTOMAKE([1.15.1 foreign])
# =============================================================================
# libtool
LT_PREREQ([2.4.6])
LT_CONFIG_LTDL_DIR([libltdl])
LT_INIT([static shared win32-dll dlopen])
LTDL_INIT
# =============================================================================
# define LIGEIA
LIGEIA_RELEASE="indir([RELEASE])"
# -----------------------------------------------------------------------------
LIGEIA_CPPFLAGS=
LIGEIA_CFLAGS="\
	-pipe \
	-std=c99 \
	-fno-asm \
	-fno-builtin \
	-fvisibility=hidden \
	-pedantic-errors \
	-Werror \
	-W \
	-Wall \
	-Wextra \
	-Waggregate-return \
	-Wcast-align \
	-Wcast-qual \
	-Wchar-subscripts \
	-Wcomment \
	-Wconversion \
	-Wenum-compare \
	-Wformat=2 \
	-Wimplicit \
	-Wimplicit-int \
	-Wimplicit-function-declaration \
	-Winline \
	-Wlong-long \
	-Wmain \
	-Wmissing-prototypes \
	-Wmissing-declarations \
	-Wnested-externs \
	-Wno-import \
	-Wparentheses \
	-Wpointer-arith \
	-Wredundant-decls \
	-Wreturn-type \
	-Wshadow \
	-Wstrict-prototypes \
	-Wswitch \
	-Wtrigraphs \
	-Wuninitialized \
	-Wunused \
	-Wwrite-strings \
	"
LIGEIA_LDFLAGS=
LIGEIA_LIBS="-lm"
LIGEIA_LFLAGS=
LIGEIA_YFLAGS="-y -d"
# =============================================================================
# Checks for system
case "$host" in
    *-*-linux-gnu)
	LIGEIA_CPPFLAGS="$LIGEIA_CPPFLAGS -DPIC -DPIE"
	LIGEIA_CFLAGS="$LIGEIA_CFLAGS -fPIC -fPIE"
	LIGEIA_LDFLAGS="$LIGEIA_LDFLAGS -pie -rdynamic"
	;;
    *-*-cygwin | *-*-mingw32)
	LIGEIA_CPPFLAGS="$LIGEIA_CPPFLAGS -mwindows"
	LIGEIA_LDFLAGS="$LIGEIA_LDFLAGS -mwindows"
	;;
    *)
	AC_MSG_ERROR([not support $host])
	;;
esac
# =============================================================================
# Checks for debug
AC_MSG_CHECKING([for debug])
AC_ARG_ENABLE([debug],
	[  --enable-debug	  turn on debug. [[default=no]]],
	,
	[enable_debug=no])
AC_MSG_RESULT([$enable_debug])
# -----------------------------------------------------------------------------
# Checks for debug GC
AC_MSG_CHECKING([for debug_gc])
AC_ARG_ENABLE([debug_gc],
	[  --enable-debug_gc	  turn on debug_gc. [[default=no]]],
	,
	[enable_debug_gc=no])
AC_MSG_RESULT([$enable_debug_gc])
# -----------------------------------------------------------------------------
if test "x$enable_debug" = "xno" -a "x$enable_debug_gc" = "xyes"; then
	AC_MSG_NOTICE([turn on enable-debug by enable-debug_gc])
	enable_debug=yes
fi
# -----------------------------------------------------------------------------
if test "x$enable_debug" = "xyes"; then
	LIGEIA_CPPFLAGS="${LIGEIA_CPPFLAGS} -DDEBUG -DLIGEIA_DEBUG"
	LIGEIA_CFLAGS="${LIGEIA_CFLAGS} -g -gstabs -pg -O0"
	LIGEIA_LDFLAGS="${LIGEIA_LDFLAGS} -pg"
else
	LIGEIA_CPPFLAGS="${LIGEIA_CPPFLAGS} -DNDEBUG"
	LIGEIA_CFLAGS="${LIGEIA_CFLAGS} -fomit-frame-pointer -Os"
fi
AM_CONDITIONAL(DEBUG, test "x$enable_debug" = "xyes")
# -----------------------------------------------------------------------------
if test "x$enable_debug_gc" = "xyes"; then
	LIGEIA_CPPFLAGS="${LIGEIA_CPPFLAGS} -DLIGEIA_DEBUG_GC"
fi
AM_CONDITIONAL(DEBUG_GC, test "x$enable_debug_gc" = "xyes")
# =============================================================================
# Checks for programs.
AC_PROG_CXX
AC_PROG_AWK
AC_PROG_CC
AM_PROG_CC_C_O
AC_PROG_CPP
AC_PROG_RANLIB
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PROG_YACC
AM_PROG_LEX
# =============================================================================
# Checks for libraries.
# =============================================================================
# Checks for header files.
AC_CHECK_HEADERS([float.h locale.h memory.h stddef.h stdint.h stdlib.h string.h])
# =============================================================================
# Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_INT8_T
AC_TYPE_SIZE_T
AC_TYPE_UINT16_T
AC_TYPE_UINT32_T
AC_TYPE_UINT8_T
# =============================================================================
# Checks for library functions.
#AC_FUNC_MALLOC
AC_CHECK_FUNCS([memset setlocale])
# =============================================================================
# Checks for test
AC_MSG_CHECKING([for test])
AC_ARG_ENABLE([test],
	[  --enable-test	   turn on test. [[default=no]]],
	,
	[enable_test=no])
AC_MSG_RESULT([$enable_test])
# -----------------------------------------------------------------------------
AM_CONDITIONAL(TEST, test "x$enable_test" = "xyes")
# =============================================================================
# Checks for test splint
AC_MSG_CHECKING([for test_splint])
AC_ARG_ENABLE([test_splint],
	[  --enable-test_splint	  turn on test_splint. [[default=yes]]],
	,
	[enable_test_splint=yes])
AC_MSG_RESULT([$enable_test_splint])
# -----------------------------------------------------------------------------
if test "x$enable_test" = "xno" -a "x$enable_test_splint" = "xyes"; then
	AC_MSG_NOTICE([turn off test_splint by enable-test])
	enable_test_splint=no
fi
# -----------------------------------------------------------------------------
if test "x$enable_test_splint" = "xyes"; then
   # splint
   AC_CHECK_PROG(SPLINT, [splint], [splint])
   AC_SUBST(SPLINT)
fi
# -----------------------------------------------------------------------------
AM_CONDITIONAL(TEST_SPLINT, test "x$enable_test_splint" = "xyes")
# =============================================================================
# Checks for test scan-build
AC_MSG_CHECKING([for test_scan_build])
AC_ARG_ENABLE([test_scan_build],
	[  --enable-test_scan_build
			  turn on test_scan_build. [[default=yes]]],
	,
	[enable_test_scan_build=yes])
AC_MSG_RESULT([$enable_test_scan_build])
# -----------------------------------------------------------------------------
if test "x$enable_test" = "xno" -a "x$enable_test_scan_build" = "xyes"; then
	AC_MSG_NOTICE([turn off test_scan_build by enable-test])
	enable_test_scan_build=no
fi
# -----------------------------------------------------------------------------
if test "x$enable_test_scan_build" = "xyes"; then
   # scan-build
   AC_CHECK_PROG(SCAN_BUILD, [scan-build], [scan-build])
   AC_SUBST(SCAN_BUILD)
fi
# -----------------------------------------------------------------------------
AM_CONDITIONAL(TEST_SCAN_BUILD, test "x$enable_test_scan_build" = "xyes")
# =============================================================================
# export
AC_SUBST(LIGEIA_RELEASE)

AC_SUBST(LIGEIA_CPPFLAGS)
AC_SUBST(LIGEIA_CFLAGS)
AC_SUBST(LIGEIA_LDFLAGS)
AC_SUBST(LIGEIA_LIBS)
AC_SUBST(LIGEIA_YFLAGS)
AC_SUBST(LIGEIA_LFLAGS)

AC_CONFIG_FILES([
	Makefile
	ligeia.pc
	Doxyfile
	splint.sh
	scan-build.sh
	include/ligeia/Makefile
	src/Makefile
	src/ligCell/Makefile
	src/ligReader/Makefile
	test/Makefile
	test/ligCell/Makefile
],
[
	chmod ug+x splint.sh
	chmod ug+x scan-build.sh
])
AC_CONFIG_SUBDIRS([
])
# =============================================================================
AC_OUTPUT
